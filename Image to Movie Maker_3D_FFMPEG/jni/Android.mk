LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := ARM_ARCH

LOCAL_SRC_FILES := com_escrow_libffmpeg_utils_armArch.c

LOCAL_CFLAGS := -DHAVE_NEON=1
LOCAL_STATIC_LIBRARIES := cpufeatures

LOCAL_LDLIBS := -llog

include $(BUILD_SHARED_LIBRARY)
$(call import-module,android/cpufeatures)

ifeq (,$(TARGET_BUILD_APPS))

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := android-support-v8-renderscript
#LOCAL_SDK_VERSION := 17
LOCAL_SRC_FILES := $(call all-java-files-under, java/src)

include $(BUILD_STATIC_JAVA_LIBRARY)

# Include this library in the build server's output directory
# TODO: Not yet.
#$(call dist-for-goals, dist_files, $(LOCAL_BUILT_MODULE):volley.jar)

# TODO: Build the tests as an APK here

include $(call all-makefiles-under, $(LOCAL_PATH))

endif