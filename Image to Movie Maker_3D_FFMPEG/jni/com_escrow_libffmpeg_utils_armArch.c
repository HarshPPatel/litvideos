#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <cpu-features.h>
#include<Android/log.h>

#define TAG		"armArch_test"
#define logI(...) __android_log_print(ANDROID_LOG_INFO,TAG,__VA_ARGS__)

jstring
Java_com_videolib_libffmpeg_ArmArchHelper_cpuArchFromJNI(JNIEnv* env, jobject obj)
{
    // Maximum we need to store here is ARM v7-neon
    // Which is 11 char long, so initializing a character array of length 11
	char arch_info[11] = "";

	uint64_t cpu = android_getCpuFamily();
		// checking if CPU is of ARM family or not


		if (cpu == ANDROID_CPU_FAMILY_ARM) {
			strcpy(arch_info, "ARM");
			logI("ANDROID_CPU_FAMILY_ARM = %d",ANDROID_CPU_FAMILY_ARM);
			uint64_t cpuFeatures = android_getCpuFeatures();
					if ((cpuFeatures & ANDROID_CPU_ARM_FEATURE_ARMv7) != 0) {
					    strcat(arch_info, "v7");
					    logI("ANDROID_CPU_FAMILY_ARM = v7");
						// checking if CPU is ARM v7 Neon
						if((cpuFeatures & ANDROID_CPU_ARM_FEATURE_NEON) != 0) {
						    strcat(arch_info, "-neon");
						    logI("ANDROID_CPU_FAMILY_ARM = v7-neon" );
						}
					}
		} else if (cpu == ANDROID_CPU_FAMILY_ARM64) {
			strcpy(arch_info, "ARM_64");
			logI("ANDROID_CPU_FAMILY_ARM = ARM64" );
		} else if (cpu == ANDROID_CPU_FAMILY_X86) {
			strcpy(arch_info, "x86");
			logI("ANDROID_CPU_FAMILY_ARM = x86" );
		} else if (cpu == ANDROID_CPU_FAMILY_MIPS) {
			strcpy(arch_info, "MIPS");
			logI("ANDROID_CPU_FAMILY_ARM = MIPS" );
		} else if (cpu == ANDROID_CPU_FAMILY_X86_64) {
			strcpy(arch_info, "x86_64");
			logI("ANDROID_CPU_FAMILY_ARM = X86_64");
		}





			//ANDROID_CPU_FAMILY_ARM,
		    //ANDROID_CPU_FAMILY_X86,
		    //ANDROID_CPU_FAMILY_MIPS,
		    //ANDROID_CPU_FAMILY_ARM64,
		    //ANDROID_CPU_FAMILY_X86_64,
		    //ANDROID_CPU_FAMILY_MIPS64,


    // checking if CPU is of ARM family or not
	/*if (android_getCpuFamily() == ANDROID_CPU_FAMILY_ARM) {
		strcpy(arch_info, "ARM");

		// checking if CPU is ARM v7 or not
		uint64_t cpuFeatures = android_getCpuFeatures();
		if ((cpuFeatures & ANDROID_CPU_ARM_FEATURE_ARMv7) != 0) {
		    strcat(arch_info, " v7");

			// checking if CPU is ARM v7 Neon
			if((cpuFeatures & ANDROID_CPU_ARM_FEATURE_NEON) != 0) {
			    strcat(arch_info, "-neon");
			}
		}
	}*/
	return (*env)->NewStringUTF(env, arch_info);
}
