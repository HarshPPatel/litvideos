package com.videolib.libffmpeg;

import android.text.TextUtils;

enum CpuArch {
	x86("0dd4dbad305ff197a1ea9e6158bd2081d229e70e"),
		    ARMv7("871888959ba2f063e18f56272d0d98ae01938ceb"),
	
	
//    x86("1b3daf0402c38ec0019ec436d71a1389514711bd"),
//    ARMv7("e27cf3c432b121896fc8af2d147eff88d3074dd5"),
//    ARMv7_NEON("9463c40e898c53dcac59b8ba39cfd590e2f1b1bf"),
//    X86_64("a8bd66274b5a8e0dc486a3ce554804dbf331e791"),
//    ARM64("22bba39a6540575b413d890535050aacdc888d40"),
    NONE(null);

    private String sha1;

    CpuArch(String sha1) {
        this.sha1 = sha1;
    }

    static CpuArch fromString(String sha1) {
        if (!TextUtils.isEmpty(sha1)) {
            for (CpuArch cpuArch : CpuArch.values()) {
                if (sha1.equalsIgnoreCase(cpuArch.sha1)) {
                    return cpuArch;
                }
            }
        }
        return NONE;
    }

    String getSha1() {
        return sha1;
    }
}
