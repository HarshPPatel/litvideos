package com.videolib.libffmpeg;

import android.os.Build;

class CpuArchHelper {
//
//    static CpuArch getCpuArch() {
//        // check if device is x86
//        Log.d(Build.CPU_ABI);
//        if (Build.CPU_ABI.equals(getx86CpuAbi()))
//            return CpuArch.x86;
//        else if (Build.CPU_ABI.equals(getArmeabiv7CpuAbi())) {
//            // check if device is armeabi
//            ArmArchHelper cpuNativeArchHelper = new ArmArchHelper();
//            String archInfo = cpuNativeArchHelper.cpuArchFromJNI();
//            // check if device is arm v7
//            if (cpuNativeArchHelper.isARM_v7_CPU(archInfo)) {
//                // check if device is neon
//                if (cpuNativeArchHelper.isNeonSupported(archInfo)) {
//                    return CpuArch.ARMv7;
//                }
//                return CpuArch.ARMv7;
//            }
//            // check if device is x86_64
//        } else if (Build.CPU_ABI.equals(getX86_64CpuAbi()))
//            return CpuArch.x86;
//            // check if device is arm64v8
//        else if (Build.CPU_ABI.equals(getArm64CpuAbi()))
//            return CpuArch.ARMv7;
//        return CpuArch.NONE;
//    }
    
    
    static CpuArch getCpuArch() {
        // check if device is x86
        Log.d(Build.CPU_ABI);
        if (Build.CPU_ABI.equals(getx86CpuAbi()))
            return CpuArch.x86;
        else if (Build.CPU_ABI.equals(getArmeabiv7CpuAbi())) {
            // check if device is armeabi
//            ArmArchHelper cpuNativeArchHelper = new ArmArchHelper();
//            String archInfo = cpuNativeArchHelper.cpuArchFromJNI();
//            // check if device is arm v7
//            if (cpuNativeArchHelper.isARM_v7_CPU(archInfo)) {
//                // check if device is neon
//                if (cpuNativeArchHelper.isNeonSupported(archInfo)) {
//                    return CpuArch.ARMv7;
//                }
//                return CpuArch.ARMv7;
//            }
            
            return CpuArch.ARMv7;
            // check if device is x86_64
        } else if (Build.CPU_ABI.equals(getX86_64CpuAbi()))
            return CpuArch.x86;
            // check if device is arm64v8
        else if (Build.CPU_ABI.equals(getArm64CpuAbi()))
            return CpuArch.ARMv7;
        return CpuArch.NONE;
    }

    static String getx86CpuAbi() {
        return "x86";
    }

    static String getArmeabiv7CpuAbi() {
        return "armeabi-v7a";
    }

    static String getX86_64CpuAbi() {
        return "x86_64";
    }

    static String getArm64CpuAbi() {
        return "arm64-v8a";
    }
}
