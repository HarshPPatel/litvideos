package com.videolib.libffmpeg;

public interface FFmpegLoadBinaryResponseHandler extends ResponseHandler {

	  /**
     * on Fail
     */
    void onFailure();
    void onFailure(String cpuType);

    /**
     * on Success
     */
    void onSuccess();
    void onSuccess(String cpuType);

}
