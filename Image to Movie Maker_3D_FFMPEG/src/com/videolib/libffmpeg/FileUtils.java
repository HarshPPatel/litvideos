package com.videolib.libffmpeg;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.provider.MediaStore.Files;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.List;
import java.util.Map;

public class FileUtils {

    static final String ffmpegFileName = "ffmpeg";
    private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;
    private static final int EOF = -1;
    
    
    
    
    
    
    

	public final static String rawExternalStorage = System
			.getenv("EXTERNAL_STORAGE");
	public static String rawSecondaryStoragesStr = System
			.getenv("SECONDARY_STORAGE");
	public static long mDeleteFileCount = 0l;
	public static File mSdCard = new File(Environment
			.getExternalStorageDirectory().getAbsolutePath());
	public static File mDownloadDir = Environment
			.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

	public static File APP_DIRECTORY = new File(mSdCard, "Lit Videos");

	public static final File TEMP_DIRECTORY,TEMP_VID_DIRECTORY ,TEMP_DIRECTORY_AUDIO;

	public static final File frameFile;
	public static final File watermarkFile;
	static {
		TEMP_DIRECTORY = new File(APP_DIRECTORY, ".temp");
		TEMP_VID_DIRECTORY = new File(TEMP_DIRECTORY, ".temp_vid");
		TEMP_DIRECTORY_AUDIO =new File(APP_DIRECTORY, ".temp_audio");
		frameFile = new File(APP_DIRECTORY,".frame.png");
		watermarkFile = new File(APP_DIRECTORY,".icon.png");
		if (!TEMP_DIRECTORY.exists()) {
			TEMP_DIRECTORY.mkdirs();
		}
		if (!TEMP_VID_DIRECTORY.exists()) {
			TEMP_VID_DIRECTORY.mkdirs();
		}
	}
	
	

	
	public static File getVideoDirectory( ){
		if(!TEMP_VID_DIRECTORY.exists()){
			TEMP_VID_DIRECTORY.mkdirs();
		}
		return TEMP_VID_DIRECTORY;
	}
	
	
	public static File getVideoFile(int vNo ){
		if(!TEMP_VID_DIRECTORY.exists()){
			TEMP_VID_DIRECTORY.mkdirs();
		}
		File vidFile = new File(TEMP_VID_DIRECTORY, String.format("vid_%03d.mp4", vNo) );
		return vidFile;
	}
	
	
	public static File getImageDirectory(int iNo){
		File imageDir = new File(TEMP_DIRECTORY,String.format("IMG_%03d", iNo));
		if(!imageDir.exists()){
			imageDir.mkdirs();
		}
		return imageDir;
	}
	
	public static File getImageDirectory(String theme ){
		File imageDir = new File(TEMP_DIRECTORY,theme);
		if(!imageDir.exists()){
			imageDir.mkdirs();
		}
		return imageDir;
	}
	public static File getImageDirectory(String theme,int iNo){
		File imageDir = new File(getImageDirectory(theme),String.format( "IMG_%03d", iNo));
		if(!imageDir.exists()){
			imageDir.mkdirs();
		}
		return imageDir;
	}
	
	public static boolean  deleteThemeDir(String theme){
		return deleteFile(getImageDirectory(theme));
	}


	public FileUtils() {
		mDeleteFileCount = 0;
	}

	private static File[] mStorageList;

	private static File[] getStorge() {
		List<File> storage = new ArrayList<File>();
		if (rawExternalStorage != null) {
			File mSdCard0 = new File(rawExternalStorage);
			storage.add(mSdCard0);
		} else if (mSdCard != null) {
			storage.add(mSdCard);
		}
		if (rawSecondaryStoragesStr != null) {
			File mSdCard1 = new File(rawSecondaryStoragesStr);
			storage.add(mSdCard1);
		}
		mStorageList = new File[storage.size()];
		for (int i = 0; i < storage.size(); i++) {
			mStorageList[i] = storage.get(i);
		}
		return mStorageList;
	}

	public static File[] getStorages() {
		if (mStorageList != null) {
			return mStorageList;
		}

		return getStorge();
	}

	public static final String hiddenDirectoryName = ".MyGalaryLock/";
	public static String hiddenDirectoryNameThumb = ".MyGalaryLock/.thumbnail/";
	public static String unlockDirectoryNameVideo = "GalaryLock/Video/";
	public static String unlockDirectoryNameImage = "GalaryLock/Image/";
	//
	public static final String hiddenDirectoryNameVideo = "Video/";
	public static final String hiddenDirectoryNameImage = "Image/";

	public static final String hiddenDirectoryNameThumbImage = ".thumb/Image/";
	public static final String hiddenDirectoryNameThumbVideo = ".thumb/Video/";

	public static String humanReadableByteCount(long bytes, boolean si) {
		int unit = si ? 1000 : 1024;
		if (bytes < unit)
			return bytes + " B";
		int exp = (int) (Math.log(bytes) / Math.log(unit));
		String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1)
				+ (si ? "" : "i");
		return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
	}

	public static File getHiddenAppDirectory(File sdCard) {
		File file = new File(sdCard, hiddenDirectoryName);
		if (file.exists()) {
			file.mkdirs();
		}
		return file;
	}


	public static File getMoveFolderpath(File oldFilePath, String newFolderName) {
		File superParent = oldFilePath.getParentFile().getParentFile();
		return new File(superParent, newFolderName + "/"
				+ oldFilePath.getName());
	}

	public static File getRestoreVideoDirectory(File sdCFile, String folderName) {
		return new File(sdCFile, unlockDirectoryNameVideo + folderName);
	}

	public static File getRestoreImageDirectory(File sdCFile, String folderName) {
		return new File(sdCFile, unlockDirectoryNameImage + folderName);
	}

	public static File getHiddenVideoDirectory(File sdCFile, String folderName) {
		return new File(sdCFile, hiddenDirectoryName + hiddenDirectoryNameVideo
				+ folderName);
	}

	public static File getHiddenImageDirectory(File sdCFile, String folderName) {
		return new File(sdCFile, hiddenDirectoryName + hiddenDirectoryNameImage
				+ folderName);
	}

	public static File getThumbnailDirectory(File sdCard, int type) {
		File file = null;
		if (type == 0) {
			file = new File(getHiddenAppDirectory(sdCard),
					hiddenDirectoryNameThumbVideo);
		} else {
			file = new File(getHiddenAppDirectory(sdCard),
					hiddenDirectoryNameThumbImage);
		}
		if (file.exists()) {
			file.mkdirs();
		}
		return file;
	}


	public static File getHiddenImageAppDirectory(File sdCard) {
		File file = new File(getHiddenAppDirectory(sdCard),
				hiddenDirectoryNameImage);
		if (file.exists()) {
			file.mkdirs();
		}
		return file;
	}

	public static File getHiddenVideoAppDirectory(File sdCard) {
		File file = new File(getHiddenAppDirectory(sdCard),
				hiddenDirectoryNameVideo);
		if (file.exists()) {
			file.mkdirs();
		}
		return file;
	}

	public static ArrayList<File> getCacheDirectory(String packageName) {
		ArrayList<File> cacheDirs = new ArrayList<>();
		for (File sdCard : getStorages()) {
			File file = new File(sdCard, "Android/data/" + packageName
					+ "/cache");
			if (file.exists()) {
				cacheDirs.add(file);
			}
			file = new File(sdCard, "Android/data/" + packageName + "/Cache");
			if (file.exists()) {
				cacheDirs.add(file);
			}
			file = new File(sdCard, "Android/data/" + packageName + "/.cache");
			if (file.exists()) {
				cacheDirs.add(file);
			}
			file = new File(sdCard, "Android/data/" + packageName + "/.Cache");
			if (file.exists()) {
				cacheDirs.add(file);
			}
		}
		return cacheDirs;
	}


	@SuppressLint("SimpleDateFormat")
	public static File getOutputImageFile() {
		if (!APP_DIRECTORY.exists()) {
			if (!APP_DIRECTORY.mkdirs()) {
				return null;
			}
		}
		// take the current timeStamp
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		File mediaFile = new File(APP_DIRECTORY, "IMG_" + timeStamp + ".jpg");

		return mediaFile;
	}

	public static Bitmap getPicFromBytes(byte[] bytes) {
		if (bytes != null) {
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;

			// Bitmap backCameraImage = BitmapFactory.decodeByteArray(imageData,
			// 0, imageData.length).copy(Bitmap.Config.RGB_565, true);
			BitmapFactory.decodeByteArray(bytes, 0, bytes.length, o);
			// The new size we want to scale to
			final int REQUIRED_SIZE = 2048 / 2;

			// Find the correct scale value. It should be the power of 2.
			int scale = 1;
			if (o.outWidth >= o.outHeight) {
				if (o.outWidth >= REQUIRED_SIZE) {
					scale = Math.round((float) o.outWidth
							/ (float) REQUIRED_SIZE);
				}
			} else {
				if (o.outHeight >= REQUIRED_SIZE) {
					scale = Math.round((float) o.outHeight
							/ (float) REQUIRED_SIZE);
				}
			}
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeByteArray(bytes, 0, bytes.length, o2)
					.copy(Bitmap.Config.RGB_565, true);
		}
		return null;
	}

	public static long getDirectorySize(File directory) {
		long dirSize = 0L;
		if (directory != null) {
			File[] listFile = directory.listFiles();
			if (listFile != null && listFile.length > 0)
				for (File mFile : listFile) {
					if (mFile.isDirectory()) {
						dirSize += getDirectorySize(mFile);
					} else {
						dirSize += mFile.length();

					}
				}
		}
		return dirSize;
	}

	public static void copyFile(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}

	public static void deleteTempDir(){
		File[] list =TEMP_DIRECTORY.listFiles();
		for(final File child:list){
			new Thread(){
				public void run() {
					deleteFile(child);
				};
			}.start();
		}
	}
	// TODO delete directory or file
	public static boolean deleteFile(File mFile) {
		boolean idDelete = false;
		if (mFile == null)
			return idDelete;

		if (mFile.exists()) {
			if (mFile.isDirectory()) {
				File[] children = mFile.listFiles();
				if (children != null && children.length > 0) {
					for (File child : children) {
						mDeleteFileCount += (long) child.length();
						idDelete = deleteFile(child);
					}
				}
				mDeleteFileCount += (long) mFile.length();
				idDelete = mFile.delete();
			} else {
				mDeleteFileCount += (long) mFile.length();
				idDelete = mFile.delete();
			}
		}
		return idDelete;
	}

	public static boolean deleteFile(final String s) {
		return deleteFile(new File(s));
	}

	public static void moveFile(File src, File des) throws IOException {

		if (!src.exists()) {
			return;
		}
		if (src.renameTo(des)) {
			return;
		}
		if (!des.getParentFile().exists()) {
			des.getParentFile().mkdirs();
		}
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(des);
		// Copy the bits from instream to outstream
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();

		if (src.exists()) {
			src.delete();
		}
	}

	public static void moveFile(final String sourceLocation,
			final String targetLocation) throws IOException {

		if ((new File(sourceLocation).renameTo(new File(targetLocation)))) {
			return;
		}

		File file = new File(sourceLocation);

		if (!file.getParentFile().exists()) {
			file.getParentFile().mkdirs();
		}

		InputStream in = new FileInputStream(sourceLocation);
		OutputStream out = new FileOutputStream(targetLocation);
		// Copy the bits from instream to outstream
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
		File deleteFile = new File(sourceLocation);
		if (deleteFile.exists()) {
			deleteFile.delete();
		}
	}

	public static String getFileFormat(String id) {
		String format = System.currentTimeMillis() + "_0";
		if (id.endsWith("_1")) {
			return ".jpg";
		} else if (id.endsWith("_2")) {
			return ".JPG";
		} else if (id.endsWith("_3")) {
			return ".png";
		} else if (id.endsWith("_4")) {
			return ".PNG";
		} else if (id.endsWith("_5")) {
			return ".jpeg";
		} else if (id.endsWith("_6")) {
			return ".JPEG";
		} else if (id.endsWith("_7")) {
			return ".mp4";
		} else if (id.endsWith("_8")) {
			return ".3gp";
		} else if (id.endsWith("_9")) {
			return ".flv";
		} else if (id.endsWith("_10")) {
			return ".m4v";
		} else if (id.endsWith("_11")) {
			return ".avi";
		} else if (id.endsWith("_12")) {
			return ".wmv";
		} else if (id.endsWith("_13")) {
			return ".mpeg";
		} else if (id.endsWith("_14")) {
			return ".VOB";
		} else if (id.endsWith("_15")) {
			return ".MOV";
		} else if (id.endsWith("_16")) {
			return ".MPEG4";
		} else if (id.endsWith("_17")) {
			return ".DivX";
		} else if (id.endsWith("_18")) {
			return ".mkv";
		}
		return format;
	}

	public static String genrateFileId(String filePath) {

		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		String format = "" + System.currentTimeMillis();
		if (filePath.endsWith(".jpg")) {
			return format + "_1";
		} else if (filePath.endsWith(".JPG")) {
			return format + "_2";
		} else if (filePath.endsWith(".png")) {
			return format + "_3";
		} else if (filePath.endsWith(".PNG")) {
			return format + "_4";
		} else if (filePath.endsWith(".jpeg")) {
			return format + "_5";
		} else if (filePath.endsWith(".JPEG")) {
			return format + "_6";
		} else if (filePath.endsWith(".mp4")) {
			return format + "_7";
		} else if (filePath.endsWith(".3gp")) {
			return format + "_8";
		} else if (filePath.endsWith(".flv")) {
			return format + "_9";
		} else if (filePath.endsWith(".m4v")) {
			return format + "_10";
		} else if (filePath.endsWith(".avi")) {
			return format + "_11";
		} else if (filePath.endsWith(".wmv")) {
			return format + "_12";
		} else if (filePath.endsWith(".mpeg")) {
			return format + "_13";
		} else if (filePath.endsWith(".VOB")) {
			return format + "_14";
		} else if (filePath.endsWith(".MOV")) {
			return format + "_15";
		} else if (filePath.endsWith(".MPEG4")) {
			return format + "_16";
		} else if (filePath.endsWith(".DivX")) {
			return format + "_17";
		} else if (filePath.endsWith(".mkv")) {
			return format + "_18";
		}
		return format;
	}

	@SuppressLint("DefaultLocale") 
	public static String getDuration(long duration) {
		if (duration < 1000L) {
			return String.format("%02d:%02d", 0,0);
		}
		final long n = duration / 1000L;
		final long n2 = n / 3600L;
		final long n3 = (n - 3600L * n2) / 60L;
		final long n4 = n - (3600L * n2 + 60L * n3);
		if (n2 == 0L) {
			return String.format("%02d:%02d", n3,n4);
		}
		return     String.format("%02d:%02d:%02d",n2, n3,n4);
	}

	private static String putPrefixZero(final long n) {
		if (new StringBuilder().append(n).toString().length() < 2) {
			return "0" + n;
		}
		return new StringBuilder().append(n).toString();
	}

	public static char[] readPatternData(final Context activity) {
		try {
			if (new File(activity.getFilesDir() + "/pattern").exists()) {
				final ObjectInputStream objectInputStream = new ObjectInputStream(
						new FileInputStream(activity.getFilesDir() + "/pattern"));
				final char[] array = (char[]) objectInputStream.readObject();
				objectInputStream.close();
				return array;
			}
		} catch (Exception ex) {
		}
		return null;
	}

    
    
    
    
    
    

    static boolean copyBinaryFromAssetsToData(Context context, String fileNameFromAssets, String outputFileName) {
		
		// create files directory under /data/data/package name
		File filesDirectory = getFilesDirectory(context);
		
		InputStream is;
		try {
			is = context.getAssets().open(fileNameFromAssets);
			// copy ffmpeg file from assets to files dir
			final FileOutputStream os = new FileOutputStream(new File(filesDirectory, outputFileName));
			byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
			
			int n;
			while(EOF != (n = is.read(buffer))) {
				os.write(buffer, 0, n);
			}

            Util.close(os);
            Util.close(is);
			
			return true;
		} catch (IOException e) {
			Log.e("issue in coping binary from assets to data. ", e);
		}
        return false;
	}

	static File getFilesDirectory(Context context) {
		// creates files directory under data/data/package name
        return context.getFilesDir();
	}

  public  static String getFFmpeg(Context context) {
	  
//	  return new File(getFilesDirectory(context),FileUtils.ffmpegFileName).getAbsolutePath();
        return getFilesDirectory(context).getAbsolutePath() + File.separator + FileUtils.ffmpegFileName;
  }

    static String getFFmpeg(Context context, Map<String,String> environmentVars) {
        String ffmpegCommand = "";
        if (environmentVars != null) {
            for (Map.Entry<String, String> var : environmentVars.entrySet()) {
                ffmpegCommand += var.getKey()+"="+var.getValue()+" ";
            }
        }
        ffmpegCommand += getFFmpeg(context);
        return ffmpegCommand;
    }

    static String SHA1(String file) {
        InputStream is = null;
        try {
            is = new BufferedInputStream(new FileInputStream(file));
            return SHA1(is);
        } catch (IOException e) {
            Log.e(e);
        } finally {
            Util.close(is);
        }
        return null;
    }

    static String SHA1(InputStream is) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
            final byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            for (int read; (read = is.read(buffer)) != -1; ) {
                messageDigest.update(buffer, 0, read);
            }

            Formatter formatter = new Formatter();
            // Convert the byte to hex format
            for (final byte b : messageDigest.digest()) {
                formatter.format("%02x", b);
            }
            return formatter.toString();
        } catch (NoSuchAlgorithmException e) {
            Log.e(e);
        } catch (IOException e) {
            Log.e(e);
        } finally {
            Util.close(is);
        }
        return null;
    }
    
    
    public static void addVideoToConcat(int i){
    	appendLog(new File(getVideoDirectory(),"video.txt"), String.format("file '%s'", getVideoFile(i).getAbsolutePath()));
    }
    public static void addImageTovideo( File file){
    	
    	appendLog(getVideoDirectory(), String.format("file '%s'", file.getAbsolutePath()));
    }
    
	public static  void appendLog(File parent,String text) {
		File logFile = parent;
		if (!logFile.exists()) {
			try {
				logFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			BufferedWriter buf = new BufferedWriter(new FileWriter(logFile,true));
			buf.append(text);
			buf.newLine();
			buf.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
 
}