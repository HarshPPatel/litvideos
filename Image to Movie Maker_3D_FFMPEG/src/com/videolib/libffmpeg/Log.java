package com.videolib.libffmpeg;

class Log {

    private static String TAG = FFmpeg.class.getSimpleName();
    private static boolean DEBUG = false;

    static void setDEBUG(boolean DEBUG) {
        Log.DEBUG = DEBUG;
    }

    static void d(Object obj) {
        if (DEBUG) {
            android.util.Log.d(TAG, obj != null ? obj.toString() : null + "");
        }
    }

    static void i(Object obj) {
        if (DEBUG) {
            android.util.Log.i(TAG, obj != null ? obj.toString() : null + "");
        }
    }

    static void e(Object obj, Throwable throwable) {
        if (DEBUG) {
            android.util.Log.e(TAG, obj != null ? obj.toString() : null + "", throwable);
        }
    }

    static void e(Throwable throwable) {
        if (DEBUG) {
            android.util.Log.e(TAG, "", throwable);
        }
    }

}
