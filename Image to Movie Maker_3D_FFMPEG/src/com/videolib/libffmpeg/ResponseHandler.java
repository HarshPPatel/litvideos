package com.videolib.libffmpeg;

interface ResponseHandler {

    /**
     * on Start
     */
    void onStart();

    /**
     * on Finish
     */
    void onFinish();

}
