package com.videolib.libffmpeg;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;

public class Util {
	
	public static final File project_dir= new File(Environment.getExternalStorageDirectory(),"/EscrowFFmpeg");
	public static final File temp_audio_dir=new File(project_dir,"/.temp/audio");
	public static final File temp_simple_video_dir=new File(project_dir,"/.temp/video");
	public static final File temp_animated_video_dir=new File(project_dir,"/.temp/anim_video");
	
	
	public static final boolean IS_JBMR2 = Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2;
	public static final boolean IS_ISC = Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
	public static final boolean IS_GINGERBREAD_MR1 = Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD_MR1;
	
	public static void makeDirectorys(){
		if(!project_dir.exists()){
			project_dir.mkdirs();
		}
		if(!temp_audio_dir.exists()){
			temp_audio_dir.mkdirs();
		}
		if(!temp_simple_video_dir.exists()){
			temp_simple_video_dir.mkdirs();
		}
		if(!temp_animated_video_dir.exists()){
			temp_animated_video_dir.mkdirs();
		}
	}
	static{
		makeDirectorys();
	}
	public static boolean isDebug(Context context) {
        return (0 != (context.getApplicationContext().getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE));
    }
    public  static void close(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                // Do nothing
            }
        }
    }

    public static void close(OutputStream outputStream) {
        if (outputStream != null) {
            try {
                outputStream.flush();
                outputStream.close();
            } catch (IOException e) {
                // Do nothing
            }
        }
    }

    public static String convertInputStreamToString(InputStream inputStream) {
        try {
            BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
            String str;
            StringBuilder sb = new StringBuilder();
            while ((str = r.readLine()) != null) {
                sb.append(str);
            }
            return sb.toString();
        } catch (IOException e) {
            Log.e("error converting input stream to string", e);
        }
        return null;
    }

    public  static void destroyProcess(Process process) {
        if (process != null)
            process.destroy();
    }

    public static boolean killAsync(AsyncTask<?, ?, ?> asyncTask) {
        return asyncTask != null && !asyncTask.isCancelled() && asyncTask.cancel(true);
    }

   public static boolean isProcessCompleted(Process process) {
        try {
            if (process == null) return true;
            process.exitValue();
            return true;
        } catch (IllegalThreadStateException e) {
            // do nothing
        }
        return false;
    }
}
