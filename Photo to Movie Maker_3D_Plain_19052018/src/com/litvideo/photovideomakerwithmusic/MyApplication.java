package com.litvideo.photovideomakerwithmusic;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.ImageColumns;
import android.provider.MediaStore.MediaColumns;
import android.util.Log;

import com.litvideo.photovideomakerwithmusic.data.ImageData;
import com.litvideo.photovideomakerwithmusic.data.MusicData;
import com.litvideo.photovideomakerwithmusic.util.PermissionModelUtil;
import com.movienaker.movie.themes.THEMES;
import com.videolib.libffmpeg.FFmpeg;
import com.videolib.libffmpeg.FFmpegLoadBinaryResponseHandler;
import com.videolib.libffmpeg.exceptions.FFmpegNotSupportedException;

public class MyApplication extends Application {
	public static int VIDEO_WIDTH = 720;
	public static int VIDEO_HEIGHT = 480;
	public static boolean islistchanged;
	public HashMap<String, ArrayList<ImageData>> allAlbum;
	public ArrayList<String> videoImages = new ArrayList<>();
	private ArrayList<String> allFolder;
	private final ArrayList<ImageData> selectedImages = new ArrayList<ImageData>();
	private static MyApplication instance;
	public static boolean isBreak = false;
	private String selectedFolderId = "";
	private MusicData musicData;
	private float second = 2.0f;
	public THEMES selectedTheme = THEMES.Shine;
	public boolean isEditModeEnable = false;
	public boolean isFromSdCardAudio = false;

	public int min_pos = Integer.MAX_VALUE;

	public static MyApplication getInstance() {
		return instance;
	}

	public MyApplication() {
		super();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		init();
	}

	private void init() {
		if (!new PermissionModelUtil(this).needPermissionCheck()) {

			getFolderList();
		}

		try {
			loadLib();
		} catch (FFmpegNotSupportedException e) {
			e.printStackTrace();
			return;
		}
	}

	public void initArray() {
		videoImages = new ArrayList<>();
	}

	public float getSecond() {
		return second;
	}

	public void setSecond(float second) {
		this.second = second;
	}

	public void setMusicData(MusicData musicData) {
		isFromSdCardAudio = false;
		this.musicData = musicData;
	}

	public MusicData getMusicData() {
		return musicData;
	}

	public void setSelectedFolderId(String selectedFolderId) {
		this.selectedFolderId = selectedFolderId;

	}

	public String getSelectedFolderId() {
		return selectedFolderId;
	}

	public HashMap<String, ArrayList<ImageData>> getAllAlbum() {
		return allAlbum;
	}

	public ArrayList<ImageData> getImageByAlbum(String folderId) {
		ArrayList<ImageData> imageDatas = getAllAlbum().get(folderId);
		if (imageDatas == null) {
			imageDatas = new ArrayList<ImageData>();
		}
		return imageDatas;
	}

	public ArrayList<ImageData> getSelectedImages() {
		return selectedImages;
	}

	public void addSelectedImage(ImageData imageData) {
		selectedImages.add(imageData);
		imageData.imageCount++;
	}

	// public void removeSelectedImage(ImageData imageData){
	// selectedImages.remove(imageData);
	// imageData.imageCount--;
	// }
	public void removeSelectedImage(int imageData) {
		if (imageData <= selectedImages.size()) {
			selectedImages.remove(imageData).imageCount--;
		}
	}

	public void getFolderList() {
		allFolder = new ArrayList<String>();
		allAlbum = new HashMap<String, ArrayList<ImageData>>();
		String[] projection = new String[] { MediaColumns.DATA,
				BaseColumns._ID, ImageColumns.BUCKET_DISPLAY_NAME,
				ImageColumns.BUCKET_ID, ImageColumns.DATE_TAKEN };
		Uri images = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
		final String orderBy = ImageColumns.BUCKET_DISPLAY_NAME;
		Cursor cur = getContentResolver().query(images, projection, null, null,
		// orderBy + " DESC" // Ordering
				orderBy + " DESC" // Ordering
		);
		ArrayList<ImageData> imagePath;
		if (cur.moveToFirst()) {
			String folderName, folderId, date;
			int bucketColumn = cur
					.getColumnIndex(ImageColumns.BUCKET_DISPLAY_NAME);
			int bucketIdColumn = cur.getColumnIndex(ImageColumns.BUCKET_ID);
			int dateColumn = cur.getColumnIndex(ImageColumns.DATE_TAKEN);

			ImageData data = null;

			setSelectedFolderId(cur.getString(bucketIdColumn));
			do {
				data = new ImageData();
				data.imagePath = cur.getString(cur
						.getColumnIndex(MediaColumns.DATA));
				if (!data.imagePath.endsWith(".gif")) {
					date = cur.getString(dateColumn);
					folderName = cur.getString(bucketColumn);
					folderId = cur.getString(bucketIdColumn);
					if (!allFolder.contains(folderId)) {
						allFolder.add(folderId);
					}
					imagePath = allAlbum.get(folderId);
					if (imagePath == null) {
						imagePath = new ArrayList<ImageData>();
					}
					data.folderName = folderName;
					imagePath.add(data);
					allAlbum.put(folderId, imagePath);
				}
			} while (cur.moveToNext());
		}
	}

	private void loadLib() throws FFmpegNotSupportedException {
		FFmpeg fFmpeg = FFmpeg.getInstance(this);

		fFmpeg.loadBinary(new FFmpegLoadBinaryResponseHandler() {
			@Override
			public void onStart() {
			}

			@Override
			public void onFinish() {
			}

			@Override
			public void onSuccess(String cpuType) {
				Log.e("ffmpeg", cpuType);
			}

			@Override
			public void onFailure(String cpuType) {
				Log.e("ffmpeg", cpuType);
			}

			@Override
			public void onSuccess() {
			}

			@Override
			public void onFailure() {
			}
		});
	}

	public Typeface getApplicationTypeFace() {
		return null;
	}

	public void clearAllSelection() {
		videoImages.clear();
		allAlbum = null;
		getSelectedImages().clear();
		System.gc();
		getFolderList();
	}

	public void setCurrentTheme(String currentTheme) {
		SharedPreferences.Editor editor = getSharedPreferences("theme",
				MODE_PRIVATE).edit();
		editor.putString("current_theme", currentTheme);
		editor.commit();
	}

	public String getCurrentTheme() {
		return getSharedPreferences("theme", MODE_PRIVATE).getString(
				"current_theme", THEMES.Shine.toString());
	}

	private OnProgressReceiver onProgressReceiver;

	public void setOnProgressReceiver(OnProgressReceiver onProgressReceiver) {
		this.onProgressReceiver = onProgressReceiver;
	}

	public OnProgressReceiver getOnProgressReceiver() {
		return onProgressReceiver;
	}

	public static boolean isMyServiceRunning(Context context,
			Class<?> serviceClass) {
		ActivityManager manager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if (serviceClass.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	int frame = -1;

	public void setFrame(int data) {
		frame = data;
	}

	public int getFrame() {
		return frame;
	}
}