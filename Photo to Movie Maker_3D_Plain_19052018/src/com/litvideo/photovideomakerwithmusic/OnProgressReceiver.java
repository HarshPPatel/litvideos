package com.litvideo.photovideomakerwithmusic;

public interface OnProgressReceiver {
	public void onImageProgressFrameUpdate(float progress);
	public void onVideoProgressFrameUpdate(float progress);
	public void onProgressFinish(String videoPath);
	
}
