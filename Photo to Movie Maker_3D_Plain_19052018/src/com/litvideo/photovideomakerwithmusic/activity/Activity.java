package com.litvideo.photovideomakerwithmusic.activity;

import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.service.CreateVideoService;
import com.litvideo.photovideomakerwithmusic.service.ImageCreatorService;
import com.litvideo.photovideomakerwithmusic.util.ActivityAnimUtil;
import com.litvideo.photovideomakerwithmusic.util.PermissionModelUtil;
import com.litvideo.photovideomakerwithmusic.util.Utils;
import com.litvideo.photovideomakerwithmusic.MyApplication;

public class Activity extends AppCompatActivity implements OnClickListener {
	PermissionModelUtil modelUtil;
	private View view;
	private String tag;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		if (isVideoInprocess()) {
			startActivity(new Intent(this, ProgressActivity.class));
			overridePendingTransition(0, 0);
			finish();
			return;
		}

		setContentView(R.layout.activity_launcher);

		init();
		addListener();
	}

	private boolean isVideoInprocess() {
		return MyApplication.isMyServiceRunning(this, CreateVideoService.class)
				|| MyApplication.isMyServiceRunning(this,
						ImageCreatorService.class);
	}

	private void init() {
		modelUtil = new PermissionModelUtil(this);
		if (modelUtil.needPermissionCheck()) {
			modelUtil.showPermissionExplanationThenAuthorization();
		} else {
			MyApplication.getInstance().getFolderList();
		}
	}

	@TargetApi(23)
	@Override
	public void onRequestPermissionsResult(int arg0, @NonNull String[] arg1,
			@NonNull int[] arg2) {
		super.onRequestPermissionsResult(arg0, arg1, arg2);

		if (!modelUtil.needPermissionCheck()) {
			MyApplication.getInstance().getFolderList();
		}
	}

	private void addListener() {
		findViewById(R.id.btnStart).setOnClickListener(this);
		findViewById(R.id.btnViewAlbum).setOnClickListener(this);
		findViewById(R.id.imgGetMore).setOnClickListener(this);
		findViewById(R.id.tvLikeUs).setOnClickListener(this);
		findViewById(R.id.tvRateUs).setOnClickListener(this);
		findViewById(R.id.tvInvite).setOnClickListener(this);
		findViewById(R.id.tvPolicy).setOnClickListener(this);

		findViewById(R.id.imgApp1).setOnClickListener(this);
		findViewById(R.id.imgApp2).setOnClickListener(this);
		findViewById(R.id.imgApp3).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		view = v;
		switch (v.getId()) {
		case R.id.btnStart:
			if (!modelUtil.needPermissionCheck()) {
				MyApplication.isBreak = false;
				MyApplication.getInstance().setMusicData(null);

				loadImageSelectionActivity(view);

			} else {
				modelUtil.showPermissionExplanationThenAuthorization();
			}
			break;
		case R.id.btnViewAlbum:
			if (!modelUtil.needPermissionCheck()) {

				loadVideoAlbumActivity(view);
			} else {
				modelUtil.showPermissionExplanationThenAuthorization();
			}
			break;

		case R.id.tvInvite:
			Toast.makeText(Activity.this, "Invite You friends",
					Toast.LENGTH_SHORT).show();
			Intent sharingIntent = new Intent(
					android.content.Intent.ACTION_SEND);
			sharingIntent.setType("text/plain");
			String shareBody = "Hey I am using this awesome app MOVIE MAKER !\n Create your own movie with this app./n MAKE YOUR MEMORIES MORE MEMORABLE./nWelcome Visit : https://http://play.google.com/"
					+ getPackageName();
			sharingIntent
					.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
			startActivity(Intent.createChooser(sharingIntent, "Share via"));
			break;
		case R.id.tvLikeUs:
			loadLike();
			break;
		case R.id.tvRateUs:
			loadLike();
			break;
		case R.id.tvPolicy:
			loadPrivacy();
			break;

		case R.id.imgGetMore:
//			loadGetMore();
			break;
//		case R.id.imgApp1:
//		case R.id.imgApp2:
//		case R.id.imgApp3:
//			tag = (String) view.getTag();
//			Intent in = new Intent(Intent.ACTION_VIEW);
//			in.setData(Uri.parse("market://details?id=" + tag));
//			startActivity(in);
//			break;
		}

	}

	private void loadPrivacy() {
		Intent intentR = new Intent(Intent.ACTION_VIEW);
		intentR.setData(Uri.parse(Utils.Privacy_policy));
		startActivity(intentR);
	}

	private void loadGetMore() {
		Intent intentG = new Intent(Intent.ACTION_VIEW);
		intentG.setData(Uri
				.parse("market://search?q=pub:" + Utils.DEV_ACC_NAME));
		startActivity(intentG);
	}

	private void loadLike() {
		Uri uri = Uri.parse("market://details?id=" + getPackageName());
		Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
		try {
			startActivity(myAppLinkToMarket);
		} catch (ActivityNotFoundException e) {
			Toast.makeText(this, " unable to find market app",
					Toast.LENGTH_LONG).show();
		}
	}

	private void loadVideoAlbumActivity(View v) {
		ActivityAnimUtil.startActivitySafely(v, new Intent(Activity.this,
				VideoAlbumActivity.class));
	}

	private void loadImageSelectionActivity(View v) {
		ActivityAnimUtil.startActivitySafely(v, new Intent(Activity.this,
				ImageSelectionActivity.class));
	}
}