package com.litvideo.photovideomakerwithmusic.activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.litvideo.photovideomakerwithmusic.R;

public class GetMoreActivity extends Activity implements OnClickListener {

	private String tag;
	Animation am1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.getmore);

		addListener();

		am1 = AnimationUtils.loadAnimation(this, R.anim.rotate_my);
		findViewById(R.id.imgApp1).startAnimation(am1);
		findViewById(R.id.imgApp2).startAnimation(am1);
		findViewById(R.id.imgApp3).startAnimation(am1);
		findViewById(R.id.imgApp4).startAnimation(am1);
		findViewById(R.id.imgApp5).startAnimation(am1);
		findViewById(R.id.imgApp6).startAnimation(am1);
		findViewById(R.id.imgApp7).startAnimation(am1);
		findViewById(R.id.imgApp8).startAnimation(am1);
		findViewById(R.id.imgApp9).startAnimation(am1);
		findViewById(R.id.imgApp10).startAnimation(am1);
		findViewById(R.id.imgApp11).startAnimation(am1);
		findViewById(R.id.imgApp12).startAnimation(am1);
	}

	private void addListener() {
		findViewById(R.id.imgApp1).setOnClickListener(this);
		findViewById(R.id.imgApp2).setOnClickListener(this);
		findViewById(R.id.imgApp3).setOnClickListener(this);
		findViewById(R.id.imgApp4).setOnClickListener(this);
		findViewById(R.id.imgApp5).setOnClickListener(this);
		findViewById(R.id.imgApp6).setOnClickListener(this);
		findViewById(R.id.imgApp7).setOnClickListener(this);
		findViewById(R.id.imgApp8).setOnClickListener(this);
		findViewById(R.id.imgApp9).setOnClickListener(this);
		findViewById(R.id.imgApp10).setOnClickListener(this);
		findViewById(R.id.imgApp11).setOnClickListener(this);
		findViewById(R.id.imgApp12).setOnClickListener(this);

		findViewById(R.id.btn_Exit).setOnClickListener(this);
		findViewById(R.id.btn_RateUs).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imgApp1:
		case R.id.imgApp2:
		case R.id.imgApp3:
		case R.id.imgApp4:
		case R.id.imgApp5:
		case R.id.imgApp6:
		case R.id.imgApp7:
		case R.id.imgApp8:
		case R.id.imgApp9:
		case R.id.imgApp10:
		case R.id.imgApp11:
		case R.id.imgApp12:
			tag = (String) v.getTag();
			Intent intent = new Intent("android.intent.action.VIEW");
			intent.setData(Uri.parse("market://details?id=" + tag));
			startActivity(intent);
			return;
		case R.id.btn_Exit:
			exitActivity();
			return;
		case R.id.btn_RateUs:
			loadRateUs();
			return;
		default:
			return;
		}

	}

	public void onBackPressed() {
		GetMoreActivity.this.finish();

	}

	private void loadRateUs() {
		Uri uri = Uri.parse("market://details?id=" + getPackageName());
		Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
		try {
			startActivity(myAppLinkToMarket);
		} catch (ActivityNotFoundException e) {
			Toast.makeText(this, " unable to find market app",
					Toast.LENGTH_LONG).show();
		}
	}

	public void exitActivity() {
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}

}