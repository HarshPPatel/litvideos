package com.litvideo.photovideomakerwithmusic.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.adapters.ImageEditAdapter;
import com.litvideo.photovideomakerwithmusic.util.ActivityAnimUtil;
import com.litvideo.photovideomakerwithmusic.view.EmptyRecyclerView;
import com.litvideo.photovideomakerwithmusic.MyApplication;

public class ImageEditActivity extends AppCompatActivity {

	private MyApplication application;
	private ImageEditAdapter imageEditAdapter;
	private EmptyRecyclerView rvSelectedImages;
	private Toolbar toolbar;
	public boolean isFromPreview = false;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
		        Window w = getWindow();
		        w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
		        w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
		    }
		setContentView(R.layout.activity_movie_album);

		isFromPreview = getIntent().hasExtra(
				ImageSelectionActivity.EXTRA_FROM_PREVIEW);
		application = MyApplication.getInstance();
		application.isEditModeEnable = true;

		bindView();
		init();
	}

	private void bindView() {
		rvSelectedImages = (EmptyRecyclerView) findViewById(R.id.rvVideoAlbum);
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setTitle("Swap Images");
	}

	private void init() {
		GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2,
				GridLayoutManager.VERTICAL, false);
		imageEditAdapter = new ImageEditAdapter(this);
		rvSelectedImages.setLayoutManager(gridLayoutManager);
		rvSelectedImages.setItemAnimator(new DefaultItemAnimator());
		rvSelectedImages.setEmptyView(findViewById(R.id.list_empty));
		rvSelectedImages.setAdapter(imageEditAdapter);

		ItemTouchHelper ith = new ItemTouchHelper(_ithCallback);
		ith.attachToRecyclerView(rvSelectedImages);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}

	protected void onResume() {
		super.onRestart();
		if (imageEditAdapter != null) {
			imageEditAdapter.notifyDataSetChanged();
		}
	};

	ItemTouchHelper.Callback _ithCallback = new ItemTouchHelper.Callback() {

		public void onSelectedChanged(RecyclerView.ViewHolder viewHolder,
				int actionState) {
			Log.e("action", "actoinState " + actionState);
			if (actionState == 0) {
				imageEditAdapter.notifyDataSetChanged();
			}
		};

		@Override
		public void onMoved(RecyclerView recyclerView,
				RecyclerView.ViewHolder viewHolder, int fromPos,
				RecyclerView.ViewHolder target, int toPos, int x, int y) {
			imageEditAdapter.swap(viewHolder.getAdapterPosition(),
					target.getAdapterPosition());
			application.min_pos = Math.min(application.min_pos,
					Math.min(fromPos, toPos));
			MyApplication.isBreak = true;
		};

		@Override
		public boolean onMove(RecyclerView recyclerView,
				RecyclerView.ViewHolder viewHolder,
				RecyclerView.ViewHolder target) {
			return true;
		}

		@Override
		public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
		}

		@Override
		public int getMovementFlags(RecyclerView recyclerView,
				RecyclerView.ViewHolder viewHolder) {
			return makeFlag(ItemTouchHelper.ACTION_STATE_DRAG,
					ItemTouchHelper.DOWN | ItemTouchHelper.UP
							| ItemTouchHelper.START | ItemTouchHelper.END);
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_selection, menu);

		menu.removeItem(R.id.menu_clear);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_done:
			done();
			break;
		case android.R.id.home:
			if (isFromPreview) {
				done();
			} else {
				super.onBackPressed();
			}
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void done() {
		application.isEditModeEnable = false;
		if (isFromPreview) {
			setResult(RESULT_OK);
			finish();
			return;
		} else {

			loadPreviewActivity();
		}
	}

	private void loadPreviewActivity() {
		Intent intent = new Intent(this, PreviewActivity.class);
		ActivityAnimUtil.startActivitySafely(toolbar, intent);
	}

	public void onBackPressed() {
		if (isFromPreview) {
			done();
		} else {
			super.onBackPressed();
		}
	};
}
