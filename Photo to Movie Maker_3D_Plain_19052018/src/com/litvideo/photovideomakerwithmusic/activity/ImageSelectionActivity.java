package com.litvideo.photovideomakerwithmusic.activity;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.adapters.AlbumAdapterById;
import com.litvideo.photovideomakerwithmusic.adapters.ImageByAlbumAdapter;
import com.litvideo.photovideomakerwithmusic.adapters.OnItemClickListner;
import com.litvideo.photovideomakerwithmusic.adapters.SelectedImageAdapter;
import com.litvideo.photovideomakerwithmusic.data.ImageData;
import com.litvideo.photovideomakerwithmusic.view.EmptyRecyclerView;
import com.litvideo.photovideomakerwithmusic.view.ExpandIconView;
import com.litvideo.photovideomakerwithmusic.view.VerticalSlidingPanel;
import com.litvideo.photovideomakerwithmusic.view.VerticalSlidingPanel.PanelSlideListener;
import com.litvideo.photovideomakerwithmusic.MyApplication;

public class ImageSelectionActivity extends AppCompatActivity implements
		PanelSlideListener {
	private RecyclerView rvAlbum, rvAlbumImages;

	private AlbumAdapterById albumAdapter;
	private ImageByAlbumAdapter albumImagesAdapter;
	private SelectedImageAdapter selectedImageAdapter;
	private VerticalSlidingPanel panel;
	private View parent;
	private Toolbar toolbar;
	private ExpandIconView expandIcon;
	private Button btnClear;
	private TextView tvImageCount;
	private MyApplication application;
	public boolean isFromPreview = false;
	private EmptyRecyclerView rvSelectedImage;
	public static final String EXTRA_FROM_PREVIEW = "extra_from_preview";

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
	        Window w = getWindow();
	        w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
	        w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
	    }
		setContentView(R.layout.image_select_activity);

		application = MyApplication.getInstance();
		isFromPreview = getIntent().hasExtra(EXTRA_FROM_PREVIEW);

		bindView();
		init();
		addListner();
	}

	private void init() {
		setSupportActionBar(toolbar);
		albumAdapter = new AlbumAdapterById(this);
		albumImagesAdapter = new ImageByAlbumAdapter(this);
		selectedImageAdapter = new SelectedImageAdapter(this);

		RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(
				getApplicationContext(), LinearLayoutManager.VERTICAL, false);
		rvAlbum.setLayoutManager(mLayoutManager);
		rvAlbum.setItemAnimator(new DefaultItemAnimator());
		rvAlbum.setAdapter(albumAdapter);

		RecyclerView.LayoutManager gridLayputManager = new GridLayoutManager(
				getApplicationContext(), 3);
		rvAlbumImages.setLayoutManager(gridLayputManager);
		rvAlbumImages.setItemAnimator(new DefaultItemAnimator());
		rvAlbumImages.setAdapter(albumImagesAdapter);

		RecyclerView.LayoutManager gridLayputManager1 = new GridLayoutManager(
				getApplicationContext(), 4);
		rvSelectedImage.setLayoutManager(gridLayputManager1);
		rvSelectedImage.setItemAnimator(new DefaultItemAnimator());
		rvSelectedImage.setAdapter(selectedImageAdapter);
		rvSelectedImage.setEmptyView(findViewById(R.id.list_empty));
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		tvImageCount.setText(String.valueOf(application.getSelectedImages()
				.size()));

	}

	private void bindView() {
		tvImageCount = (TextView) findViewById(R.id.tvImageCount);
		expandIcon = (ExpandIconView) findViewById(R.id.settings_drag_arrow);
		rvAlbum = (RecyclerView) findViewById(R.id.rvAlbum);
		rvAlbumImages = (RecyclerView) findViewById(R.id.rvImageAlbum);
		rvSelectedImage = (EmptyRecyclerView) findViewById(R.id.rvSelectedImagesList);
		panel = (VerticalSlidingPanel) findViewById(R.id.overview_panel);
		panel.setEnableDragViewTouchEvents(true);
		panel.setDragView(findViewById(R.id.settings_pane_header));
		panel.setPanelSlideListener(this);
		parent = findViewById(R.id.default_home_screen_panel);
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		btnClear = (Button) findViewById(R.id.btnClear);
	}

	private void addListner() {
		btnClear.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				clearData();
			}

		});
		albumAdapter.setOnItemClickListner(new OnItemClickListner<Object>() {

			@Override
			public void onItemClick(View view, Object item) {
				albumImagesAdapter.notifyDataSetChanged();
			}
		});
		albumImagesAdapter
				.setOnItemClickListner(new OnItemClickListner<Object>() {

					@Override
					public void onItemClick(View view, Object item) {

						tvImageCount.setText(String.valueOf(application
								.getSelectedImages().size()));
						selectedImageAdapter.notifyDataSetChanged();

					}
				});
		selectedImageAdapter
				.setOnItemClickListner(new OnItemClickListner<Object>() {

					@Override
					public void onItemClick(View view, Object item) {
						tvImageCount.setText(String.valueOf(application
								.getSelectedImages().size()));
						albumImagesAdapter.notifyDataSetChanged();
					}
				});
	}

	boolean isPause = false;

	@Override
	protected void onResume() {
		super.onResume();
		if (isPause) {
			isPause = false;
			tvImageCount.setText(String.valueOf(application.getSelectedImages()
					.size()));
			albumImagesAdapter.notifyDataSetChanged();
			selectedImageAdapter.notifyDataSetChanged();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		isPause = true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.menu_selection, menu);

		if (isFromPreview)
			menu.removeItem(R.id.menu_clear);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_done:
			if (application.getSelectedImages().size() > 2) {
				if (isFromPreview) {
					setResult(RESULT_OK);
					finish();
					return false;
				} else {
					loadImageEditActivity();
				}
			} else {
				Toast.makeText(this,
						"Select more than 2 Images for create video",
						Toast.LENGTH_LONG).show();
			}
			break;
		case android.R.id.home:
			onBackPressed();
			break;
		case R.id.menu_clear:
			clearData();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void loadImageEditActivity() {
		Intent intent = new Intent(this, ImageEditActivity.class);
		startActivity(intent);
	}

	@Override
	public void onPanelSlide(View panel, float slideOffset) {
		if (expandIcon != null)
			expandIcon.setFraction(slideOffset, false);
		if (slideOffset >= 0.005f) {
			if (parent != null && parent.getVisibility() != View.VISIBLE) {
				parent.setVisibility(View.VISIBLE);
			}
		} else {
			if (parent != null && parent.getVisibility() == View.VISIBLE) {
				parent.setVisibility(View.GONE);
			}
		}

	}

	@Override
	public void onPanelCollapsed(View panel) {

		if (parent != null) {
			parent.setVisibility(View.VISIBLE);
		}
		selectedImageAdapter.isExpanded = false;
		selectedImageAdapter.notifyDataSetChanged();
	}

	@Override
	public void onPanelExpanded(View panel) {
		// TODO Auto-generated method stub
		if (parent != null) {
			parent.setVisibility(View.GONE);
		}
		selectedImageAdapter.isExpanded = true;
		selectedImageAdapter.notifyDataSetChanged();
	}

	@Override
	public void onPanelAnchored(View panel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPanelShown(View panel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onBackPressed() {
		if (panel.isExpanded()) {
			panel.collapsePane();
			return;
		}
		if (!isFromPreview) {
			application.videoImages.clear();
			application.clearAllSelection();
		} else {
			setResult(RESULT_OK);
			finish();
			return;
		}
		super.onBackPressed();
	}

	private void clearData() {
		ArrayList<ImageData> selectedImages = application.getSelectedImages();
		for (int i = selectedImages.size() - 1; i >= 0; i--) {
			application.removeSelectedImage(i);
		}
		tvImageCount.setText("0");
		selectedImageAdapter.notifyDataSetChanged();
		albumImagesAdapter.notifyDataSetChanged();

	}
}
