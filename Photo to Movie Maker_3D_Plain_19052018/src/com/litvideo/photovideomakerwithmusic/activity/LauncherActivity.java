package com.litvideo.photovideomakerwithmusic.activity;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.adapters.ImageAdapter;
import com.litvideo.photovideomakerwithmusic.anim.CubeInRotationTransformation;
import com.litvideo.photovideomakerwithmusic.anim.FanTransformation;
import com.litvideo.photovideomakerwithmusic.anim.HingeTransformation;
import com.litvideo.photovideomakerwithmusic.anim.SimpleTransformation;
import com.litvideo.photovideomakerwithmusic.service.CreateVideoService;
import com.litvideo.photovideomakerwithmusic.service.ImageCreatorService;
import com.litvideo.photovideomakerwithmusic.util.ActivityAnimUtil;
import com.litvideo.photovideomakerwithmusic.util.PermissionModelUtil;
import com.litvideo.photovideomakerwithmusic.util.Utils;
import com.litvideo.photovideomakerwithmusic.MyApplication;

public class LauncherActivity extends AppCompatActivity implements
		OnClickListener {
	PermissionModelUtil modelUtil;
	private View view;
	private String tag;
	private Animation am1;
	ViewPager mViewPager;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		if (isVideoInprocess()) {
			startActivity(new Intent(this, ProgressActivity.class));
			overridePendingTransition(0, 0);
			finish();
			return;
		}
		 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
		        Window w = getWindow();
		        w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
		        w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
		    }
		setContentView(R.layout.activity_launcher);
		
		init();
		addListener();
		
		FanTransformation fanTransformation = new FanTransformation();
		HingeTransformation hingeTransformation = new HingeTransformation();
		SimpleTransformation simpleTransformation = new SimpleTransformation();
		 CubeInRotationTransformation cubeInRotationTransformation = new CubeInRotationTransformation();
		
		   mViewPager = (ViewPager) findViewById(R.id.viewPage);  
	        ImageAdapter adapterView = new ImageAdapter(this);  
	        mViewPager.setAdapter(adapterView); 
	        
	        mViewPager.setPageTransformer(true, cubeInRotationTransformation);
	        
	        Timer timer = new Timer();
	        timer.scheduleAtFixedRate(new MyTimerTask(), 2000, 4000);
	        
	}

	private boolean isVideoInprocess() {
		return MyApplication.isMyServiceRunning(this, CreateVideoService.class)
				|| MyApplication.isMyServiceRunning(this,
						ImageCreatorService.class);
	}

	private void init() {
		modelUtil = new PermissionModelUtil(this);
		if (modelUtil.needPermissionCheck()) {
			modelUtil.showPermissionExplanationThenAuthorization();
		} else {
			MyApplication.getInstance().getFolderList();
		}
	}

	@TargetApi(23)
	@Override
	public void onRequestPermissionsResult(int arg0, @NonNull String[] arg1,
			@NonNull int[] arg2) {
		super.onRequestPermissionsResult(arg0, arg1, arg2);

		if (!modelUtil.needPermissionCheck()) {
			MyApplication.getInstance().getFolderList();
		}
	}

	private void addListener() {
		am1 = AnimationUtils.loadAnimation(this, R.anim.rotate_my);
		findViewById(R.id.imgApp1).startAnimation(am1);
		findViewById(R.id.imgApp2).startAnimation(am1);
		findViewById(R.id.imgApp3).startAnimation(am1);
		findViewById(R.id.imgApp4).startAnimation(am1);
		
		findViewById(R.id.btnStart).setOnClickListener(this);
		findViewById(R.id.btnViewAlbum).setOnClickListener(this);
		findViewById(R.id.imgGetMore).setOnClickListener(this);
		findViewById(R.id.tvLikeUs).setOnClickListener(this);
		findViewById(R.id.tvRateUs).setOnClickListener(this);
		findViewById(R.id.tvInvite).setOnClickListener(this);
		findViewById(R.id.tvPolicy).setOnClickListener(this);

		findViewById(R.id.imgApp1).setOnClickListener(this);
		findViewById(R.id.imgApp2).setOnClickListener(this);
		findViewById(R.id.imgApp3).setOnClickListener(this);
		findViewById(R.id.imgApp4).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		view = v;
		switch (v.getId()) {
		case R.id.btnStart:
			if (!modelUtil.needPermissionCheck()) {
				MyApplication.isBreak = false;
				MyApplication.getInstance().setMusicData(null);

				loadImageSelectionActivity(view);

			} else {
				modelUtil.showPermissionExplanationThenAuthorization();
			}
			break;
		case R.id.btnViewAlbum:
			if (!modelUtil.needPermissionCheck()) {

				loadVideoAlbumActivity(view);
			} else {
				modelUtil.showPermissionExplanationThenAuthorization();
			}
			break;

		case R.id.tvInvite:
			Toast.makeText(LauncherActivity.this, "Invite You friends",
					Toast.LENGTH_SHORT).show();
			Intent sharingIntent = new Intent(
					android.content.Intent.ACTION_SEND);
			sharingIntent.setType("text/plain");
			String shareBody = "Hey I am using this awesome app MOVIE MAKER !\n Create your own movie with this app./n MAKE YOUR MEMORIES MORE MEMORABLE./nWelcome Visit : https://http://play.google.com/"
					+ getPackageName();
			sharingIntent
					.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
			startActivity(Intent.createChooser(sharingIntent, "Share via"));
			break;
		case R.id.tvLikeUs:
			loadLike();
			break;
		case R.id.tvRateUs:
			loadLike();
			break;
		case R.id.tvPolicy:
			loadPrivacy();
			break;

		case R.id.imgGetMore:
//			loadGetMore();
			Intent start = new Intent(getApplicationContext(), SettingActivity.class);
			startActivity(start);
			overridePendingTransition(0, 0);
			break;
		case R.id.imgApp1:
		case R.id.imgApp2:
		case R.id.imgApp3:
		case R.id.imgApp4:
			tag = (String) view.getTag();
			Intent in = new Intent(Intent.ACTION_VIEW);
			in.setData(Uri.parse("market://details?id=" + tag));
			startActivity(in);
			break;
		}

	}

	

	public void exitCustomDialog() {

		LayoutInflater factory = LayoutInflater.from(this);
		final View deleteView = factory.inflate(R.layout.custom_dialog, null);

		final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
		deleteDialog.setView(deleteView);

		final TextView tvTitle = (TextView) deleteView
				.findViewById(R.id.tv_title);
		final TextView tvMessage = (TextView) deleteView
				.findViewById(R.id.tv_file_name);

		tvTitle.setText(getString(R.string.alert_title));
		tvMessage.setText(getString(R.string.alert_message));

		final Button btnYes = (Button) deleteView.findViewById(R.id.btn_delete);
		final Button btnNo = (Button) deleteView.findViewById(R.id.btn_cancel);

		btnYes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				deleteDialog.dismiss();
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra("EXIT", "EXIT");
				startActivity(intent);
				finish();
			}
		});

		btnNo.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				deleteDialog.dismiss();
			}
		});
		deleteDialog.show();
	}

	private void loadPrivacy() {
		Intent intentR = new Intent(Intent.ACTION_VIEW);
		intentR.setData(Uri.parse(Utils.Privacy_policy));
		startActivity(intentR);
	}

	private void loadGetMore() {
		Intent intentG = new Intent(Intent.ACTION_VIEW);
		intentG.setData(Uri
				.parse("market://search?q=pub:" + Utils.DEV_ACC_NAME));
		startActivity(intentG);
	}

	private void loadLike() {
		Uri uri = Uri.parse("market://details?id=" + getPackageName());
		Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
		try {
			startActivity(myAppLinkToMarket);
		} catch (ActivityNotFoundException e) {
			Toast.makeText(this, " unable to find market app",
					Toast.LENGTH_LONG).show();
		}
	}

	private void loadVideoAlbumActivity(View v) {
		ActivityAnimUtil.startActivitySafely(v, new Intent(
				LauncherActivity.this, VideoAlbumActivity.class));
	}

	private void loadImageSelectionActivity(View v) {
		ActivityAnimUtil.startActivitySafely(v, new Intent(
				LauncherActivity.this, ImageSelectionActivity.class));
	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		// exitCustomDialog();
//		 Intent in = new Intent(LauncherActivity.this,GetMoreActivity.class);
//		 startActivity(in);
//		finish();
	}
	
	public class MyTimerTask extends TimerTask{
		
			public void run() {
				LauncherActivity.this.runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						if (mViewPager.getCurrentItem() == 0) {
							mViewPager.setCurrentItem(1);
						}else if (mViewPager.getCurrentItem() == 1) {
							mViewPager.setCurrentItem(2);
						}else if (mViewPager.getCurrentItem() == 2) {
							mViewPager.setCurrentItem(3);
						}else if (mViewPager.getCurrentItem() == 3) {
							mViewPager.setCurrentItem(4);
						}else if (mViewPager.getCurrentItem() == 4) {
							mViewPager.setCurrentItem(5);
						}
						else {
							mViewPager.setCurrentItem(0);
						}
					}
				});
			}
	}
	
	
}