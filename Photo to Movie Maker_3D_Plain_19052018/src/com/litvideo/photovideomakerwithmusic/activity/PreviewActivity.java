package com.litvideo.photovideomakerwithmusic.activity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.signature.MediaStoreSignature;
import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.adapters.FrameAdapter;
import com.litvideo.photovideomakerwithmusic.adapters.MoviewThemeAdapter;
import com.litvideo.photovideomakerwithmusic.data.ImageData;
import com.litvideo.photovideomakerwithmusic.data.MusicData;
import com.litvideo.photovideomakerwithmusic.service.CreateVideoService;
import com.litvideo.photovideomakerwithmusic.service.ImageCreatorService;
import com.litvideo.photovideomakerwithmusic.MyApplication;
import com.movienaker.movie.themes.THEMES;
import com.videolib.libffmpeg.FileUtils;

public class PreviewActivity extends AppCompatActivity implements
		OnClickListener, OnSeekBarChangeListener {
	private Handler handler = new Handler();
	private LockRunnable lockRunnable = new LockRunnable();
	private float seconds = 2.0f;
	private MyApplication application;
	private ImageView ivPreview;
	private SeekBar seekBar;
	private ArrayList<ImageData> arrayList;
	private TextView tvEndTime;
	private RequestManager glide;
	private MediaPlayer mPlayer;
	private Toolbar toolbar;
	private RecyclerView rvThemes;
	private TextView tvTime;
	private View ivPlayPause;
	private BottomSheetBehavior<View> behavior;
	LayoutInflater inflater;
	private View flLoader;
	private ImageView ivFrame;

	private Float duration[] = new Float[] { 1.0f, 1.5f, 2.0f, 2.5f, 3.0f,
			3.5f, 4.0f };

	class LockRunnable implements Runnable {
		ArrayList<ImageData> appList;
		boolean isPause = false;

		public LockRunnable() {
			appList = new ArrayList<>();
		}

		void setAppList(ArrayList<ImageData> appList) {
			this.appList.clear();
			this.appList.addAll(appList);
		}

		@Override
		public void run() {
			displayImage();
			if (!isPause) {
				handler.postDelayed(lockRunnable, Math.round(50f * seconds));
			}
		}

		public boolean isPause() {
			return isPause;
		}

		public void play() {
			isPause = false;
			playMusic();
			handler.postDelayed(lockRunnable, Math.round(50f * seconds));
			Animation animation = new AlphaAnimation(1f, 0f);
			animation.setDuration(500);
			animation.setFillAfter(true);
			animation.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
					ivPlayPause.setVisibility(View.VISIBLE);
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
				}

				@Override
				public void onAnimationEnd(Animation animation) {
					ivPlayPause.setVisibility(View.INVISIBLE);
				}
			});
			ivPlayPause.startAnimation(animation);
			if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
				behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
				return;
			}
		}

		public void pause() {
			isPause = true;
			pauseMusic();
			Animation animation = new AlphaAnimation(0f, 1f);
			animation.setDuration(500);
			animation.setFillAfter(true);
			ivPlayPause.startAnimation(animation);
			animation.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
					ivPlayPause.setVisibility(View.VISIBLE);
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
				}

				@Override
				public void onAnimationEnd(Animation animation) {
				}
			});
		}

		public void stop() {
			pause();
			i = 0;

			if (mPlayer != null) {
				mPlayer.stop();
			}
			reinitMusic();
			seekBar.setProgress(i);

		}
	}

	private RecyclerView rvDuration;
	private RecyclerView rvFrame;
	private FrameAdapter frameAdapter;

	private void reinitMusic() {
		final MusicData musicData = application.getMusicData();
		if (musicData != null) {
			mPlayer = MediaPlayer.create(this, Uri.parse(musicData.track_data));
			mPlayer.setLooping(true);
			try {
				mPlayer.prepare();
			} catch (IllegalStateException | IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void playMusic() {
		if (flLoader.getVisibility() != View.VISIBLE) {
			if (mPlayer != null) {
				if (!mPlayer.isPlaying()) {

					mPlayer.start();
				}
			}
		}
	}

	private void pauseMusic() {
		if (mPlayer != null) {
			if (mPlayer.isPlaying()) {
				mPlayer.pause();
			}
		}
	}

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {

		application = MyApplication.getInstance();
		application.videoImages.clear();
		MyApplication.isBreak = false;
		Intent intent = new Intent(getApplicationContext(),
				ImageCreatorService.class);
		intent.putExtra(ImageCreatorService.EXTRA_SELECTED_THEME,
				application.getCurrentTheme());
		startService(intent);
		super.onCreate(savedInstanceState);
		 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
		        Window w = getWindow();
		        w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
		        w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
		    }
		setContentView(R.layout.activity_preview);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		bindView();
		init();
		addListner();
	}

	private void bindView() {
		flLoader = findViewById(R.id.flLoader);
		ivPreview = (ImageView) findViewById(R.id.previewImageView1);
		ivFrame = (ImageView) findViewById(R.id.ivFrame);
		seekBar = (SeekBar) findViewById(R.id.sbPlayTime);
		tvEndTime = (TextView) findViewById(R.id.tvEndTime);
		tvTime = (TextView) findViewById(R.id.tvTime);
		ivPlayPause = findViewById(R.id.ivPlayPause);
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		rvThemes = (RecyclerView) findViewById(R.id.rvThemes);
		rvDuration = (RecyclerView) findViewById(R.id.rvDuration);
		rvFrame = (RecyclerView) findViewById(R.id.rvFrame);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

	}

	private void init() {
		seconds = application.getSecond();
		inflater = LayoutInflater.from(this);
		glide = Glide.with(this);
		application = MyApplication.getInstance();
		arrayList = application.getSelectedImages();
		seekBar.setMax((arrayList.size() - 1) * 30);
		int total = (int) ((arrayList.size() - 1) * seconds);
		tvEndTime.setText(String.format("%02d:%02d", total / 60, total % 60));
		setUpThemeAdapter();
		glide.load(application.getSelectedImages().get(0).imagePath).into(
				ivPreview);
		final View bottomSheet = findViewById(R.id.bottom_sheet);
		behavior = BottomSheetBehavior.from(bottomSheet);
		behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
			@Override
			public void onStateChanged(@NonNull View bottomSheet, int newState) {
				if (newState == BottomSheetBehavior.STATE_EXPANDED
						&& !lockRunnable.isPause()) {
					lockRunnable.pause();
				}
			}

			@Override
			public void onSlide(@NonNull View bottomSheet, float slideOffset) {
				// React to dragging events
			}
		});
		setTheme();
		lockRunnable.play();
	}

	private void setUpThemeAdapter() {
		themeAdapter = new MoviewThemeAdapter(this);
		LayoutManager manager = new LinearLayoutManager(PreviewActivity.this,
				LinearLayoutManager.HORIZONTAL, false);
		GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1,
				GridLayoutManager.HORIZONTAL, false);
		GridLayoutManager gridLayoutManagerFrame = new GridLayoutManager(this,
				1, GridLayoutManager.HORIZONTAL, false);

		rvThemes.setLayoutManager(gridLayoutManager);
		rvThemes.setItemAnimator(new DefaultItemAnimator());
		rvThemes.setAdapter(themeAdapter);

		frameAdapter = new FrameAdapter(this);
		rvFrame.setLayoutManager(gridLayoutManagerFrame);
		rvFrame.setItemAnimator(new DefaultItemAnimator());
		rvFrame.setAdapter(frameAdapter);

		rvDuration.setHasFixedSize(true);
		rvDuration.setLayoutManager(new LinearLayoutManager(this));
		rvDuration.setAdapter(new DurationAdapter());
	}

	
	private void addListner() {
		findViewById(R.id.ibAddImages).setOnClickListener(this);
		findViewById(R.id.video_clicker).setOnClickListener(this);
		seekBar.setOnSeekBarChangeListener(this);
		findViewById(R.id.ibAddMusic).setOnClickListener(this);
		findViewById(R.id.ibAddDuration).setOnClickListener(this);
		findViewById(R.id.ibEditMode).setOnClickListener(this);

	}

	int i = 0;
	boolean isFromTouch = false;
	private MoviewThemeAdapter themeAdapter;
	private final int REQUEST_PICK_AUDIO = 101;
	private final int REQUEST_PICK_IMAGES = 102;
	private final int REQUEST_PICK_EDIT = 103;

	private synchronized void displayImage() {

		try {
			if (i >= seekBar.getMax()) {
				i = 0;
				lockRunnable.stop();
				return;
			}

			if (i > 0 && flLoader.getVisibility() == View.VISIBLE) {
				flLoader.setVisibility(View.GONE);
				if (mPlayer != null) {
					if (!mPlayer.isPlaying()) {
						mPlayer.start();
					}
				}
			}
			seekBar.setSecondaryProgress(application.videoImages.size());
			if (seekBar.getProgress() >= seekBar.getSecondaryProgress()) {
				// pauseMusic();
				return;
			} else {
				// playMusic();
			}
			i = i % application.videoImages.size();
			String imageData = application.videoImages.get(i);
			// new SimpleTarget<Bitmap>(640,360 ) {}
			glide.load(imageData)
					.asBitmap()
					.signature(
							new MediaStoreSignature("image/*", System
									.currentTimeMillis(), 0))
					.diskCacheStrategy(DiskCacheStrategy.SOURCE)
					.into(new SimpleTarget<Bitmap>() {
						@Override
						public void onResourceReady(Bitmap resource,
								GlideAnimation glideAnimation) {
							ivPreview.setImageBitmap(resource);
						}
					});
			i++;
			if (!isFromTouch) {
				seekBar.setProgress(i);
			}
		} catch (Exception e) {
			glide = Glide.with(this);
		}
		int j = (int) (i / (15 * 2.0f) * seconds);
		int mm = j / 60;
		int ss = j % 60;
		tvTime.setText(String.format("%02d:%02d", mm, ss));
		int total = (int) ((arrayList.size() - 1) * seconds);
		tvEndTime.setText(String.format("%02d:%02d", total / 60, total % 60));
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		switch (v.getId()) {
		case R.id.video_clicker:
			if (lockRunnable.isPause()) {
				lockRunnable.play();
			} else {
				lockRunnable.pause();
			}
			break;
		case R.id.ibAddMusic:
			flLoader.setVisibility(View.GONE);
			intent = new Intent(this, SongEditActivity.class);
			startActivityForResult(intent, REQUEST_PICK_AUDIO);
			break;
		case R.id.ibAddImages:
			flLoader.setVisibility(View.GONE);
			MyApplication.isBreak = true;
			application.isEditModeEnable = true;
			lastData.clear();
			lastData.addAll(arrayList);
			intent = new Intent(this, ImageSelectionActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
			intent.putExtra(ImageSelectionActivity.EXTRA_FROM_PREVIEW, true);
			startActivityForResult(intent, REQUEST_PICK_IMAGES);
			break;
		case R.id.ibAddDuration:
			behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
			break;

		case R.id.ibEditMode:
			flLoader.setVisibility(View.GONE);
			application.isEditModeEnable = true;
			lockRunnable.pause();

			startActivityForResult(
					new Intent(this, ImageEditActivity.class).putExtra(
							ImageSelectionActivity.EXTRA_FROM_PREVIEW, true),
					REQUEST_PICK_EDIT);

			break;
		default:
			break;
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		lockRunnable.pause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_selection, menu);

		menu.removeItem(R.id.menu_clear);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_done:
			handler.removeCallbacks(lockRunnable);
			Intent intent = new Intent(this, CreateVideoService.class);
			startService(intent);
			Intent intent2 = new Intent(application, ProgressActivity.class);
			intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent2);
			finish();
			break;
		case android.R.id.home:
			onBackPressed();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		application.isEditModeEnable = false;
		if (resultCode == RESULT_OK) {
			int total;
			switch (requestCode) {
			case REQUEST_PICK_IMAGES:
				if (isNeedRestart()) {
					Log.e("isNeedRestart", "isNeedRestart true");
					Intent intent = new Intent(getApplicationContext(),
							ImageCreatorService.class);
					stopService(intent);
					lockRunnable.stop();
					seekBar.postDelayed(new Runnable() {
						@Override
						public void run() {
							MyApplication.isBreak = false;
							application.videoImages.clear();
							application.min_pos = Integer.MAX_VALUE;
							Intent intent = new Intent(getApplicationContext(),
									ImageCreatorService.class);
							intent.putExtra(
									ImageCreatorService.EXTRA_SELECTED_THEME,
									application.getCurrentTheme());
							startService(intent);
						}
					}, 1000);
					total = (int) ((arrayList.size() - 1) * seconds);
					arrayList = application.getSelectedImages();
					seekBar.setMax((application.getSelectedImages().size() - 1) * 30);
					tvEndTime.setText(String.format("%02d:%02d", total / 60,
							total % 60));
					return;

				} else if (ImageCreatorService.isImageComplate) {
					Log.e("isNeedRestart", "Service complate");
					MyApplication.isBreak = false;
					application.videoImages.clear();
					application.min_pos = Integer.MAX_VALUE;
					Intent intent = new Intent(getApplicationContext(),
							ImageCreatorService.class);
					intent.putExtra(ImageCreatorService.EXTRA_SELECTED_THEME,
							application.getCurrentTheme());
					startService(intent);
					i = 0;
					seekBar.setProgress(0);
				}
				total = (int) ((arrayList.size() - 1) * seconds);
				arrayList = application.getSelectedImages();
				seekBar.setMax((application.getSelectedImages().size() - 1) * 30);
				tvEndTime.setText(String.format("%02d:%02d", total / 60,
						total % 60));
				break;
			case REQUEST_PICK_AUDIO:
				application.isFromSdCardAudio = true;
				i = 0;
				reinitMusic();
				break;
			case REQUEST_PICK_EDIT:
				lockRunnable.stop();
				if (ImageCreatorService.isImageComplate
						|| !MyApplication.isMyServiceRunning(application,
								ImageCreatorService.class)) {
					MyApplication.isBreak = false;
					application.videoImages.clear();
					application.min_pos = Integer.MAX_VALUE;
					Intent intent = new Intent(getApplicationContext(),
							ImageCreatorService.class);
					intent.putExtra(ImageCreatorService.EXTRA_SELECTED_THEME,
							application.getCurrentTheme());
					startService(intent);

				}
				i = 0;
				seekBar.setProgress(i);
				arrayList = application.getSelectedImages();
				total = (int) ((arrayList.size() - 1) * seconds);
				seekBar.setMax((application.getSelectedImages().size() - 1) * 30);
				tvEndTime.setText(String.format("%02d:%02d", total / 60,
						total % 60));
				break;

			default:
				break;
			}

		}
	}

	ArrayList<ImageData> lastData = new ArrayList<ImageData>();

	private boolean isNeedRestart() {
		if (lastData.size() > application.getSelectedImages().size()) {
			MyApplication.isBreak = true;
			Log.e("isNeedRestart", "isNeedRestart size");
			return true;
		}
		for (int i = 0; i < lastData.size(); i++) {

			Log.e("isNeedRestart", lastData.get(i).imagePath + "___ "
					+ application.getSelectedImages().get(i).imagePath);
			if (!lastData.get(i).imagePath.equals(application
					.getSelectedImages().get(i).imagePath)) {
				MyApplication.isBreak = true;
				return true;
			}
		}
		return false;
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		i = progress;
		if (isFromTouch) {
			seekBar.setProgress(Math.min(progress,
					seekBar.getSecondaryProgress()));
			displayImage();
			seekMediaPlayer();
		}
	}

	private void seekMediaPlayer() {
		if (mPlayer != null) {
			try {
				int duration = (int) (i / (15 * 2.0f) * seconds * 1000)
						% mPlayer.getDuration();
				mPlayer.seekTo(duration);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		isFromTouch = true;
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		isFromTouch = false;
	}

	public void reset() {
		MyApplication.isBreak = false;
		application.videoImages.clear();
		handler.removeCallbacks(lockRunnable);
		lockRunnable.stop();
		Glide.get(this).clearMemory();
		new Thread() {
			@Override
			public void run() {
				Glide.get(PreviewActivity.this).clearDiskCache();
			};
		}.start();
		FileUtils.deleteTempDir();
		glide = Glide.with(this);
		flLoader.setVisibility(View.VISIBLE);
		setTheme();

	}

	private class DurationAdapter extends
			RecyclerView.Adapter<DurationAdapter.ViewHolder> {

		public class ViewHolder extends RecyclerView.ViewHolder {
			CheckedTextView checkedTextView;

			public ViewHolder(View view) {
				super(view);
				checkedTextView = (CheckedTextView) view
						.findViewById(android.R.id.text1);
			}
		}

		@Override
		public int getItemCount() {
			return duration.length;
		}

		@Override
		public void onBindViewHolder(ViewHolder holder, int pos) {
			final float dur = duration[pos];
			holder.checkedTextView.setText(String.format("%.1f Second", dur));
			holder.checkedTextView.setChecked(dur == seconds);
			holder.checkedTextView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					seconds = dur;
					application.setSecond(seconds);
					notifyDataSetChanged();
					lockRunnable.play();
				}
			});
		}

		@Override
		public ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
			return new ViewHolder(inflater.inflate(R.layout.duration_list_item,
					parent, false));
		}
	}

	@Override
	public void onBackPressed() {
		if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
			behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
			return;
		}

		onBackDialog();
	}

	private void onBackDialog() {

		new AlertDialog.Builder(this, R.style.Theme_MovieMaker_AlertDialog)
				.setTitle(R.string.app_name)
				.setMessage("Are you sure ? \nYour video is not prepared yet!")
				.setPositiveButton("Go Back",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(
									DialogInterface paramDialogInterface,
									int paramInt) {
								application.videoImages.clear();
								MyApplication.isBreak = true;
								NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
								notificationManager
										.cancel(CreateVideoService.NOTIFICATION_ID);
								PreviewActivity.super.onBackPressed();
							}
						}).setNegativeButton("Stay here", null).create().show();
	}

	public void setTheme() {
		if (!application.isFromSdCardAudio) {
			new Thread() {
				public void run() {
					THEMES themes = application.selectedTheme;
					try {
						FileUtils.TEMP_DIRECTORY_AUDIO.mkdirs();
						File tempFile = new File(
								FileUtils.TEMP_DIRECTORY_AUDIO, "temp.mp3");
						if (tempFile.exists()) {
							FileUtils.deleteFile(tempFile);
						}
						InputStream in = getResources().openRawResource(
								themes.getThemeMusic());
						FileOutputStream out = new FileOutputStream(tempFile);
						byte[] buff = new byte[1024];
						int read = 0;
						while ((read = in.read(buff)) > 0) {
							out.write(buff, 0, read);
						}
						MediaPlayer player = new MediaPlayer();
						player.setDataSource(tempFile.getAbsolutePath());
						player.setAudioStreamType(AudioManager.STREAM_MUSIC);
						player.prepare();
						final MusicData musicData = new MusicData();
						musicData.track_data = tempFile.getAbsolutePath();
						player.setOnPreparedListener(new OnPreparedListener() {
							@Override
							public void onPrepared(MediaPlayer mp) {
								musicData.track_duration = mp.getDuration();
								mp.stop();
							}
						});
						musicData.track_Title = "temp";
						application.setMusicData(musicData);
					} catch (Exception e) {
					}
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							reinitMusic();
							lockRunnable.play();
						}
					});
				};
			}.start();
		} else {
			lockRunnable.play();
		}
	}

	private void startService() {
		MyApplication.isBreak = false;
		application.videoImages.clear();
		application.min_pos = Integer.MAX_VALUE;
		Intent intent = new Intent(getApplicationContext(),
				ImageCreatorService.class);
		intent.putExtra(ImageCreatorService.EXTRA_SELECTED_THEME,
				application.getCurrentTheme());
		startService(intent);
		seekBar.setProgress(0);
		i = 0;
		int total = (int) ((arrayList.size() - 1) * seconds);
		arrayList = application.getSelectedImages();
		seekBar.setMax((application.getSelectedImages().size() - 1) * 30);
		tvEndTime.setText(String.format("%02d:%02d", total / 60, total % 60));
		if (mPlayer != null) {
			mPlayer.seekTo(0);
		}
	}

	int frame;

	public void setFrame(int data) {
		frame = data;
		if (data == -1) {
			ivFrame.setImageDrawable(null);

		} else {
			ivFrame.setImageResource(data);
		}
		application.setFrame(data);
	}

	public int getFrame() {
		return application.getFrame();
	}
}
