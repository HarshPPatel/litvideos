package com.litvideo.photovideomakerwithmusic.activity;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.service.CreateVideoService;
import com.litvideo.photovideomakerwithmusic.util.ActivityAnimUtil;
import com.litvideo.photovideomakerwithmusic.view.CircularFillableLoaders;
import com.litvideo.photovideomakerwithmusic.MyApplication;
import com.litvideo.photovideomakerwithmusic.OnProgressReceiver;

public class ProgressActivity extends AppCompatActivity implements
		OnProgressReceiver {
	private MyApplication application;
	private CircularFillableLoaders circularProgress;
	private TextView tvProgress;

	final float[] from = new float[3], to = new float[3];
	final float[] hsv = new float[3];

	private int startColor, endColor;
	private String newPath;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_progress);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		application = MyApplication.getInstance();

		bindView();
		init();
	}

	private void bindView() {
		circularProgress = (CircularFillableLoaders) findViewById(R.id.circularProgress);
		tvProgress = (TextView) findViewById(R.id.tvProgress);
	}

	private void init() {
		startColor = getResources().getColor(R.color.circle_start);
		endColor = getResources().getColor(R.color.circle_end);
		Color.colorToHSV(startColor, from); // from white
		Color.colorToHSV(endColor, to);
	}

	@Override
	protected void onResume() {
		super.onResume();
		application.setOnProgressReceiver(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		application.setOnProgressReceiver(null);
		if (MyApplication.isMyServiceRunning(this, CreateVideoService.class)) {
			finish();
		}
	}

	@Override
	public void onBackPressed() {
	}

	@Override
	public void onProgressFinish(String videoPath) {
		newPath = videoPath;

		runOnUiThread(new Runnable() {
			public void run() {

				loadVideoPlayActivity(newPath);
			}
		});
	}

	private void loadVideoPlayActivity(String videoPath) {
		Intent intent = new Intent(this, VideoPlayActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(Intent.EXTRA_TEXT, videoPath);
		ActivityAnimUtil.startActivitySafely(circularProgress, intent);
	}

	public Integer evaluate(float fraction, Integer startValue, Integer endValue) {
		int startA, startR, startG, startB;
		int aDelta = (int) ((Color.alpha(endValue) - (startA = Color
				.alpha(startValue))) * fraction);
		int rDelta = (int) ((Color.red(endValue) - (startR = Color
				.red(startValue))) * fraction);
		int gDelta = (int) ((Color.green(endValue) - (startG = Color
				.green(startValue))) * fraction);
		int bDelta = (int) ((Color.blue(endValue) - (startB = Color
				.blue(startValue))) * fraction);
		return Color.argb(startA + aDelta, startR + rDelta, startG + gDelta,
				startB + bDelta);
	}

	float lastProg = 0;
	boolean isComplate = true;

	private synchronized void changeBgColor(float progress) {

		Log.e("progress", "progress__" + progress);

		if (isComplate) {
			final ValueAnimator animate = ValueAnimator.ofFloat(lastProg,
					progress);
			animate.setDuration(300);
			animate.setInterpolator(new LinearInterpolator());
			animate.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
				@Override
				public void onAnimationUpdate(ValueAnimator animation) {
					hsv[0] = from[0] + (to[0] - from[0])
							* (float) animation.getAnimatedValue() / 100f;
					hsv[1] = from[1] + (to[1] - from[1])
							* (float) animation.getAnimatedValue() / 100f;
					hsv[2] = from[2] + (to[2] - from[2])
							* (float) animation.getAnimatedValue() / 100f;
					float p = (float) animation.getAnimatedValue();
					tvProgress.setText(String.format(
							"Preparing Video %05.2f%%", p));
					float progress = ((float) animation.getAnimatedValue() / 100f);
					circularProgress.setWaveColor(evaluate(progress,
							startColor, endColor));
					circularProgress.setProgress(p);
				}
			});

			animate.addListener(new AnimatorListener() {

				@Override
				public void onAnimationStart(Animator arg0) {
					isComplate = false;
				}

				@Override
				public void onAnimationRepeat(Animator arg0) {
				}

				@Override
				public void onAnimationEnd(Animator arg0) {
					isComplate = true;
				}

				@Override
				public void onAnimationCancel(Animator arg0) {
					isComplate = true;
				}
			});
			animate.start();
			lastProg = progress;
		}

	}

	@Override
	public void onImageProgressFrameUpdate(final float progress) {
		if (circularProgress != null) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					changeBgColor(25f * progress / 100f);
				}
			});
		}
	}

	@Override
	public void onVideoProgressFrameUpdate(final float progress) {
		if (circularProgress != null) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					float p = 25f + (75f * progress / 100f);
					changeBgColor(p);
				}
			});
		}
	}
}
