package com.litvideo.photovideomakerwithmusic.activity;

import com.litvideo.photovideomakerwithmusic.R;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

public class SettingActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
		        Window w = getWindow();
		        w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
		        w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
		    }
		setContentView(R.layout.activity_setting);
	}
}
