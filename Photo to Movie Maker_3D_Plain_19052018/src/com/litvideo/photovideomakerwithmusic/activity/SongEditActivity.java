package com.litvideo.photovideomakerwithmusic.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.provider.MediaStore.Audio.AudioColumns;
import android.provider.MediaStore.MediaColumns;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsoluteLayout;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.audiocutter.soundfile.CheapSoundFile;
import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.data.MusicData;
import com.litvideo.photovideomakerwithmusic.util.SongMetadataReader;
import com.litvideo.photovideomakerwithmusic.view.MarkerView;
import com.litvideo.photovideomakerwithmusic.view.WaveformView;
import com.litvideo.photovideomakerwithmusic.MyApplication;
import com.videolib.libffmpeg.FileUtils;

@SuppressWarnings("deprecation")
public class SongEditActivity extends AppCompatActivity implements
		MarkerView.MarkerListener, WaveformView.WaveformListener {

	private ArrayList<MusicData> mMusicDatas;
	private RecyclerView mMusicList;
	private MusicAdapter mAdapter;
	boolean isPlaying = false;
	private Toolbar toolbar;
	private MusicData selectedMusicData;
	private long mLoadingLastUpdateTime;
	private boolean mLoadingKeepGoing;
	private boolean isFromItemClick = false;
	private ProgressDialog mProgressDialog;
	private CheapSoundFile mSoundFile;
	private File mFile;
	private String mFilename = "record";
	private String mArtist;
	private String mTitle;
	private String mExtension;
	private String mRecordingFilename;
	private Uri mRecordingUri;
	private WaveformView mWaveformView;
	private MarkerView mStartMarker;
	private MarkerView mEndMarker;
	private TextView mStartText;
	private TextView mEndText;
	private ImageButton mPlayButton;
	private ImageButton mRewindButton;
	private ImageButton mFfwdButton;
	private ImageButton mZoomInButton;
	private ImageButton mZoomOutButton;
	private boolean mKeyDown;
	private int mWidth;
	private int mMaxPos;
	private int mStartPos;
	private int mEndPos;
	private boolean mStartVisible;
	private boolean mEndVisible;
	private int mLastDisplayedStartPos;
	private int mLastDisplayedEndPos;
	private int mOffset;
	private int mOffsetGoal;
	private int mFlingVelocity;
	private int mPlayStartMsec;
	private int mPlayStartOffset;
	private int mPlayEndMsec;
	private Handler mHandler;
	private boolean mIsPlaying;
	private MediaPlayer mPlayer;
	private boolean mCanSeekAccurately;
	private boolean mTouchDragging;
	private float mTouchStart;
	private int mTouchInitialOffset;
	private int mTouchInitialStartPos;
	private int mTouchInitialEndPos;
	private long mWaveformTouchStartMsec;
	private float mDensity;
	private int mMarkerLeftInset;
	private int mMarkerRightInset;
	private int mMarkerTopOffset;
	private int mMarkerBottomOffset;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		
		 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
		        Window w = getWindow();
		        w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
		        w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
		    }

		mRecordingFilename = null;
		mRecordingUri = null;
		mPlayer = null;
		mIsPlaying = false;

		mSoundFile = null;
		mKeyDown = false;

		mHandler = new Handler();

		loadGui();

		init();
		mHandler.postDelayed(mTimerRunnable, 100);

	}

	private void bindView() {
		mMusicList = (RecyclerView) findViewById(R.id.rvMusicList);
		toolbar = (Toolbar) findViewById(R.id.toolbar);

	}

	private void init() {
		setSupportActionBar(toolbar);
		new LoadMusics().execute();
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

	}

	private void setUpRecyclerView() {
		mAdapter = new MusicAdapter(mMusicDatas);
		RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(
				getApplicationContext(), LinearLayoutManager.VERTICAL, false);
		mMusicList.setLayoutManager(mLayoutManager);
		mMusicList.setItemAnimator(new DefaultItemAnimator());
		mMusicList.setAdapter(mAdapter);
	}

	private void addListener() {
	}

	private ArrayList<MusicData> getMusicFiles() {
		ArrayList<MusicData> mMusicDatas = new ArrayList<MusicData>();
		MusicData musicData;
		String projection_fields[] = { BaseColumns._ID, MediaColumns.TITLE,
				MediaColumns.DATA, MediaColumns.DISPLAY_NAME,
				AudioColumns.DURATION };

		Cursor mCursor = getContentResolver().query(
				MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, projection_fields,
				AudioColumns.IS_MUSIC + " != 0", null,
				MediaColumns.TITLE + " ASC");
		int trackId = mCursor.getColumnIndex(BaseColumns._ID);
		int trackTitle = mCursor.getColumnIndex(MediaColumns.TITLE);
		int trackDisplayName = mCursor
				.getColumnIndex(MediaColumns.DISPLAY_NAME);
		int trackData = mCursor.getColumnIndex(MediaColumns.DATA);
		int trackDuration = mCursor.getColumnIndex(AudioColumns.DURATION);
		while (mCursor.moveToNext()) {

			String path = mCursor.getString(trackData);
			if (isAudioFile(path)) {
				musicData = new MusicData();
				musicData.track_Id = mCursor.getLong(trackId);
				musicData.track_Title = mCursor.getString(trackTitle);
				musicData.track_data = path;
				musicData.track_duration = mCursor.getLong(trackDuration);
				musicData.track_displayName = mCursor
						.getString(trackDisplayName);
				mMusicDatas.add(musicData);
			}

		}
		return mMusicDatas;
	}

	private boolean isAudioFile(String path) {
		if (!TextUtils.isEmpty(path)) {
			return path.endsWith(".mp3");
		}

		return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (TextUtils.isEmpty(mFilename) || !mFilename.equals("record")) {
			getMenuInflater().inflate(R.menu.menu_selection, menu);
			menu.removeItem(R.id.menu_clear);
		} else {
			menu.clear();
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_done:

			onSave();
			MyApplication.getInstance().setMusicData(selectedMusicData);
			break;

		case android.R.id.home:
			onBackPressed();
			break;

		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {

		if (mPlayer != null && mPlayer.isPlaying()) {
			mPlayer.stop();
		}
		mPlayer = null;

		if (mRecordingFilename != null) {
			try {
				if (!new File(mRecordingFilename).delete()) {
					showFinalAlert(new Exception(), R.string.delete_tmp_error);
				}

				getContentResolver().delete(mRecordingUri, null, null);
			} catch (SecurityException e) {
				showFinalAlert(e, R.string.delete_tmp_error);
			}
		}

		super.onDestroy();
	}

	/** Called with an Activity we started with an Intent returns. */

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_SPACE) {
			onPlay(mStartPos);
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void waveformDraw() {
		mWidth = mWaveformView.getMeasuredWidth();
		if (mOffsetGoal != mOffset && !mKeyDown)
			updateDisplay();
		else if (mIsPlaying) {
			updateDisplay();
		} else if (mFlingVelocity != 0) {
			updateDisplay();
		}
	}

	@Override
	public void waveformTouchStart(float x) {
		mTouchDragging = true;
		mTouchStart = x;
		mTouchInitialOffset = mOffset;
		mFlingVelocity = 0;
		mWaveformTouchStartMsec = System.currentTimeMillis();
	}

	@Override
	public void waveformTouchMove(float x) {
		mOffset = trap((int) (mTouchInitialOffset + (mTouchStart - x)));
		updateDisplay();
	}

	@Override
	public void waveformTouchEnd() {
		mTouchDragging = false;
		mOffsetGoal = mOffset;

		long elapsedMsec = System.currentTimeMillis() - mWaveformTouchStartMsec;
		if (elapsedMsec < 300) {
			if (mIsPlaying) {
				int seekMsec = mWaveformView
						.pixelsToMillisecs((int) (mTouchStart + mOffset));
				if (seekMsec >= mPlayStartMsec && seekMsec < mPlayEndMsec) {
					mPlayer.seekTo(seekMsec - mPlayStartOffset);
				} else {
					handlePause();
				}
			} else {
				onPlay((int) (mTouchStart + mOffset));
			}
		}
	}

	@Override
	public void waveformFling(float vx) {
		mTouchDragging = false;
		mOffsetGoal = mOffset;
		mFlingVelocity = (int) (-vx);
		updateDisplay();
	}

	//
	// MarkerListener
	//

	@Override
	public void markerDraw() {
	}

	@Override
	public void markerTouchStart(MarkerView marker, float x) {
		mTouchDragging = true;
		mTouchStart = x;
		mTouchInitialStartPos = mStartPos;
		mTouchInitialEndPos = mEndPos;
	}

	@Override
	public void markerTouchMove(MarkerView marker, float x) {
		float delta = x - mTouchStart;

		if (marker == mStartMarker) {
			mStartPos = trap((int) (mTouchInitialStartPos + delta));
			mEndPos = trap((int) (mTouchInitialEndPos + delta));
		} else {
			mEndPos = trap((int) (mTouchInitialEndPos + delta));
			if (mEndPos < mStartPos)
				mEndPos = mStartPos;
		}

		updateDisplay();
	}

	@Override
	public void markerTouchEnd(MarkerView marker) {
		mTouchDragging = false;
		if (marker == mStartMarker) {
			setOffsetGoalStart();
		} else {
			setOffsetGoalEnd();
		}
	}

	@Override
	public void markerLeft(MarkerView marker, int velocity) {
		mKeyDown = true;

		if (marker == mStartMarker) {
			int saveStart = mStartPos;
			mStartPos = trap(mStartPos - velocity);
			mEndPos = trap(mEndPos - (saveStart - mStartPos));
			setOffsetGoalStart();
		}

		if (marker == mEndMarker) {
			if (mEndPos == mStartPos) {
				mStartPos = trap(mStartPos - velocity);
				mEndPos = mStartPos;
			} else {
				mEndPos = trap(mEndPos - velocity);
			}

			setOffsetGoalEnd();
		}

		updateDisplay();
	}

	@Override
	public void markerRight(MarkerView marker, int velocity) {
		mKeyDown = true;

		if (marker == mStartMarker) {
			int saveStart = mStartPos;
			mStartPos += velocity;
			if (mStartPos > mMaxPos)
				mStartPos = mMaxPos;
			mEndPos += (mStartPos - saveStart);
			if (mEndPos > mMaxPos)
				mEndPos = mMaxPos;

			setOffsetGoalStart();
		}

		if (marker == mEndMarker) {
			mEndPos += velocity;
			if (mEndPos > mMaxPos)
				mEndPos = mMaxPos;

			setOffsetGoalEnd();
		}

		updateDisplay();
	}

	@Override
	public void markerEnter(MarkerView marker) {
	}

	@Override
	public void markerKeyUp() {
		mKeyDown = false;
		updateDisplay();
	}

	@Override
	public void markerFocus(MarkerView marker) {
		mKeyDown = false;
		if (marker == mStartMarker) {
			setOffsetGoalStartNoUpdate();
		} else {
			setOffsetGoalEndNoUpdate();
		}
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				updateDisplay();
			}
		}, 100);
	}

	public static void onAbout(final Activity activity) {
		new AlertDialog.Builder(activity).setTitle(R.string.about_title)
				.setMessage(R.string.about_text)
				.setPositiveButton(R.string.alert_ok_button, null)
				.setCancelable(false).show();
	}

	private void loadGui() {
		setContentView(R.layout.activity_add_music);

		bindView();
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		mDensity = metrics.density;

		mMarkerLeftInset = (int) (46 * mDensity);
		mMarkerRightInset = (int) (48 * mDensity);
		mMarkerTopOffset = (int) (10 * mDensity);
		mMarkerBottomOffset = (int) (10 * mDensity);

		mStartText = (TextView) findViewById(R.id.starttext);
		mStartText.addTextChangedListener(mTextWatcher);
		mEndText = (TextView) findViewById(R.id.endtext);
		mEndText.addTextChangedListener(mTextWatcher);

		mPlayButton = (ImageButton) findViewById(R.id.play);
		mPlayButton.setOnClickListener(mPlayListener);
		mRewindButton = (ImageButton) findViewById(R.id.rew);
		mRewindButton.setOnClickListener(mRewindListener);
		mFfwdButton = (ImageButton) findViewById(R.id.ffwd);
		mFfwdButton.setOnClickListener(mFfwdListener);

		enableDisableButtons();

		mWaveformView = (WaveformView) findViewById(R.id.waveform);
		mWaveformView.setListener(this);

		mMaxPos = 0;
		mLastDisplayedStartPos = -1;
		mLastDisplayedEndPos = -1;

		if (mSoundFile != null) {
			mWaveformView.setSoundFile(mSoundFile);
			mWaveformView.recomputeHeights(mDensity);
			mMaxPos = mWaveformView.maxPos();
		}

		mStartMarker = (MarkerView) findViewById(R.id.startmarker);
		mStartMarker.setListener(this);
		mStartMarker.setAlpha(255);
		mStartMarker.setFocusable(true);
		mStartMarker.setFocusableInTouchMode(true);
		mStartVisible = true;

		mEndMarker = (MarkerView) findViewById(R.id.endmarker);
		mEndMarker.setListener(this);
		mEndMarker.setAlpha(255);
		mEndMarker.setFocusable(true);
		mEndMarker.setFocusableInTouchMode(true);
		mEndVisible = true;

		updateDisplay();
	}

	private void loadFromFile() {
		mFile = new File(mFilename);
		mExtension = getExtensionFromFilename(mFilename);

		SongMetadataReader metadataReader = new SongMetadataReader(this,
				mFilename);
		mTitle = metadataReader.mTitle;
		mArtist = metadataReader.mArtist;

		String titleLabel = mTitle;
		if (mArtist != null && mArtist.length() > 0) {
			titleLabel += " - " + mArtist;
		}
		setTitle(titleLabel);

		mLoadingLastUpdateTime = System.currentTimeMillis();
		mLoadingKeepGoing = true;
		mProgressDialog = new ProgressDialog(SongEditActivity.this);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		mProgressDialog.setTitle(R.string.progress_dialog_loading);
		mProgressDialog.setCancelable(true);
		mProgressDialog
				.setOnCancelListener(new DialogInterface.OnCancelListener() {
					@Override
					public void onCancel(DialogInterface dialog) {
						mLoadingKeepGoing = false;
					}
				});
		mProgressDialog.show();

		final CheapSoundFile.ProgressListener listener = new CheapSoundFile.ProgressListener() {
			@Override
			public boolean reportProgress(double fractionComplete) {
				long now = System.currentTimeMillis();
				if (now - mLoadingLastUpdateTime > 100) {
					mProgressDialog
							.setProgress((int) (mProgressDialog.getMax() * fractionComplete));
					mLoadingLastUpdateTime = now;
				}
				return mLoadingKeepGoing;
			}
		};

		// Create the MediaPlayer in a background thread
		mCanSeekAccurately = false;
		new Thread() {
			@Override
			public void run() {
				mCanSeekAccurately = SeekTest
						.CanSeekAccurately(getPreferences(Context.MODE_PRIVATE));

				System.out.println("Seek test done, creating media player.");
				try {
					MediaPlayer player = new MediaPlayer();
					player.setDataSource(mFile.getAbsolutePath());
					player.setAudioStreamType(AudioManager.STREAM_MUSIC);
					player.prepare();
					mPlayer = player;

				} catch (final java.io.IOException e) {
					Runnable runnable = new Runnable() {
						@Override
						public void run() {
							handleFatalError("ReadError", getResources()
									.getText(R.string.read_error), e);
						}
					};
					mHandler.post(runnable);
				}
				;
			}
		}.start();

		// Load the sound file in a background thread
		new Thread() {
			@Override
			public void run() {
				try {
					mSoundFile = CheapSoundFile.create(mFile.getAbsolutePath(),
							listener);

					if (mSoundFile == null) {
						mProgressDialog.dismiss();
						String name = mFile.getName().toLowerCase();
						String[] components = name.split("\\.");
						String err;
						if (components.length < 2) {
							err = getResources().getString(
									R.string.no_extension_error);
						} else {
							err = getResources().getString(
									R.string.bad_extension_error)
									+ " " + components[components.length - 1];
						}
						final String finalErr = err;
						Runnable runnable = new Runnable() {
							@Override
							public void run() {
								handleFatalError("UnsupportedExtension",
										finalErr, new Exception());
							}
						};
						mHandler.post(runnable);
						return;
					}
				} catch (final Exception e) {
					mProgressDialog.dismiss();
					e.printStackTrace();
					// mInfo.setText(e.toString());

					Runnable runnable = new Runnable() {
						@Override
						public void run() {
							handleFatalError("ReadError", getResources()
									.getText(R.string.read_error), e);
						}
					};
					mHandler.post(runnable);
					return;
				}
				mProgressDialog.dismiss();
				if (mLoadingKeepGoing) {
					Runnable runnable = new Runnable() {
						@Override
						public void run() {
							finishOpeningSoundFile();
						}
					};
					mHandler.post(runnable);
				} else {
					SongEditActivity.this.finish();
				}
			}
		}.start();
	}

	private void finishOpeningSoundFile() {
		mWaveformView.setSoundFile(mSoundFile);
		mWaveformView.recomputeHeights(mDensity);

		mMaxPos = mWaveformView.maxPos();
		mLastDisplayedStartPos = -1;
		mLastDisplayedEndPos = -1;

		mTouchDragging = false;

		mOffset = 0;
		mOffsetGoal = 0;
		mFlingVelocity = 0;
		resetPositions();
		if (mEndPos > mMaxPos)
			mEndPos = mMaxPos;
		// mInfo.setText(mCaption);
		updateDisplay();
		if (isFromItemClick) {
			onPlay(mStartPos);
		}
	}

	private synchronized void updateDisplay() {
		if (mIsPlaying) {
			int now = mPlayer.getCurrentPosition() + mPlayStartOffset;
			int frames = mWaveformView.millisecsToPixels(now);
			mWaveformView.setPlayback(frames);
			setOffsetGoalNoUpdate(frames - mWidth / 2);
			if (now >= mPlayEndMsec) {
				handlePause();
			}
		}

		if (!mTouchDragging) {
			int offsetDelta;

			if (mFlingVelocity != 0) {

				offsetDelta = mFlingVelocity / 30;
				if (mFlingVelocity > 80) {
					mFlingVelocity -= 80;
				} else if (mFlingVelocity < -80) {
					mFlingVelocity += 80;
				} else {
					mFlingVelocity = 0;
				}

				mOffset += offsetDelta;

				if (mOffset + mWidth / 2 > mMaxPos) {
					mOffset = mMaxPos - mWidth / 2;
					mFlingVelocity = 0;
				}
				if (mOffset < 0) {
					mOffset = 0;
					mFlingVelocity = 0;
				}
				mOffsetGoal = mOffset;
			} else {
				offsetDelta = mOffsetGoal - mOffset;

				if (offsetDelta > 10)
					offsetDelta = offsetDelta / 10;
				else if (offsetDelta > 0)
					offsetDelta = 1;
				else if (offsetDelta < -10)
					offsetDelta = offsetDelta / 10;
				else if (offsetDelta < 0)
					offsetDelta = -1;
				else
					offsetDelta = 0;

				mOffset += offsetDelta;
			}
		}

		mWaveformView.setParameters(mStartPos, mEndPos, mOffset);
		mWaveformView.invalidate();

		mStartMarker.setContentDescription(getResources().getText(
				R.string.start_marker)
				+ " " + formatTime(mStartPos));
		mEndMarker.setContentDescription(getResources().getText(
				R.string.end_marker)
				+ " " + formatTime(mEndPos));

		int startX = mStartPos - mOffset - mMarkerLeftInset;
		if (startX + mStartMarker.getWidth() >= 0) {
			if (!mStartVisible) {
				// Delay this to avoid flicker
				mHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						mStartVisible = true;
						mStartMarker.setAlpha(255);
					}
				}, 0);
			}
		} else {
			if (mStartVisible) {
				mStartMarker.setAlpha(0);
				mStartVisible = false;
			}
			startX = 0;
		}

		int endX = mEndPos - mOffset - mEndMarker.getWidth()
				+ mMarkerRightInset;
		if (endX + mEndMarker.getWidth() >= 0) {
			if (!mEndVisible) {
				// Delay this to avoid flicker
				mHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						mEndVisible = true;
						mEndMarker.setAlpha(255);
					}
				}, 0);
			}
		} else {
			if (mEndVisible) {
				mEndMarker.setAlpha(0);
				mEndVisible = false;
			}
			endX = 0;
		}

		mStartMarker.setLayoutParams(new AbsoluteLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, startX,
				mMarkerTopOffset));

		mEndMarker.setLayoutParams(new AbsoluteLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, endX,
				mWaveformView.getMeasuredHeight() - mEndMarker.getHeight()
						- mMarkerBottomOffset));
	}

	private Runnable mTimerRunnable = new Runnable() {
		@Override
		public void run() {
			// Updating an EditText is slow on Android. Make sure
			// we only do the update if the text has actually changed.
			if (mStartPos != mLastDisplayedStartPos && !mStartText.hasFocus()) {
				mStartText.setText(formatTime(mStartPos));
				mLastDisplayedStartPos = mStartPos;
			}

			if (mEndPos != mLastDisplayedEndPos && !mEndText.hasFocus()) {
				mEndText.setText(formatTime(mEndPos));
				mLastDisplayedEndPos = mEndPos;
			}

			mHandler.postDelayed(mTimerRunnable, 100);
		}
	};

	private void enableDisableButtons() {
		if (mIsPlaying) {
			mPlayButton.setImageResource(android.R.drawable.ic_media_pause);
			mPlayButton.setContentDescription(getResources().getText(
					R.string.stop));
		} else {
			mPlayButton.setImageResource(android.R.drawable.ic_media_play);
			mPlayButton.setContentDescription(getResources().getText(
					R.string.play));
		}
	}

	private void resetPositions() {
		mStartPos = mWaveformView.secondsToPixels(0.0);
		// mEndPos = mWaveformView.secondsToPixels(15.0);TODO
		mEndPos = mWaveformView.secondsToPixels(mMaxPos);
	}

	private int trap(int pos) {
		if (pos < 0)
			return 0;
		if (pos > mMaxPos)
			return mMaxPos;
		return pos;
	}

	private void setOffsetGoalStart() {
		setOffsetGoal(mStartPos - mWidth / 2);
	}

	private void setOffsetGoalStartNoUpdate() {
		setOffsetGoalNoUpdate(mStartPos - mWidth / 2);
	}

	private void setOffsetGoalEnd() {
		setOffsetGoal(mEndPos - mWidth / 2);
	}

	private void setOffsetGoalEndNoUpdate() {
		setOffsetGoalNoUpdate(mEndPos - mWidth / 2);
	}

	private void setOffsetGoal(int offset) {
		setOffsetGoalNoUpdate(offset);
		updateDisplay();
	}

	private void setOffsetGoalNoUpdate(int offset) {
		if (mTouchDragging) {
			return;
		}

		mOffsetGoal = offset;
		if (mOffsetGoal + mWidth / 2 > mMaxPos)
			mOffsetGoal = mMaxPos - mWidth / 2;
		if (mOffsetGoal < 0)
			mOffsetGoal = 0;
	}

	private String formatTime(int pixels) {
		if (mWaveformView != null && mWaveformView.isInitialized()) {
			return formatDecimal(mWaveformView.pixelsToSeconds(pixels));
		} else {
			return "";
		}
	}

	private String formatDecimal(double x) {
		int xWhole = (int) x;
		int xFrac = (int) (100 * (x - xWhole) + 0.5);

		if (xFrac >= 100) {
			xWhole++; // Round up
			xFrac -= 100; // Now we need the remainder after the round up
			if (xFrac < 10) {
				xFrac *= 10; // we need a fraction that is 2 digits long
			}
		}

		if (xFrac < 10)
			return xWhole + ".0" + xFrac;
		else
			return xWhole + "." + xFrac;
	}

	private synchronized void handlePause() {
		if (mPlayer != null && mPlayer.isPlaying()) {
			mPlayer.pause();
		}
		mWaveformView.setPlayback(-1);
		mIsPlaying = false;
		enableDisableButtons();
	}

	private synchronized void onPlay(int startPosition) {
		if (mIsPlaying) {
			handlePause();
			return;
		}

		if (mPlayer == null || startPosition == -1) {
			// Not initialized yet
			return;
		}

		try {
			mPlayStartMsec = mWaveformView.pixelsToMillisecs(startPosition);
			if (startPosition < mStartPos) {
				mPlayEndMsec = mWaveformView.pixelsToMillisecs(mStartPos);
			} else if (startPosition > mEndPos) {
				mPlayEndMsec = mWaveformView.pixelsToMillisecs(mMaxPos);
			} else {
				mPlayEndMsec = mWaveformView.pixelsToMillisecs(mEndPos);
			}

			mPlayStartOffset = 0;

			int startFrame = mWaveformView
					.secondsToFrames(mPlayStartMsec * 0.001);
			int endFrame = mWaveformView.secondsToFrames(mPlayEndMsec * 0.001);
			int startByte = mSoundFile.getSeekableFrameOffset(startFrame);
			int endByte = mSoundFile.getSeekableFrameOffset(endFrame);
			if (mCanSeekAccurately && startByte >= 0 && endByte >= 0) {
				try {
					mPlayer.reset();
					mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
					FileInputStream subsetInputStream = new FileInputStream(
							mFile.getAbsolutePath());
					mPlayer.setDataSource(subsetInputStream.getFD(), startByte,
							endByte - startByte);
					mPlayer.prepare();
					mPlayStartOffset = mPlayStartMsec;
				} catch (Exception e) {
					System.out.println("Exception trying to play file subset");
					mPlayer.reset();
					mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
					mPlayer.setDataSource(mFile.getAbsolutePath());
					mPlayer.prepare();
					mPlayStartOffset = 0;
				}
			}

			mPlayer.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public synchronized void onCompletion(MediaPlayer arg0) {
					handlePause();
				}
			});
			mIsPlaying = true;

			if (mPlayStartOffset == 0) {
				mPlayer.seekTo(mPlayStartMsec);
			}
			mPlayer.start();
			updateDisplay();
			enableDisableButtons();
		} catch (Exception e) {
			showFinalAlert(e, R.string.play_error);
			return;
		}
	}

	/**
	 * Show a "final" alert dialog that will exit the activity after the user
	 * clicks on the OK button. If an exception is passed, it's assumed to be an
	 * error condition, and the dialog is presented as an error, and the stack
	 * trace is logged. If there's no exception, it's a success message.
	 */
	private void showFinalAlert(Exception e, CharSequence message) {
		CharSequence title;
		if (e != null) {
			Log.e("", "Error: " + message);
			Log.e("", getStackTrace(e));
			title = getResources().getText(R.string.alert_title_failure);
			setResult(RESULT_CANCELED, new Intent());
		} else {
			Log.i("Ringdroid", "Success: " + message);
			title = getResources().getText(R.string.alert_title_success);
		}

		new AlertDialog.Builder(SongEditActivity.this,
				R.style.Theme_MovieMaker_AlertDialog)
				.setTitle(title)
				.setMessage(message)
				.setPositiveButton(R.string.alert_ok_button,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int whichButton) {
								finish();
							}
						}).setCancelable(false).show();
	}

	private void showFinalAlert(Exception e, int messageResourceId) {
		showFinalAlert(e, getResources().getText(messageResourceId));
	}

	private String makeRingtoneFilename(CharSequence title, String extension) {
		FileUtils.TEMP_DIRECTORY_AUDIO.mkdirs();
		File tempFile = new File(FileUtils.TEMP_DIRECTORY_AUDIO, title
				+ extension);
		if (tempFile.exists()) {
			FileUtils.deleteFile(tempFile);
		}
		return tempFile.getAbsolutePath();
	}

	private void saveRingtone(final CharSequence title) {
		final String outPath = makeRingtoneFilename(title, mExtension);

		if (outPath == null) {
			showFinalAlert(new Exception(), R.string.no_unique_filename);
			return;
		}
		double startTime = mWaveformView.pixelsToSeconds(mStartPos);
		double endTime = mWaveformView.pixelsToSeconds(mEndPos);
		final int startFrame = mWaveformView.secondsToFrames(startTime);
		final int endFrame = mWaveformView.secondsToFrames(endTime);
		final int duration = (int) (endTime - startTime + 0.5);

		// Create an indeterminate progress dialog
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setTitle(R.string.progress_dialog_saving);
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();

		// Save the sound file in a background thread
		new Thread() {
			@Override
			public void run() {
				final File outFile = new File(outPath);
				try {
					// Write the new file
					mSoundFile.WriteFile(outFile, startFrame, endFrame
							- startFrame);

					// Try to load the new file to make sure it worked
					final CheapSoundFile.ProgressListener listener = new CheapSoundFile.ProgressListener() {
						@Override
						public boolean reportProgress(double frac) {
							// Do nothing - we're not going to try to
							// estimate when reloading a saved sound
							// since it's usually fast, but hard to
							// estimate anyway.
							return true; // Keep going
						}
					};
					CheapSoundFile.create(outPath, listener);
				} catch (Exception e) {
					mProgressDialog.dismiss();

					CharSequence errorMessage;
					if (e.getMessage().equals("No space left on device")) {
						errorMessage = getResources().getText(
								R.string.no_space_error);
						e = null;
					} else {
						errorMessage = getResources().getText(
								R.string.write_error);
					}

					final CharSequence finalErrorMessage = errorMessage;
					final Exception finalException = e;
					Runnable runnable = new Runnable() {
						@Override
						public void run() {
							handleFatalError("WriteError", finalErrorMessage,
									finalException);
						}
					};
					mHandler.post(runnable);
					return;
				}

				mProgressDialog.dismiss();

				Runnable runnable = new Runnable() {
					@Override
					public void run() {
						afterSavingRingtone(title, outPath, outFile, duration);
					}
				};
				mHandler.post(runnable);
			}
		}.start();
	}

	private void afterSavingRingtone(CharSequence title, String outPath,
			File outFile, int duration) {
		long length = outFile.length();
		if (length <= 512) {
			outFile.delete();
			new AlertDialog.Builder(this)
					.setTitle(R.string.alert_title_failure)
					.setMessage(R.string.too_small_error)
					.setPositiveButton(R.string.alert_ok_button, null)
					.setCancelable(false).show();
			return;
		}

		// Create the database record, pointing to the existing file path

		long fileSize = outFile.length();
		String mimeType = "audio/mpeg";

		String artist = "" + getResources().getText(R.string.artist_name);

		ContentValues values = new ContentValues();
		values.put(MediaStore.MediaColumns.DATA, outPath);
		values.put(MediaStore.MediaColumns.TITLE, title.toString());
		values.put(MediaStore.MediaColumns.SIZE, fileSize);
		values.put(MediaStore.MediaColumns.MIME_TYPE, mimeType);
		values.put(AudioColumns.ARTIST, artist);
		values.put(AudioColumns.DURATION, duration);
		values.put(AudioColumns.IS_MUSIC, true);

		Log.e("audio", "duaration is " + duration);
		// Insert it into the database
		Uri uri = MediaStore.Audio.Media.getContentUriForPath(outPath);
		final Uri newUri = getContentResolver().insert(uri, values);
		setResult(RESULT_OK, new Intent().setData(newUri));
		selectedMusicData.track_data = outPath;
		selectedMusicData.track_duration = duration * 1000;
		MyApplication.getInstance().setMusicData(selectedMusicData);

		// There's nothing more to do with music or an alarm. Show a
		// success message and then quit.

		// Toast.makeText(this,title + " is Select",
		// Toast.LENGTH_SHORT)
		// .show();

		finish();

	}

	private void handleFatalError(final CharSequence errorInternalName,
			final CharSequence errorString, final Exception exception) {
		Log.i("Ringdroid", "handleFatalError");
	}

	private void onSave() {
		if (mIsPlaying) {
			handlePause();
		}

		saveRingtone("temp");
		// MyApplication.getInstance().setMusicData(selectedMusicData);
		// setResult(RESULT_OK);
		// finish();

	}

	private void enableZoomButtons() {
		mZoomInButton.setEnabled(mWaveformView.canZoomIn());
		mZoomOutButton.setEnabled(mWaveformView.canZoomOut());
	}

	@SuppressWarnings("unused")
	private OnClickListener mSaveListener = new OnClickListener() {
		@Override
		public void onClick(View sender) {
			onSave();
		}
	};

	private OnClickListener mPlayListener = new OnClickListener() {
		@Override
		public void onClick(View sender) {
			onPlay(mStartPos);
		}
	};

	@SuppressWarnings("unused")
	private OnClickListener mZoomInListener = new OnClickListener() {
		@Override
		public void onClick(View sender) {
			mWaveformView.zoomIn();
			mStartPos = mWaveformView.getStart();
			mEndPos = mWaveformView.getEnd();
			mMaxPos = mWaveformView.maxPos();
			mOffset = mWaveformView.getOffset();
			mOffsetGoal = mOffset;
			enableZoomButtons();
			updateDisplay();
		}
	};

	@SuppressWarnings("unused")
	private OnClickListener mZoomOutListener = new OnClickListener() {
		@Override
		public void onClick(View sender) {
			mWaveformView.zoomOut();
			mStartPos = mWaveformView.getStart();
			mEndPos = mWaveformView.getEnd();
			mMaxPos = mWaveformView.maxPos();
			mOffset = mWaveformView.getOffset();
			mOffsetGoal = mOffset;
			enableZoomButtons();
			updateDisplay();
		}
	};

	private OnClickListener mRewindListener = new OnClickListener() {
		@Override
		public void onClick(View sender) {
			if (mIsPlaying) {
				int newPos = mPlayer.getCurrentPosition() - 5000;
				if (newPos < mPlayStartMsec)
					newPos = mPlayStartMsec;
				mPlayer.seekTo(newPos);
			} else {
				mStartMarker.requestFocus();
				markerFocus(mStartMarker);
			}
		}
	};

	private OnClickListener mFfwdListener = new OnClickListener() {
		@Override
		public void onClick(View sender) {
			if (mIsPlaying) {
				int newPos = 5000 + mPlayer.getCurrentPosition();
				if (newPos > mPlayEndMsec)
					newPos = mPlayEndMsec;
				mPlayer.seekTo(newPos);
			} else {
				mEndMarker.requestFocus();
				markerFocus(mEndMarker);
			}
		}
	};

	@SuppressWarnings("unused")
	private OnClickListener mMarkStartListener = new OnClickListener() {
		@Override
		public void onClick(View sender) {
			if (mIsPlaying) {
				mStartPos = mWaveformView.millisecsToPixels(mPlayer
						.getCurrentPosition() + mPlayStartOffset);
				updateDisplay();
			}
		}
	};

	@SuppressWarnings("unused")
	private OnClickListener mMarkEndListener = new OnClickListener() {
		@Override
		public void onClick(View sender) {
			if (mIsPlaying) {
				mEndPos = mWaveformView.millisecsToPixels(mPlayer
						.getCurrentPosition() + mPlayStartOffset);
				updateDisplay();
				handlePause();
			}
		}
	};

	private TextWatcher mTextWatcher = new TextWatcher() {
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}

		@Override
		public void afterTextChanged(Editable s) {
			if (mStartText.hasFocus()) {
				try {
					mStartPos = mWaveformView.secondsToPixels(Double
							.parseDouble(mStartText.getText().toString()));
					updateDisplay();
				} catch (NumberFormatException e) {
				}
			}
			if (mEndText.hasFocus()) {
				try {
					mEndPos = mWaveformView.secondsToPixels(Double
							.parseDouble(mEndText.getText().toString()));
					updateDisplay();
				} catch (NumberFormatException e) {
				}
			}
		}
	};

	private String getStackTrace(Exception e) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PrintWriter writer = new PrintWriter(stream, true);
		e.printStackTrace(writer);
		return stream.toString();
	}

	/**
	 * Return extension including dot, like ".mp3"
	 */
	private String getExtensionFromFilename(String filename) {
		return filename.substring(filename.lastIndexOf('.'), filename.length());
	}

	@Override
	public void onBackPressed() {
		setResult(RESULT_CANCELED);
		super.onBackPressed();
		if (isPlaying) {
			mPlayer.release();
		}

	}

	public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.Holder> {
		private ArrayList<MusicData> musicDatas;
		int mSelectedChoice = 0;

		public class Holder extends RecyclerView.ViewHolder {

			public CheckBox radioMusicName;

			public Holder(View v) {
				super(v);
				radioMusicName = (CheckBox) v.findViewById(R.id.radioMusicName);
			}
		}

		public MusicAdapter(ArrayList<MusicData> mMusicDatas) {
			this.musicDatas = mMusicDatas;
			booleanArray.put(0, true);
		}

		@Override
		public Holder onCreateViewHolder(ViewGroup parent, int paramInt) {
			View itemView = LayoutInflater.from(parent.getContext()).inflate(
					R.layout.music_list_items, parent, false);

			return new Holder(itemView);
		}

		RadioButton mButton;
		SparseBooleanArray booleanArray = new SparseBooleanArray();

		@Override
		public void onBindViewHolder(final Holder holder, final int pos) {
			holder.radioMusicName
					.setText(musicDatas.get(pos).track_displayName);
			holder.radioMusicName.setChecked(booleanArray.get(pos, false));

			holder.radioMusicName.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					booleanArray.clear();
					booleanArray.put(pos, true);
					onPlay(-1);
					playMusic(pos);
					isFromItemClick = true;
					notifyDataSetChanged();
				}
			});
		}

		@Override
		public int getItemCount() {
			return musicDatas.size();
		}

		public void playMusic(int pos) {
			if (mSelectedChoice != pos) {

				selectedMusicData = mMusicDatas.get(pos);

				mFilename = selectedMusicData.getTrack_data();
				loadFromFile();

			}
			mSelectedChoice = pos;
		}
	}

	public class LoadMusics extends AsyncTask<Void, Void, Void> {
		ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(SongEditActivity.this);
			pDialog.setTitle("Please wait");
			pDialog.setMessage("Loading music...");
			pDialog.show();

		}

		@Override
		protected Void doInBackground(Void... paramVarArgs) {
			mMusicDatas = getMusicFiles();
			if (mMusicDatas.size() > 0) {
				selectedMusicData = mMusicDatas.get(0);
				mFilename = selectedMusicData.getTrack_data();
			} else {
				mFilename = "record";
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			pDialog.dismiss();
			if (!mFilename.equals("record")) {
				setUpRecyclerView();
				loadFromFile();
				supportInvalidateOptionsMenu();
			} else if (mMusicDatas.size() > 0) {
				Toast.makeText(getApplicationContext(),
						"No Music found in device\nPlease add music in sdCard",
						Toast.LENGTH_LONG).show();
			}
		}

	}

}
