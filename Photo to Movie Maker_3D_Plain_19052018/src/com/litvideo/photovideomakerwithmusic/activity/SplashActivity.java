package com.litvideo.photovideomakerwithmusic.activity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.litvideo.photovideomakerwithmusic.R;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.LauncherActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class SplashActivity extends Activity {
	
	Button btn_createslide;
//	ImageView img_start;
	Animation animMoveToTop;
	ImageView gif1, gif2, gif3, gif4;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.splash_screen);
		
		gif1 = (ImageView)findViewById(R.id.imagegif1);
		gif2 = (ImageView)findViewById(R.id.imagegif2);
		gif3 = (ImageView)findViewById(R.id.imagegif3);
		gif4 = (ImageView)findViewById(R.id.imagegif4);

        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(gif1);
        Glide.with(this).load(R.drawable.diwali).into(imageViewTarget);
        
        GlideDrawableImageViewTarget imageViewTarget1 = new GlideDrawableImageViewTarget(gif2);
        Glide.with(this).load(R.drawable.birthday).into(imageViewTarget1);
        
        GlideDrawableImageViewTarget imageViewTarget2 = new GlideDrawableImageViewTarget(gif3);
        Glide.with(this).load(R.drawable.anniversary).into(imageViewTarget2);
        
//        GlideDrawableImageViewTarget imageViewTarget3 = new GlideDrawableImageViewTarget(gif4);
//        Glide.with(this).load(R.drawable.backgif).into(imageViewTarget3);
        
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
            	Intent start = new Intent(getApplicationContext(), com.litvideo.photovideomakerwithmusic.activity.LauncherActivity.class);
				startActivity(start);
				finish();
				overridePendingTransition(0, 0);
            }
        },3500);
		
		btn_createslide = (Button)findViewById(R.id.btn_create);
		btn_createslide.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent start = new Intent(getApplicationContext(), com.litvideo.photovideomakerwithmusic.activity.LauncherActivity.class);
				startActivity(start);
				finish();
				overridePendingTransition(0, 0);
			}
		});
	
	}
}
