package com.litvideo.photovideomakerwithmusic.activity;

import java.io.File;
import java.util.ArrayList;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.provider.MediaStore.Video.VideoColumns;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;

import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.adapters.VideoAlbumAdapter;
import com.litvideo.photovideomakerwithmusic.data.VideoData;
import com.litvideo.photovideomakerwithmusic.util.ActivityAnimUtil;
import com.litvideo.photovideomakerwithmusic.view.EmptyRecyclerView;
import com.litvideo.photovideomakerwithmusic.view.SpacesItemDecoration;
import com.litvideo.photovideomakerwithmusic.MyApplication;
import com.videolib.libffmpeg.FileUtils;

public class VideoAlbumActivity extends AppCompatActivity {
	public static final String EXTRA_FROM_VIDEO = "EXTRA_FROM_VIDEO";
	private EmptyRecyclerView rvVideoAlbum;
	private VideoAlbumAdapter mVideoAlbumAdapter;
	private ArrayList<VideoData> mVideoDatas;
	private Toolbar toolbar;
	private boolean isFromVideo = false;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		try {
			sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
					Uri.fromFile(FileUtils.APP_DIRECTORY)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onCreate(savedInstanceState);
		isFromVideo = getIntent().hasExtra(EXTRA_FROM_VIDEO);
		 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
		        Window w = getWindow();
		        w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
		        w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
		    }
		setContentView(R.layout.activity_movie_album);

		bindView();
		init();
		setUpRecyclerView();
		addListener();
	}

	private void bindView() {
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		rvVideoAlbum = (EmptyRecyclerView) findViewById(R.id.rvVideoAlbum);

	}

	private void init() {
		getVideoList();
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}

	
	private void setUpRecyclerView() {

		RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(
				getApplicationContext(), 1);
		rvVideoAlbum.setLayoutManager(mLayoutManager);
		rvVideoAlbum.setEmptyView(findViewById(R.id.list_empty));
		rvVideoAlbum.setItemAnimator(new DefaultItemAnimator());
		int spacingInPixels = getResources().getDimensionPixelSize(
				R.dimen.spacing);
		rvVideoAlbum
				.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

		mVideoAlbumAdapter = new VideoAlbumAdapter(this, mVideoDatas);
		rvVideoAlbum.setAdapter(mVideoAlbumAdapter);
	}

	private void getVideoList() {
		mVideoDatas = new ArrayList<VideoData>();
		String[] projection = new String[] { MediaColumns.DATA,
				BaseColumns._ID, VideoColumns.BUCKET_DISPLAY_NAME,
				VideoColumns.DURATION, VideoColumns.TITLE,

				VideoColumns.DATE_TAKEN, MediaColumns.DISPLAY_NAME };
		Uri video = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
		final String orderBy = VideoColumns.DATE_TAKEN;
		Cursor cur = getContentResolver().query(
				video,
				projection,
				MediaColumns.DATA + " like '%"
						+ FileUtils.APP_DIRECTORY.getAbsolutePath() + "%'",
				null, orderBy + " DESC" // Ordering
		);
		VideoData videoData;
		if (cur.moveToFirst()) {
			int bucketColumn = cur.getColumnIndex(VideoColumns.DURATION);
			int data = cur.getColumnIndex(MediaColumns.DATA);
			int name = cur.getColumnIndex(VideoColumns.TITLE);
			int dateTaken = cur.getColumnIndex(VideoColumns.DATE_TAKEN);
			do {
				videoData = new VideoData();
				videoData.videoDuration = cur.getLong(bucketColumn);
				videoData.videoFullPath = cur.getString(data);
				videoData.videoName = cur.getString(name);
				videoData.dateTaken = cur.getLong(dateTaken);
				if (new File(videoData.videoFullPath).exists())
					mVideoDatas.add(videoData);
			} while (cur.moveToNext());
		}
	}

	private void addListener() {

		findViewById(R.id.list_empty).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View paramView) {
				MyApplication.isBreak = false;
				MyApplication.getInstance().setMusicData(null);

				ActivityAnimUtil.startActivitySafely(paramView, new Intent(
						VideoAlbumActivity.this, ImageSelectionActivity.class));
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		if (isFromVideo) {
			Intent intent2 = new Intent(this, LauncherActivity.class);
			intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TASK);
			// startActivity(intent2);
			ActivityAnimUtil.startActivitySafely(toolbar, intent2);
			finish();
		}
	}
}
