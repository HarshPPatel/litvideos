package com.litvideo.photovideomakerwithmusic.activity;

import java.io.File;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.util.ActivityAnimUtil;
import com.litvideo.photovideomakerwithmusic.view.MyVideoView;
import com.litvideo.photovideomakerwithmusic.view.MyVideoView.PlayPauseListner;
import com.videolib.libffmpeg.FileUtils;

public class VideoPlayActivity extends AppCompatActivity implements
		PlayPauseListner, OnClickListener, OnSeekBarChangeListener {
	private MyVideoView videoView;
	private String videoPath;
	private File videoFile;
	private TextView tvDuration, tvDurationEnd;
	private Handler mHandler = new Handler();
	private ImageView ivPlayPause;
	private SeekBar sbVideo;
	private TextView tvWhatsApp, tvFacebook, tvInstagram;
	private Toolbar toolbar;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
		        Window w = getWindow();
		        w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
		        w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
		    }
		setContentView(R.layout.activity_video_play);

		bindView();
		init();
		addListner();
	}

	private void bindView() {
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		videoView = (MyVideoView) findViewById(R.id.videoView);
		tvDuration = (TextView) findViewById(R.id.tvDuration1);
		tvDurationEnd = (TextView) findViewById(R.id.tvDuration);
		ivPlayPause = (ImageView) findViewById(R.id.ivPlayPause);
		sbVideo = (SeekBar) findViewById(R.id.sbVideo);
		tvWhatsApp = (TextView) findViewById(R.id.tvWhatsApp);
		tvFacebook = (TextView) findViewById(R.id.tvFacebook);
		tvInstagram = (TextView) findViewById(R.id.tvInstagram);
	}

	private void init() {

		setSupportActionBar(toolbar);
		Intent intent = getIntent();
		videoPath = intent.getStringExtra(Intent.EXTRA_TEXT);
		videoFile = new File(videoPath);
		videoView.setVideoPath(videoPath);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

	}

	private void addListner() {
		videoView.setOnPlayPauseListner(this);
		videoView.setOnPreparedListener(new OnPreparedListener() {

			@Override
			public void onPrepared(MediaPlayer mp) {
				try {
					mp.seekTo(100);
					sbVideo.setMax(mp.getDuration());
					progressToTimer(mp.getDuration(), mp.getDuration());
					tvDuration.setText(FileUtils.getDuration(mp
							.getCurrentPosition()));
					tvDurationEnd.setText(FileUtils.getDuration(mp
							.getDuration()));
				} catch (IllegalStateException e) {
					e.printStackTrace();
				}
			}
		});
		findViewById(R.id.list_item_video_clicker).setOnClickListener(this);
		findViewById(R.id.tvMore).setOnClickListener(this);
		tvInstagram.setOnClickListener(this);
		tvFacebook.setOnClickListener(this);
		tvWhatsApp.setOnClickListener(this);
		videoView.setOnClickListener(this);
		ivPlayPause.setOnClickListener(this);
		sbVideo.setOnSeekBarChangeListener(this);
		videoView.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				isComplate = true;
				mHandler.removeCallbacks(mUpdateTimeTask);
				tvDuration.setText(FileUtils.getDuration(mp.getDuration()));
				tvDurationEnd.setText(FileUtils.getDuration(mp.getDuration()));
			}
		});
	}

	@Override
	protected void onPause() {
		super.onPause();
		videoView.pause();
	}

	@Override
	public void onVideoPause() {
		if (mHandler != null && mUpdateTimeTask != null) {
			mHandler.removeCallbacks(mUpdateTimeTask);
		}
		Animation animation = new AlphaAnimation(0f, 1f);
		animation.setDuration(500);
		animation.setFillAfter(true);
		ivPlayPause.startAnimation(animation);
		animation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
				ivPlayPause.setVisibility(View.VISIBLE);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
			}
		});
	}

	@Override
	public void onVideoPlay() {
		updateProgressBar();
		Animation animation = new AlphaAnimation(1f, 0f);
		animation.setDuration(500);
		animation.setFillAfter(true);
		ivPlayPause.startAnimation(animation);
		animation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
				ivPlayPause.setVisibility(View.VISIBLE);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				ivPlayPause.setVisibility(View.INVISIBLE);
			}
		});
	}

	public void updateProgressBar() {
		try {
			mHandler.removeCallbacks(mUpdateTimeTask);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mHandler.postDelayed(mUpdateTimeTask, 100);
	}

	private boolean isComplate;
	private Long tempCapturetime = 0L;
	private Runnable mUpdateTimeTask = new Runnable() {
		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		public void run() {
			if (!isComplate) {
				currentDuration = videoView.getCurrentPosition();
				tempCapturetime += 100L;
				tvDuration.setText(FileUtils.getDuration(videoView
						.getCurrentPosition()));
				tvDurationEnd.setText(FileUtils.getDuration(videoView
						.getDuration()));
				sbVideo.setProgress(currentDuration);
				mHandler.postDelayed(this, 100);
			}
		}
	};
	private int currentDuration;

	public int getProgressPercentage(long currentDuration, long totalDuration) {
		Double percentage = (double) 0;
		long currentSeconds = (int) (currentDuration / 1000f);
		long totalSeconds = (int) (totalDuration / 1000f);
		percentage = (((double) currentSeconds) / totalSeconds) * 100f;
		return percentage.intValue();
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.list_item_video_clicker:
		case R.id.ivPlayPause:
		case R.id.videoView:
			if (videoView.isPlaying()) {
				videoView.pause();
			} else {
				videoView.start();
				isComplate = false;
			}
			break;

		case R.id.tvMore:
			Intent share = new Intent(Intent.ACTION_SEND);
			share.setType("video/mp4");
			share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(videoFile));
			startActivity(Intent.createChooser(share, "Share Video"));
			break;
		case R.id.tvWhatsApp:
			shareImageWhatsApp("com.whatsapp", "Whatsapp");
			break;
		case R.id.tvFacebook:
			shareImageWhatsApp("com.facebook.katana", "Facebook");
			break;
		case R.id.tvInstagram:
			shareImageWhatsApp("com.instagram.android", "Instagram");
			break;

		default:
			break;
		}

	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {

	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		mHandler.removeCallbacks(mUpdateTimeTask);
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		mHandler.removeCallbacks(mUpdateTimeTask);
		int totalDuration = videoView.getDuration();
		currentDuration = progressToTimer(seekBar.getProgress(), totalDuration);
		videoView.seekTo(seekBar.getProgress());
		if (videoView.isPlaying()) {
			updateProgressBar();
		}
	}

	public int progressToTimer(int progress, int totalDuration) {
		int currentDuration = 0;
		totalDuration = (int) (totalDuration / 1000);
		currentDuration = (int) ((((double) progress) / 100) * totalDuration);
		return currentDuration * 1000;
	}

	@Override
	protected void onDestroy() {
		videoView.stopPlayback();
		mHandler.removeCallbacks(mUpdateTimeTask);
		super.onDestroy();
	}

	public void shareImageWhatsApp(String pkg, String appName) {
		Intent share = new Intent(Intent.ACTION_SEND);
		share.setType("video/mp4");
		share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(videoFile));
		if (isPackageInstalled(pkg, this)) {
			share.setPackage(pkg);
			startActivity(Intent.createChooser(share, "Share Video"));
		} else {
			Toast.makeText(getApplicationContext(),
					"Please Install " + appName, Toast.LENGTH_LONG).show();
		}
	}

	private boolean isPackageInstalled(String packagename, Context context) {
		PackageManager pm = context.getPackageManager();
		try {
			pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
			return true;
		} catch (NameNotFoundException e) {
			return false;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Intent intent = new Intent(this, VideoAlbumActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(VideoAlbumActivity.EXTRA_FROM_VIDEO, true);
		ActivityAnimUtil.startActivitySafely(toolbar, intent);

	}
}
