package com.litvideo.photovideomakerwithmusic.activity;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.MediaController;
import android.widget.VideoView;

import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.data.VideoData;

public class VideoPlayer extends AppCompatActivity {
	private VideoView mVideoPlayer;
	private MediaController mediaController;
	private int play_position = 0;
	private int clip_possition = 0;
	private int current_clip_pos = 0;
	private int max_pos, min_pos;
	private ArrayList<VideoData> mVideoDatas;
	public static final String SER_VIDEO_DATA = "SER_VIDEO_DATA";
	public static final String VIDEO_POS = "VIDEO_POS";

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video_player);

		bindView();
		init(savedInstanceState);
		addListener();
	}

	private void bindView() {
		mVideoPlayer = (VideoView) findViewById(R.id.videoPlayer);
	}

	@SuppressWarnings("unchecked")
	private void init(Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			current_clip_pos = savedInstanceState.getInt("pos");
		}
		mVideoDatas = (ArrayList<VideoData>) getIntent().getExtras()
				.getSerializable(SER_VIDEO_DATA);

		clip_possition = getIntent().getExtras().getInt(VIDEO_POS);
		play_position = clip_possition;
		max_pos = mVideoDatas.size();
		min_pos = 0;
		mediaController = new MediaController(this);
		mediaController.setAnchorView(mVideoPlayer);
		mediaController.setPrevNextListeners(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (play_position != max_pos - 1) {
					play_position++;
				} else {
					play_position = min_pos;
				}
				mVideoPlayer.setVideoPath(mVideoDatas.get(play_position).videoFullPath);
			}
		}, new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (play_position > min_pos) {
					play_position--;
				} else {
					play_position = max_pos - 1;
				}
				mVideoPlayer.setVideoPath(mVideoDatas.get(play_position).videoFullPath);
			}
		});
		mVideoPlayer.setMediaController(mediaController);
		mVideoPlayer
				.setVideoPath(mVideoDatas.get(clip_possition).videoFullPath);
		mVideoPlayer.requestFocus();
		mVideoPlayer.seekTo(current_clip_pos);
		mVideoPlayer.start();

	}

	private void addListener() {
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt("pos", mVideoPlayer.getCurrentPosition());
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
		mVideoPlayer.pause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		mVideoPlayer.seekTo(current_clip_pos);
		mVideoPlayer.start();
	}

}
