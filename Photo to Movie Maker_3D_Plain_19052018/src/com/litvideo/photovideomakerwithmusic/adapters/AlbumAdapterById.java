package com.litvideo.photovideomakerwithmusic.adapters;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.data.ImageData;
import com.litvideo.photovideomakerwithmusic.MyApplication;

public class AlbumAdapterById  extends RecyclerView.Adapter<AlbumAdapterById.Holder>{
	private MyApplication application;
	private ArrayList<String> folderId;
	private LayoutInflater inflater;
	private OnItemClickListner<Object> clickListner;
	RequestManager glide;
	public AlbumAdapterById(Context activity) {
		glide = Glide.with(activity);
		application = MyApplication.getInstance();
		folderId=new ArrayList<>(application.getAllAlbum().keySet());
		
		
		Collections.sort(folderId, new Comparator<String>() {
	        @Override
	        public int compare(String s1, String s2) {
	            return s1.compareToIgnoreCase(s2);
	        }
	    });
		application.setSelectedFolderId(folderId.get(0));
		inflater= LayoutInflater.from(activity);
	}
	public void setOnItemClickListner(OnItemClickListner<Object> clickListner) {
		this.clickListner=clickListner;

	}
	public class Holder  extends RecyclerView.ViewHolder {
		ImageView imageView;
		TextView textView;
		View parent;
		CheckBox cbSelect;
		private View clickableView;
		public Holder(View v) {
			super(v);
			parent =v;
			cbSelect = (CheckBox) v.findViewById(R.id.cbSelect);
			imageView = (ImageView) v.findViewById(R.id.imageView1);
			textView = (TextView) v.findViewById(R.id.textView1);
			clickableView =v.findViewById(R.id.clickableView);
		}
		public void onItemClick(View view,Object item){
			if(clickListner!=null){
				clickListner.onItemClick(view,  item);
			}
		}
	}
	@Override
	public int getItemCount() {
		return folderId.size();
	}
	public String getItem(int pos){
		return folderId.get(pos);
	}
	@Override
	public void onBindViewHolder(final Holder holder, int pos) {
		final String currentFolderId =getItem(pos);
		final ImageData  data = application.getImageByAlbum(currentFolderId).get(0);
		holder.textView.setSelected(true);
		holder.textView.setText(data.folderName);
		glide.load(data.imagePath).into(holder.imageView);
		holder.cbSelect.setChecked(currentFolderId.equals(application.getSelectedFolderId()));
		holder.clickableView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				application.setSelectedFolderId(currentFolderId);
				if(clickListner!=null){
					clickListner.onItemClick(v,data);
				}
				notifyDataSetChanged();
			}
		});
	}
	@Override
	public Holder onCreateViewHolder(ViewGroup parent, int pos) {
		return new Holder(inflater.inflate(R.layout.items,parent, false));
	}
}
