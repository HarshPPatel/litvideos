package com.litvideo.photovideomakerwithmusic.adapters;

import java.io.FileOutputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.activity.PreviewActivity;
import com.litvideo.photovideomakerwithmusic.util.ScalingUtilities;
import com.litvideo.photovideomakerwithmusic.MyApplication;
import com.videolib.libffmpeg.FileUtils;

public class FrameAdapter extends RecyclerView.Adapter<FrameAdapter.Holder> {
	private MyApplication application;
	private LayoutInflater inflater;
	private OnItemClickListner<Object> clickListner;
	private RequestManager glide;

	PreviewActivity activity;

	private int[] drawable = new int[] { -1, R.drawable.frame1,
			R.drawable.frame2, R.drawable.frame3, R.drawable.frame4,
			R.drawable.frame5, R.drawable.frame6, R.drawable.frame7,
			R.drawable.frame8, R.drawable.frame9, R.drawable.frame10,
			R.drawable.frame11, R.drawable.frame12, R.drawable.frame13,
			R.drawable.frame14, R.drawable.frame15, R.drawable.frame16,
			R.drawable.frame17, R.drawable.frame18, R.drawable.frame19,
			R.drawable.frame20, R.drawable.frame21, R.drawable.frame22,
			R.drawable.frame23, R.drawable.frame24, R.drawable.frame25,
			R.drawable.frame26, R.drawable.frame27, R.drawable.frame28,
			R.drawable.frame29, R.drawable.frame30, R.drawable.frame31,
			R.drawable.frame32, R.drawable.frame33, R.drawable.frame34,
			R.drawable.frame35, R.drawable.frame36, R.drawable.frame37,
			R.drawable.frame38, R.drawable.frame39, R.drawable.frame40,
			R.drawable.frame41, R.drawable.frame42, R.drawable.frame43,
			R.drawable.frame44, R.drawable.frame45, R.drawable.frame46,
			R.drawable.frame47, R.drawable.frame48, R.drawable.frame49,
			R.drawable.frame50, R.drawable.frame51, R.drawable.frame52,
			R.drawable.frame53, R.drawable.frame54, R.drawable.frame55
			
	};

	public FrameAdapter(PreviewActivity activity) {
		this.activity = activity;
		application = MyApplication.getInstance();
		inflater = LayoutInflater.from(activity);
		glide = Glide.with(activity);
	}

	public void setOnItemClickListner(OnItemClickListner<Object> clickListner) {
		this.clickListner = clickListner;

	}

	public class Holder extends RecyclerView.ViewHolder {
		private ImageView ivThumb;
		private TextView tvThemeName;
		private View mainView;
		private View clickableView;
		CheckBox cbSelect;

		public Holder(View v) {
			super(v);

			cbSelect = (CheckBox) v.findViewById(R.id.cbSelect);
			ivThumb = (ImageView) v.findViewById(R.id.ivThumb);
			tvThemeName = (TextView) v.findViewById(R.id.tvThemeName);
			clickableView = v.findViewById(R.id.clickableView);
			mainView = v;
		}
	}

	@Override
	public int getItemCount() {
		return drawable.length;
	}

	public int getItem(int pos) {

		return drawable[pos];
	}

	int lastPos = 0;

	@Override
	public void onBindViewHolder(final Holder holder, final int pos) {
		final int themes = getItem(pos);
		holder.ivThumb.setScaleType(ScaleType.FIT_XY);
		Glide.with(application).load(themes).into(holder.ivThumb);
		holder.cbSelect.setChecked(themes == activity.getFrame());

		holder.clickableView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (themes == activity.getFrame()) {
					return;
				}
				activity.setFrame(themes);
				if (themes == -1) {
					return;
				}
				notifyItemChanged(lastPos);
				notifyItemChanged(pos);
				lastPos = pos;
				FileUtils.deleteFile(FileUtils.frameFile);
				try {
					Bitmap bm = BitmapFactory.decodeResource(
							activity.getResources(), themes);
					// if(bm.getWidth()!=MyApplication.VIDEO_WIDTH ||
					// bm.getHeight()!= MyApplication.VIDEO_HEIGHT)
					{
						bm = ScalingUtilities.scaleCenterCrop(bm,
								MyApplication.VIDEO_WIDTH,
								MyApplication.VIDEO_HEIGHT);
					}
					FileOutputStream outStream = new FileOutputStream(
							FileUtils.frameFile);
					bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
					outStream.flush();
					outStream.close();
					bm.recycle();
					System.gc();
				} catch (Exception exception) {
				}
			}
		});
	}

	@Override
	public Holder onCreateViewHolder(ViewGroup parent, int pos) {
		View v = inflater.inflate(R.layout.movie_theme_items, parent, false);
		Holder holder = new Holder(v);

		return holder;
	}
}
