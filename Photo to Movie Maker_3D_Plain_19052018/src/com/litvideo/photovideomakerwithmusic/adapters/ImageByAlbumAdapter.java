package com.litvideo.photovideomakerwithmusic.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.data.ImageData;
import com.litvideo.photovideomakerwithmusic.MyApplication;

public class ImageByAlbumAdapter  extends RecyclerView.Adapter<ImageByAlbumAdapter.Holder>{
	private MyApplication application;
	private LayoutInflater inflater;
	private OnItemClickListner<Object> clickListner;
	private RequestManager glide;
	public ImageByAlbumAdapter(Context activity) {
		application = MyApplication.getInstance();
		inflater= LayoutInflater.from(activity);
		glide=Glide.with(activity);
	}
	public void setOnItemClickListner(OnItemClickListner<Object> clickListner) {
		this.clickListner=clickListner;
	}
	public class Holder  extends RecyclerView.ViewHolder {
		ImageView imageView;
		TextView textView;
		View parent;
		View clickableView;
		public Holder(View v) {
			super(v);
			parent =v;
			imageView = (ImageView) v.findViewById(R.id.imageView1);
			textView = (TextView) v.findViewById(R.id.textView1);
			clickableView =v.findViewById(R.id.clickableView);
		}
		public void onItemClick(View view,Object item){
			if(clickListner!=null){
				clickListner.onItemClick(view,  item);
			}
		}
	}
	
	
	@Override
	public int getItemCount() {
		return application.getImageByAlbum(application.getSelectedFolderId()).size();
	}
	public ImageData getItem(int pos){
		return application.getImageByAlbum(application.getSelectedFolderId()).get(pos);
	}
	@Override
	public void onBindViewHolder(final Holder holder,final  int pos) {
		final ImageData  data = getItem(pos) ;
		holder.textView.setSelected(true);
		holder.textView.setText( data.imageCount==0?"":
			String.format("%02d", 
				data.imageCount)
			 );
		glide.load(data.imagePath).into(holder.imageView);
		
		holder.textView.setBackgroundColor(data.imageCount==0?Color.TRANSPARENT:
			0x60ffc107);
		
		
		holder.clickableView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(holder.imageView.getDrawable()==null){
					Toast.makeText(application, "Image currpted or not support.", Toast.LENGTH_LONG).show();
					return;
				}
				application.addSelectedImage(data);
				notifyItemChanged(pos);
				if(clickListner!=null){
					clickListner.onItemClick(v,data);
				}
			}
		});
	}
	@Override
	public Holder onCreateViewHolder(ViewGroup parent, int pos) {
		return new Holder(inflater.inflate(R.layout.items_by_folder,parent, false));
	}
	 
}