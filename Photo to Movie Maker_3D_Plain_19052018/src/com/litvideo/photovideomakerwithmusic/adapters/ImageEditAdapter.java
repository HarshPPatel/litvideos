package com.litvideo.photovideomakerwithmusic.adapters;

import java.util.ArrayList;
import java.util.Collections;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.data.ImageData;
import com.litvideo.photovideomakerwithmusic.MyApplication;

public class ImageEditAdapter  extends RecyclerView.Adapter<ImageEditAdapter.Holder>{
	private MyApplication application;
	private LayoutInflater inflater;
	private OnItemClickListner<Object> clickListner;
	final int TYPE_IMAGE=0,TYPE_BLANK=1;
	private RequestManager glide;
	public ImageEditAdapter(Context activity) {
		application = MyApplication.getInstance();
		inflater= LayoutInflater.from(activity);
		glide=Glide.with(activity);
	}
	public void setOnItemClickListner(OnItemClickListner<Object> clickListner) {
		this.clickListner=clickListner;
	}
	public class Holder  extends RecyclerView.ViewHolder {
		View parent;
		private ImageView ivRemove;
		private ImageView ivThumb;
		private View clickableView;
		
		public Holder(View v) {
			super(v);
			parent =v;
			
			ivThumb = (ImageView) v.findViewById(R.id.ivThumb);
			ivRemove = (ImageView) v.findViewById(R.id.ivRemove);
			clickableView =v.findViewById(R.id.clickableView);
		}
		public void onItemClick(View view,Object item){
			if(clickListner!=null){
				clickListner.onItemClick(view,  item);
			}
		}
	}
	@Override
	public int getItemCount() {
		return application.getSelectedImages().size();
	}
	public ImageData getItem(int pos){
		ArrayList<ImageData> list = application.getSelectedImages();
		if(list.size()<=pos	){
			return  new ImageData();
		}
		return list.get(pos);
	}
	@Override
	public void onBindViewHolder(final Holder holder,final  int pos) {
		holder.parent.setVisibility(View.VISIBLE);
		final ImageData  data = getItem(pos) ;
		glide.load(data.imagePath).into(holder.ivThumb);
		if(getItemCount()<=2){
			holder.ivRemove.setVisibility(View.GONE);
		}else{
			holder.ivRemove.setVisibility(View.VISIBLE);
			
		}
		holder.ivRemove.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				
				application.min_pos = Math.min(application.min_pos,Math.max(0, pos-1));
				MyApplication.isBreak =true;
				application.removeSelectedImage(pos);
				if(clickListner!=null){
					clickListner.onItemClick(v,data);
				}
				notifyDataSetChanged();
			}
		});
	}
	@Override
	public Holder onCreateViewHolder(ViewGroup parent, int pos) {
		View v=inflater.inflate(R.layout.grid_selected_item,parent, false);
		Holder holder = new Holder(v);
		if(getItemViewType(pos)==TYPE_BLANK){
			v.setVisibility(View.INVISIBLE);
		}else{
			v.setVisibility(View.VISIBLE);
		}
		return  holder;
	}
	public synchronized void swap(int fromPosition, int toPosition) {
		ArrayList<ImageData> list = application.getSelectedImages();
		Collections.swap(list, fromPosition, toPosition);
		notifyItemMoved(fromPosition,toPosition);
	}
 
}