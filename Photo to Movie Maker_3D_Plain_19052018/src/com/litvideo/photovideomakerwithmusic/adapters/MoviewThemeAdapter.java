package com.litvideo.photovideomakerwithmusic.adapters;

import java.util.ArrayList;
import java.util.Arrays;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.activity.PreviewActivity;
import com.litvideo.photovideomakerwithmusic.service.ImageCreatorService;
import com.litvideo.photovideomakerwithmusic.MyApplication;
import com.movienaker.movie.themes.THEMES;
import com.videolib.libffmpeg.FileUtils;

public class MoviewThemeAdapter extends
		RecyclerView.Adapter<MoviewThemeAdapter.Holder> {
	private MyApplication application;
	private PreviewActivity previewActivity;
	private ArrayList<THEMES> list;
	private LayoutInflater inflater;

	public MoviewThemeAdapter(PreviewActivity previewActivity) {
		application = MyApplication.getInstance();
		this.previewActivity=previewActivity;
		list = new ArrayList<THEMES>(Arrays.asList(THEMES.values()));
		inflater = LayoutInflater.from(previewActivity);
	}

	public class Holder extends RecyclerView.ViewHolder {
		private ImageView ivThumb;
		private TextView tvThemeName;
		private View mainView;
		private View clickableView;
		CheckBox cbSelect;
		public Holder(View v) {
			super(v);
			
			cbSelect =   (CheckBox) v.findViewById(R.id.cbSelect);
			ivThumb =   (ImageView) v.findViewById(R.id.ivThumb);
			tvThemeName = (TextView) v.findViewById( R.id.tvThemeName);
			clickableView =v.findViewById(R.id.clickableView);
			mainView = v;
		}
	}
	@Override
	public Holder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt) {
		View view = inflater.inflate(R.layout.movie_theme_items,
				paramViewGroup, false);
		return new Holder(view);
	}
	@Override
	public void onBindViewHolder(final Holder holder, final int pos) {
		THEMES themes =list.get(pos);
		Glide.with(application).load(themes
				.getThemeDrawable()).into(holder.ivThumb);
		holder.tvThemeName.setText( themes.toString());
		
		holder.cbSelect.setChecked(themes==application.selectedTheme);
		
		holder.clickableView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if( list.get(pos)==application.selectedTheme){
					return;
				}
				deleteThemeDir(application.selectedTheme.toString());
				application.videoImages.clear();
				application.selectedTheme = list.get(pos);
				application.setCurrentTheme(application.selectedTheme.toString());
				previewActivity.reset();
				Intent intent = new Intent(application, ImageCreatorService.class);
				intent.putExtra(ImageCreatorService.EXTRA_SELECTED_THEME, application.getCurrentTheme());
				application.startService(intent);
				notifyDataSetChanged();
			}
		});
	}
	
	
	
	private void deleteThemeDir(final String dir) {
	 new Thread(){
		 public void run() {
			 FileUtils.deleteThemeDir(dir);
		 };
	 }.start();
	}
	public ArrayList<THEMES> getList() {
		return list;
	}
	@Override
	public int getItemCount() {
		return list.size();
	}
}
