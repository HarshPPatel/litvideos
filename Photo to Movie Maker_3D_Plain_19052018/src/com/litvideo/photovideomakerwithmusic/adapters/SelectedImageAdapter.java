package com.litvideo.photovideomakerwithmusic.adapters;

import java.util.ArrayList;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.activity.ImageSelectionActivity;
import com.litvideo.photovideomakerwithmusic.data.ImageData;
import com.litvideo.photovideomakerwithmusic.MyApplication;

public class SelectedImageAdapter  extends RecyclerView.Adapter<SelectedImageAdapter.Holder>{
	private MyApplication application;
	private LayoutInflater inflater;
	private OnItemClickListner<Object> clickListner;
	final int TYPE_IMAGE=0,TYPE_BLANK=1;
	public boolean  isExpanded =false;
	private RequestManager glide;
	
	ImageSelectionActivity activity;
	
	public SelectedImageAdapter(ImageSelectionActivity activity) {
		this.activity=activity;
		application = MyApplication.getInstance();
		inflater= LayoutInflater.from(activity);
		glide=Glide.with(activity);
	}
	public void setOnItemClickListner(OnItemClickListner<Object> clickListner) {
		this.clickListner=clickListner;

	}
	public class Holder  extends RecyclerView.ViewHolder {
		View parent;
		private ImageView ivRemove;
		private ImageView ivThumb;
		private View clickableView;
		public Holder(View v) {
			super(v);
			parent =v;
			ivThumb = (ImageView) v.findViewById(R.id.ivThumb);
			ivRemove = (ImageView) v.findViewById(R.id.ivRemove);
			clickableView =v.findViewById(R.id.clickableView);
		}
		public void onItemClick(View view,Object item){
			if(clickListner!=null){
				clickListner.onItemClick(view,  item);
			}
		}
	}
	@Override
	public int getItemCount() {
		ArrayList<ImageData> list = application.getSelectedImages();
		if(!isExpanded){
			return list.size()+20;
		}
		
		return list.size();
	}
	
	
	
	@Override
	public int getItemViewType(int position) {
		super.getItemViewType(position);
		if(isExpanded){
			return TYPE_IMAGE;
		}
		ArrayList<ImageData> list = application.getSelectedImages();
		if(position>=list.size()){
			return TYPE_BLANK;
		}
		return TYPE_IMAGE;
	}
	
	private boolean hideRemoveBtn() {
		return application.getSelectedImages().size()<=3 && activity.isFromPreview;
	}
	
	
	public ImageData getItem(int pos){
		ArrayList<ImageData> list = application.getSelectedImages();
		if(list.size()<=pos	){
			return  new ImageData();
		}
		return list.get(pos);
	}
	@Override
	public void onBindViewHolder(final Holder holder,final  int pos) {
		
		if(getItemViewType(pos)==TYPE_BLANK){
			holder.parent.setVisibility(View.INVISIBLE);
			return;
		}
		holder.parent.setVisibility(View.VISIBLE);
		final ImageData  data = getItem(pos) ;
		glide.load(data.imagePath).into(holder.ivThumb);
		if(hideRemoveBtn()){
			holder.ivRemove.setVisibility(View.GONE);
		}else{
			holder.ivRemove.setVisibility(View.VISIBLE);
		}
		
		holder.ivRemove.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				
				int index=application.getSelectedImages().indexOf(data);
				if(activity.isFromPreview){
					application.min_pos = Math.min(application.min_pos,Math.max(0, pos-1));
				}
				application.removeSelectedImage(pos);
				if(clickListner!=null){
					clickListner.onItemClick(v,data);
				}
				if(hideRemoveBtn()){
					Toast.makeText(activity, "At least 3 images require \nif you want to remove this images than add more images.", Toast.LENGTH_LONG).show();
				}
				notifyDataSetChanged();
				
//				notifyItemRemoved(index);
			}
		});
	}
	@Override
	public Holder onCreateViewHolder(ViewGroup parent, int pos) {
		View v=inflater.inflate(R.layout.grid_selected_item,parent, false);
		Holder holder = new Holder(v);
		if(getItemViewType(pos)==TYPE_BLANK){
			v.setVisibility(View.INVISIBLE);
		}else{
			v.setVisibility(View.VISIBLE);
		}
		return  holder;
	}
}