package com.litvideo.photovideomakerwithmusic.adapters;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.activity.VideoPlayActivity;
import com.litvideo.photovideomakerwithmusic.data.VideoData;
import com.litvideo.photovideomakerwithmusic.util.ActivityAnimUtil;
import com.videolib.libffmpeg.FileUtils;

public class VideoAlbumAdapter extends
		RecyclerView.Adapter<VideoAlbumAdapter.Holder> {

	private ArrayList<VideoData> mVideoDatas;
	private Context mContext;

	public class Holder extends RecyclerView.ViewHolder {

		private ImageView ivPreview;
		private TextView tvFileName, tvDuration;
		private Toolbar toolbar;
		private TextView tvVideoDate;
		private View clickable;

		public Holder(View v) {
			super(v);
			clickable = v.findViewById(R.id.list_item_video_clicker);
			ivPreview = (ImageView) v.findViewById(R.id.list_item_video_thumb);
			tvDuration = (TextView) v
					.findViewById(R.id.list_item_video_duration);
			tvFileName = (TextView) v.findViewById(R.id.list_item_video_title);
			tvVideoDate = (TextView) v.findViewById(R.id.list_item_video_date);
			toolbar = (Toolbar) v.findViewById(R.id.list_item_video_toolbar);
		}
	}

	public VideoAlbumAdapter(Context context, ArrayList<VideoData> mVideoDatas) {
		this.mVideoDatas = mVideoDatas;
		this.mContext = context;
	}

	@Override
	public int getItemCount() {
		return mVideoDatas.size();
	}

	@Override
	public void onBindViewHolder(Holder holder, final int pos) {

		// holder.tvDuration.setText(String.valueOf(mVideoDatas.get(pos).videoDuration));
		holder.tvDuration
				.setText(FileUtils.getDuration(mVideoDatas.get(pos).videoDuration));
		Glide.with(mContext).load(mVideoDatas.get(pos).videoFullPath)
				.into(holder.ivPreview);
		holder.tvFileName.setText(mVideoDatas.get(pos).videoName);
		holder.tvVideoDate.setText(DateFormat.getDateInstance().format(
				Long.valueOf(mVideoDatas.get(pos).dateTaken)));
		holder.clickable.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(mContext, VideoPlayActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(Intent.EXTRA_TEXT,
						mVideoDatas.get(pos).videoFullPath);
				ActivityAnimUtil.startActivitySafely(v, intent);

			}
		});
		menu(holder.toolbar, R.menu.home_item_exported_video_local_menu,
				new MenuItemClickListener(mVideoDatas.get(pos)));
	}

	private class MenuItemClickListener implements
			Toolbar.OnMenuItemClickListener {
		VideoData videoData;

		public MenuItemClickListener(VideoData videoData) {
			this.videoData = videoData;
		}

		@Override
		public boolean onMenuItemClick(MenuItem menu) {
			final int pos = mVideoDatas.indexOf(videoData);
			switch (menu.getItemId()) {
			case R.id.action_share_native:
				File file = new File(mVideoDatas.get(pos).videoFullPath);

				Intent shareIntent = new Intent(
						android.content.Intent.ACTION_SEND);
				shareIntent.setType("video/*");
				shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
						mVideoDatas.get(pos).videoName);
				shareIntent.putExtra(android.content.Intent.EXTRA_TITLE,
						mVideoDatas.get(pos).videoName);
				shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));

				mContext.startActivity(Intent.createChooser(shareIntent,
						"Share Video"));
				break;
			case R.id.action_delete:

				AlertDialog.Builder builder = new AlertDialog.Builder(mContext,
						R.style.Theme_MovieMaker_AlertDialog);
				builder.setTitle("Delete Video !");
				builder.setMessage("Are you sure to delete "
						+ mVideoDatas.get(pos).videoName + " ?");
				builder.setPositiveButton("Delete",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								FileUtils.deleteFile(new File(mVideoDatas
										.remove(pos).videoFullPath));
								notifyItemRemoved(pos);
							}
						});
				builder.setNegativeButton("Cancel", null);
				builder.show();
				break;

			default:
				break;
			}
			return false;
		}

	}

	@Override
	public Holder onCreateViewHolder(ViewGroup parent, int arg1) {
		View view = LayoutInflater.from(mContext).inflate(
				R.layout.list_item_published_video, parent, false);
		return new Holder(view);
	}

	public static void menu(Toolbar paramToolbar, int paramInt,
			Toolbar.OnMenuItemClickListener paramOnMenuItemClickListener) {
		paramToolbar.getMenu().clear();
		paramToolbar.inflateMenu(paramInt);
		paramToolbar.setOnMenuItemClickListener(paramOnMenuItemClickListener);
	}
}
