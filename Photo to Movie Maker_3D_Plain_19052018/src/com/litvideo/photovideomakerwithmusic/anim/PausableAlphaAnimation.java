package com.litvideo.photovideomakerwithmusic.anim;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class PausableAlphaAnimation extends AlphaAnimation {
	private long mElapsedAtPause = 0;
	private boolean mPaused = false;

	PlayPauseListner listner;

	public static interface PlayPauseListner {
		void onAnimationPause(Animation animation);
		void onAnimationResume(Animation animation);
		
	}

	public void setOnPlayPauseListner(PlayPauseListner listner) {
		this.listner = listner;
	}

	public PausableAlphaAnimation(float fromAlpha, float toAlpha) {
		super(fromAlpha, toAlpha);
	}

	@Override
	public boolean getTransformation(long currentTime,
			Transformation outTransformation) {
		if (mPaused && mElapsedAtPause == 0) {
			mElapsedAtPause = currentTime - getStartTime();
		}
		if (mPaused)
			setStartTime(currentTime - mElapsedAtPause);
		return super.getTransformation(currentTime, outTransformation);
	}

	@Override
	public void start() {
		super.start();
		mPaused = false;
	}

	public void pause() {
		if (listner != null) {
			listner.onAnimationPause(this);
		}
		mElapsedAtPause = 0;
		mPaused = true;
	}

	public void resume() {
		if (listner != null) {
			listner.onAnimationResume(this);
		}
		mPaused = false;
	}

	public boolean isPause() {
		return mPaused;
	}

	@Override
	public void cancel() {
		super.cancel();
	}
}