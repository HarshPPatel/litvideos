package com.litvideo.photovideomakerwithmusic.anim;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.RotateAnimation;
import android.view.animation.Transformation;

public class PausableRotateAnimation extends RotateAnimation {
	private long mElapsedAtPause = 0;
	private boolean mPaused = false;
	
	public PausableRotateAnimation(float fromDegrees, float toDegrees,
			int pivotXType, float pivotXValue, int pivotYType, float pivotYValue) {
		super(fromDegrees, toDegrees, pivotXType, pivotXValue, pivotYType, pivotYValue);
	}

	public PausableRotateAnimation(float fromDegrees, float toDegrees,
			float pivotX, float pivotY) {
		super(fromDegrees, toDegrees, pivotX, pivotY);
	}

	public PausableRotateAnimation(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public PausableRotateAnimation(float fromDegrees, float toDegrees) {
		super(fromDegrees, toDegrees);
	}
	@Override
	public boolean getTransformation(long currentTime,
			Transformation outTransformation) {
		if (mPaused && mElapsedAtPause == 0) {
			mElapsedAtPause = currentTime - getStartTime();
		}
		if (mPaused)
			setStartTime(currentTime - mElapsedAtPause);
		return super.getTransformation(currentTime, outTransformation);
	}

	public void pause() {
		mElapsedAtPause = 0;
		mPaused = true;
	}

	public void resume() {
		mPaused = false;
	}
	 public boolean isPause() {
	    	return mPaused; 
		}
}
