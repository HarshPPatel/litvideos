package com.litvideo.photovideomakerwithmusic.anim;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;

public class PausableScaleAnimation extends ScaleAnimation {
	private long mElapsedAtPause = 0;
	private boolean mPaused = false;

	public PausableScaleAnimation(float fromX, float toX, float fromY,
			float toY, int pivotXType, float pivotXValue, int pivotYType,
			float pivotYValue) {
		super(fromX, toX, fromY, toY, pivotXType, pivotXValue, pivotYType,
				pivotYValue);
	}

	public PausableScaleAnimation(float fromX, float toX, float fromY,
			float toY, float pivotX, float pivotY) {
		super(fromX, toX, fromY, toY, pivotX, pivotY);
	}

	public PausableScaleAnimation(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public PausableScaleAnimation(float fromX, float toX, float fromY, float toY) {
		super(fromX, toX, fromY, toY);
	}

	@Override
	public boolean getTransformation(long currentTime,
			Transformation outTransformation) {
		if (mPaused && mElapsedAtPause == 0) {
			mElapsedAtPause = currentTime - getStartTime();
		}
		if (mPaused)
			setStartTime(currentTime - mElapsedAtPause);
		return super.getTransformation(currentTime, outTransformation);
	}

	public void pause() {
		mElapsedAtPause = 0;
		mPaused = true;
	}
	public void resume() {
		mPaused = false;
	}
	public boolean isPause() {
		return mPaused;
	}
}
