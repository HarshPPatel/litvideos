package com.litvideo.photovideomakerwithmusic.anim;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;

public class PausableTranslateAnimation extends  TranslateAnimation {
	private long mElapsedAtPause = 0;
	private boolean mPaused = false;
	public PausableTranslateAnimation(Context context, AttributeSet attrs) {super(context, attrs);}
	public PausableTranslateAnimation(int fromXType, float fromXValue,int toXType, float toXValue, int fromYType, float fromYValue,int toYType, float toYValue) {super(fromXType, fromXValue, toXType, toXValue, fromYType, fromYValue, toYType,toYValue);}
	public PausableTranslateAnimation(float fromXDelta, float toXDelta,float fromYDelta, float toYDelta) {super(fromXDelta, toXDelta, fromYDelta, toYDelta);}

	@Override
	public boolean getTransformation(long currentTime,Transformation outTransformation) {
		if (mPaused && mElapsedAtPause == 0) {
			mElapsedAtPause = currentTime - getStartTime();
		}
		if (mPaused)
			setStartTime(currentTime - mElapsedAtPause);
		return super.getTransformation(currentTime, outTransformation);
	}
	public void pause() {
		mElapsedAtPause = 0;
		mPaused = true;
	}
	public void resume() {
		mPaused = false;
	}
	 public boolean isPause() {
	    	return mPaused; 
		}
}
