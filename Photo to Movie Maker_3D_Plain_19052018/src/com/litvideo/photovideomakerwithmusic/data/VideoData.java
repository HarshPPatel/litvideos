package com.litvideo.photovideomakerwithmusic.data;

import java.io.Serializable;

public class VideoData  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1797860026700764929L;
	public String videoName, videoFullPath;
	public long videoDuration;
	public long dateTaken;
	

	public String getVideoName() {
		return videoName;
	}

	public void setVideoName(String videoName) {
		this.videoName = videoName;
	}

	public String getVideoFullPath() {
		return videoFullPath;
	}

	public void setVideoFullPath(String videoFullPath) {
		this.videoFullPath = videoFullPath;
	}

	public long getVideoDuration() {
		return videoDuration;
	}

	public void setVideoDuration(long videoDuration) {
		this.videoDuration = videoDuration;
	}

	

}
