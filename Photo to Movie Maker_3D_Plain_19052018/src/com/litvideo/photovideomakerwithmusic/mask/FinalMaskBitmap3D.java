package com.litvideo.photovideomakerwithmusic.mask;

import java.util.Random;

import android.graphics.Bitmap;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;

import com.litvideo.photovideomakerwithmusic.MyApplication;

public class FinalMaskBitmap3D {
	public static int TOTAL_FRAME = 30;
	public static int ORIGANAL_FRAME = 8;
	public static float ANIMATED_FRAME = 22f;
	public static int ANIMATED_FRAME_CAL = (int) (ANIMATED_FRAME - 1);
	static int randRect[][] ;//= new int[20][20];
	static final Paint paint;
	static Random random = new Random();
	private static Camera camera;
	private static Matrix matrix;
	private static int partNumber =8;
	private static Bitmap[][] bitmaps;
	public static EFFECT rollMode;
	public static int direction =0;
	private static int averageWidth;
	private static int averageHeight;
	private static float rotateDegree;
	private static float axisY;
	private static float axisX;
	
	
	static {
		paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		camera = new Camera();
        matrix = new Matrix();
	}

	public static void reintRect() {
		randRect = new int[(int) ANIMATED_FRAME][(int) ANIMATED_FRAME];
		for (int i = 0; i < randRect.length; i++) {
			for (int j = 0; j < randRect[i].length; j++) {
				randRect[i][j] = 0;
			}
		}
	}

	public static Paint getPaint() {
		paint.setColor(Color.BLACK);
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		return paint;
	}

	static float getRad(int w, int h) {
		return (float) Math.sqrt(((w * w) + (h * h)) / 4);
	}

	static final int HORIZONTAL=0,VERTICALE=1;
	public static enum EFFECT {
		
		
		
		Roll2D_TB("Roll2D_TB"){
			@Override
			public Bitmap getMask(Bitmap bottom, Bitmap top, int factor) {
				setRotateDegree(factor);
				Bitmap mask = Bitmap
						.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
				Canvas canvas = new Canvas(mask);
				drawRollWhole3D(bottom,top,canvas, true);
				return mask;
			}

			@Override
			public void initBitmaps(Bitmap bottom, Bitmap top) {
				partNumber =8;
				direction=VERTICALE;
				rollMode=this;
				camera = new Camera();
		        matrix = new Matrix();
			}
        	
        },
        
        Roll2D_BT("Roll2D_BT"){
			@Override
			public Bitmap getMask(Bitmap bottom, Bitmap top, int factor) {
				factor=ANIMATED_FRAME_CAL-factor;
				setRotateDegree(factor);
				Bitmap mask = Bitmap
						.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
				Canvas canvas = new Canvas(mask);
				drawRollWhole3D(top,bottom,canvas, true);
				return mask;
			}

			@Override
			public void initBitmaps(Bitmap bottom, Bitmap top) {
				partNumber =8;
				direction=VERTICALE;
				rollMode=this;
				camera = new Camera();
		        matrix = new Matrix();
			}
        },
        Roll2D_LR("Roll2D_LR"){
			@Override
			public Bitmap getMask(Bitmap bottom, Bitmap top, int factor) {
				setRotateDegree(factor);
				Bitmap mask = Bitmap
						.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
				Canvas canvas = new Canvas(mask);
				drawRollWhole3D(bottom,top,canvas, true);
				return mask;
			}

			@Override
			public void initBitmaps(Bitmap bottom, Bitmap top) {
				partNumber =8;
				direction=HORIZONTAL;
				rollMode=this;
				camera = new Camera();
		        matrix = new Matrix();
			}
        },
        Roll2D_RL("Roll2D_RL"){
			@Override
			public Bitmap getMask(Bitmap bottom, Bitmap top, int factor) {
				factor=ANIMATED_FRAME_CAL-factor;
				
				setRotateDegree(factor);
				Bitmap mask = Bitmap
						.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
				Canvas canvas = new Canvas(mask);
				drawRollWhole3D(top,bottom,canvas, true);
				return mask;
			}

			@Override
			public void initBitmaps(Bitmap bottom, Bitmap top) {
				partNumber =8;
				direction=HORIZONTAL;
				rollMode=this;
				camera = new Camera();
		        matrix = new Matrix();
			}
        },
        Whole3D_TB("Whole3D_TB"){
        	@Override
        	public Bitmap getMask(Bitmap bottom, Bitmap top, int factor) {
        		setRotateDegree(factor);
        		Bitmap mask = Bitmap
        				.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
        		Canvas canvas = new Canvas(mask);
        		rollMode=this;
        		drawRollWhole3D(bottom,top,canvas, false);
        		return mask;
        	}

			@Override
			public void initBitmaps(Bitmap bottom, Bitmap top) {
				partNumber =8;
				direction=VERTICALE;
				camera = new Camera();
		        matrix = new Matrix();
		        rollMode=this;
			}
        },
        Whole3D_BT("Whole3D_BT"){
        	@Override
        	public Bitmap getMask(Bitmap bottom, Bitmap top, int factor) {
        		factor=ANIMATED_FRAME_CAL-factor;
        		setRotateDegree(factor);
        		Bitmap mask = Bitmap
        				.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
        		Canvas canvas = new Canvas(mask);
        		rollMode=this;
        		drawRollWhole3D(top,bottom,canvas, false);
        		return mask;
        	}

			@Override
			public void initBitmaps(Bitmap bottom, Bitmap top) {
				direction=VERTICALE;
				partNumber =8;
				rollMode=this;
				camera = new Camera();
		        matrix = new Matrix();
				
			}
        },
        Whole3D_LR("Whole3D_LR"){
        	@Override
        	public Bitmap getMask(Bitmap bottom, Bitmap top, int factor) {
        		setRotateDegree(factor);
        		Bitmap mask = Bitmap
        				.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
        		Canvas canvas = new Canvas(mask);
        		rollMode=this;
        		drawRollWhole3D(bottom,top,canvas, false);
        		return mask;
        	}

			@Override
			public void initBitmaps(Bitmap bottom, Bitmap top) {
				partNumber =8;
				direction=HORIZONTAL;
				rollMode=this;
				camera = new Camera();
		        matrix = new Matrix();
			}
        },
        Whole3D_RL("Whole3D_RL"){
			@Override
			public Bitmap getMask(Bitmap bottom, Bitmap top, int factor) {
				factor=ANIMATED_FRAME_CAL-factor;
				setRotateDegree(factor);
				Bitmap mask = Bitmap
						.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
				Canvas canvas = new Canvas(mask);
				rollMode=this;
				 drawRollWhole3D(top,bottom,canvas, false);
				return mask;
			}

			@Override
			public void initBitmaps(Bitmap bottom, Bitmap top) {
				partNumber =8;
				rollMode=this;
				direction=HORIZONTAL;
				camera = new Camera();
		        matrix = new Matrix();
			}
        },
        SepartConbine_TB("SepartConbine_TB"){
			@Override
			public Bitmap getMask(Bitmap bottom, Bitmap top, int factor) {
				rollMode=this;
				setRotateDegree(factor);
				Bitmap mask = Bitmap
						.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
				Canvas canvas = new Canvas(mask);
				drawSepartConbine(canvas);
				return mask;
			}

			@Override
			public void initBitmaps(Bitmap bottom, Bitmap top) {
				partNumber =4;
				rollMode=this;
				direction=VERTICALE;
				camera = new Camera();
		        matrix = new Matrix();
				FinalMaskBitmap3D.initBitmaps(bottom,top, this);
				
			}
        },
        SepartConbine_BT("SepartConbine_BT"){
        	@Override
        	public Bitmap getMask(Bitmap bottom, Bitmap top, int factor) {
        		rollMode=this;
        		factor=ANIMATED_FRAME_CAL-factor;
        		setRotateDegree(factor);
        		Bitmap mask = Bitmap
        				.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
        		Canvas canvas = new Canvas(mask);
        		drawSepartConbine(canvas);
        		return mask;
        	}

			@Override
			public void initBitmaps(Bitmap bottom, Bitmap top) {
				partNumber =4;
				rollMode=this;
				direction=VERTICALE;
				camera = new Camera();
		        matrix = new Matrix();
				FinalMaskBitmap3D.initBitmaps(top,bottom, this);
			}
        },
        SepartConbine_LR("SepartConbine_LR"){
        	@Override
        	public Bitmap getMask(Bitmap bottom, Bitmap top, int factor) {
        		rollMode=this;
        		setRotateDegree(factor);
        		Bitmap mask = Bitmap
        				.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
        		Canvas canvas = new Canvas(mask);
        		drawSepartConbine(canvas);
        		return mask;
        	}

			@Override
			public void initBitmaps(Bitmap bottom, Bitmap top) {
				partNumber =4;
				rollMode=this;
				direction=HORIZONTAL;
				camera = new Camera();
		        matrix = new Matrix();
				FinalMaskBitmap3D.initBitmaps(bottom,top, this);
				
			}
        },
        SepartConbine_RL("SepartConbine_RL"){
        	@Override
        	public Bitmap getMask(Bitmap bottom, Bitmap top, int factor) {
        		rollMode=this;
        		factor=ANIMATED_FRAME_CAL-factor;
        		setRotateDegree(factor);
        		Bitmap mask = Bitmap
        				.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
        		Canvas canvas = new Canvas(mask);
        		drawSepartConbine(canvas);
        		return mask;
        	}

			@Override
			public void initBitmaps(Bitmap bottom, Bitmap top) {
				partNumber =4;
				rollMode=this;
				direction=HORIZONTAL;
				camera = new Camera();
		        matrix = new Matrix();
				FinalMaskBitmap3D.initBitmaps(top,bottom, this);
			}
        },
        RollInTurn_TB("RollInTurn_TB"){
        	@Override
        	public Bitmap getMask(Bitmap bottom, Bitmap top, int factor) {
        		rollMode=this;
        		setRotateDegree(factor);
        		Bitmap mask = Bitmap
        				.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
        		Canvas canvas = new Canvas(mask);
        		drawRollInTurn(canvas);
        		return mask;
        	}

			@Override
			public void initBitmaps(Bitmap bottom, Bitmap top) {
				partNumber =8;
				rollMode=this;
				direction=VERTICALE;
				camera = new Camera();
		        matrix = new Matrix();
				FinalMaskBitmap3D.initBitmaps(bottom,top, this);
			}
        }, 
        RollInTurn_BT("RollInTurn_BT"){
        	@Override
        	public Bitmap getMask(Bitmap bottom, Bitmap top, int factor) {
        		rollMode=this;
        		factor=ANIMATED_FRAME_CAL-factor;
        		setRotateDegree(factor);
        		Bitmap mask = Bitmap
        				.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
        		Canvas canvas = new Canvas(mask);
        		drawRollInTurn(canvas);
        		return mask;
        	}

			@Override
			public void initBitmaps(Bitmap bottom, Bitmap top) {
				rollMode=this;
				partNumber =8;direction=VERTICALE;
				camera = new Camera();
		        matrix = new Matrix();
				FinalMaskBitmap3D.initBitmaps(top,bottom, this);
			}
        }, 
        RollInTurn_LR("RollInTurn_LR"){
        	@Override
        	public Bitmap getMask(Bitmap bottom, Bitmap top, int factor) {
        		rollMode=this;
        		setRotateDegree(factor);
        		Bitmap mask = Bitmap
        				.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
        		Canvas canvas = new Canvas(mask);
        		drawRollInTurn(canvas);
        		return mask;
        	}

			@Override
			public void initBitmaps(Bitmap bottom, Bitmap top) {
				rollMode=this;
				partNumber =8;direction=HORIZONTAL;
				camera = new Camera();
		        matrix = new Matrix();
				FinalMaskBitmap3D.initBitmaps(bottom,top, this);
			}
        }, 
        RollInTurn_RL("RollInTurn_RL"){
  			@Override
  			public Bitmap getMask(Bitmap bottom, Bitmap top, int factor) {
  				rollMode=this;
  				factor=ANIMATED_FRAME_CAL-factor;
  				setRotateDegree(factor);
  				Bitmap mask = Bitmap
  						.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
  				Canvas canvas = new Canvas(mask);
  				 drawRollInTurn(canvas);
  				return mask;
  			}

			@Override
			public void initBitmaps(Bitmap bottom, Bitmap top) {
				rollMode=this;
				partNumber =8;direction=HORIZONTAL;
				camera = new Camera();
		        matrix = new Matrix();
				FinalMaskBitmap3D.initBitmaps(top,bottom, this);
			}
          }, 
 
        Jalousie_BT("Jalousie_BT"){
        	
        	@Override
        	public Bitmap getMask(Bitmap bottom, Bitmap top, int factor) {
        		rollMode=this;
        		factor=ANIMATED_FRAME_CAL-factor;
        		setRotateDegree(factor);
        		Bitmap mask = Bitmap
        				.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
        		Canvas canvas = new Canvas(mask);
        		drawJalousie(canvas);
        		return mask;
        	}

			@Override
			public void initBitmaps(Bitmap bottom, Bitmap top) {
				rollMode=this;
				partNumber =8; 
				direction=VERTICALE;
				camera = new Camera();
		        matrix = new Matrix();
				FinalMaskBitmap3D.initBitmaps(top,bottom, this);
			}
        },
        Jalousie_LR("Jalousie_LR"){
        	
        	@Override
        	public Bitmap getMask(Bitmap bottom, Bitmap top, int factor) {
        		rollMode=this;
        		setRotateDegree(factor);
        		Bitmap mask = Bitmap
        				.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
        		Canvas canvas = new Canvas(mask);
        		drawJalousie(canvas);
        		return mask;
        	}

			@Override
			public void initBitmaps(Bitmap bottom, Bitmap top) {
				rollMode=this;
				partNumber =8;
				direction=HORIZONTAL;
				camera = new Camera();
		        matrix = new Matrix();
				FinalMaskBitmap3D.initBitmaps(bottom,top, this);
			}
        },
 
 ////TODO remove comment for 2D Effects & also from THEMES.class file       
//        CIRCLE_LEFT_TOP("CIRCLE_LEFT_TOP") {
//			@Override
//			public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//				
//				
//				Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//        		Canvas canvas = new Canvas(mask);
//        		canvas.drawBitmap(bottom, 0, 0, null);
//        		final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//        		Canvas canvas1 = new Canvas(bitmap3);
//				canvas1.drawBitmap(top, 0, 0, null);
//				Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//				paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//				float r = (float) Math.sqrt(MyApplication.VIDEO_WIDTH * MyApplication.VIDEO_WIDTH + MyApplication.VIDEO_HEIGHT * MyApplication.VIDEO_HEIGHT);
//				canvas1.drawCircle(0, 0, r / ANIMATED_FRAME_CAL * factor, paint);
//				canvas.drawBitmap(bitmap3, 0, 0, null);
//				
//				
//				
//				return mask;
//			}
//
//			@Override
//			public void initBitmaps(Bitmap bottom, Bitmap top) {
//				
//			}
//
//		},
//		CIRCLE_RIGHT_TOP("CIRCLE_RIGHT_TOP") {
//		 
//			
//			@Override
//			public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//				Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//        		Canvas canvas = new Canvas(mask);
//        		canvas.drawBitmap(bottom, 0, 0, null);
//        		final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//        		Canvas canvas1 = new Canvas(bitmap3);
//				canvas1.drawBitmap(top, 0, 0, null);
//				Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//				paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//				float r = (float) Math.sqrt(MyApplication.VIDEO_WIDTH * MyApplication.VIDEO_WIDTH + MyApplication.VIDEO_HEIGHT * MyApplication.VIDEO_HEIGHT);
//				canvas1.drawCircle(MyApplication.VIDEO_WIDTH, 0, r / ANIMATED_FRAME_CAL * factor, paint);
//				canvas.drawBitmap(bitmap3, 0, 0, null);
//        		return mask;
//			}
//
//			@Override
//			public void initBitmaps(Bitmap bottom, Bitmap top) {
//				
//			}
//		},CIRCLE_LEFT_BOTTOM("CIRCLE_LEFT_BOTTOM") {
//			@Override
//			public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//				Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//        		Canvas canvas = new Canvas(mask);
//        		canvas.drawBitmap(bottom, 0, 0, null);
//        		final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//        		Canvas canvas1 = new Canvas(bitmap3);
//				canvas1.drawBitmap(top, 0, 0, null);
//				Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//				paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//				float r = (float) Math.sqrt(MyApplication.VIDEO_WIDTH * MyApplication.VIDEO_WIDTH + MyApplication.VIDEO_HEIGHT * MyApplication.VIDEO_HEIGHT);
//				canvas1.drawCircle(0,MyApplication.VIDEO_HEIGHT, r / ANIMATED_FRAME_CAL * factor, paint);
//				canvas.drawBitmap(bitmap3, 0, 0, null);
//        		return mask;
//			}
//
//			@Override
//			public void initBitmaps(Bitmap bottom, Bitmap top) {
//				
//			}
//		},CIRCLE_RIGHT_BOTTOM("CIRCLE_RIGHT_BOTTOM") {
//			@Override
//			public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//				Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//        		Canvas canvas = new Canvas(mask);
//        		canvas.drawBitmap(bottom, 0, 0, null);
//        		final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//        		Canvas canvas1 = new Canvas(bitmap3);
//				canvas1.drawBitmap(top, 0, 0, null);
//				Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//				paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//				float r = (float) Math.sqrt(MyApplication.VIDEO_WIDTH * MyApplication.VIDEO_WIDTH + MyApplication.VIDEO_HEIGHT * MyApplication.VIDEO_HEIGHT);
//				canvas1.drawCircle(MyApplication.VIDEO_WIDTH,MyApplication.VIDEO_HEIGHT, r / ANIMATED_FRAME_CAL * factor, paint);
//				canvas.drawBitmap(bitmap3, 0, 0, null);
//        		return mask;
//			}
//
//			@Override
//			public void initBitmaps(Bitmap bottom, Bitmap top) {
//				
//			}
//		},
//		
//		CIRCLE_IN("CIRCLE_IN") {
//			
//			
//			@Override
//			public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//				Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//        		Canvas canvas = new Canvas(mask);
//        		canvas.drawBitmap(top, 0, 0, null);
//        		final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//        		Canvas canvas1 = new Canvas(bitmap3);
//				canvas1.drawBitmap(bottom, 0, 0, null);
//				Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//				paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//				float r = getRad(MyApplication.VIDEO_WIDTH * 2, MyApplication.VIDEO_HEIGHT * 2);
//				float f = r / (ANIMATED_FRAME_CAL) * factor;
//				canvas1.drawCircle(MyApplication.VIDEO_WIDTH / 2f, MyApplication.VIDEO_HEIGHT / 2f, r - f, paint);
//				canvas.drawBitmap(bitmap3, 0, 0, null);
//        		return mask;
//			}
//
//			@Override
//			public void initBitmaps(Bitmap bottom, Bitmap top) {
//				
//			}
//		 
//		},
//		CIRCLE_OUT("CIRCLE_OUT") {
//			
//			
//			@Override
//			public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//				Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//        		Canvas canvas = new Canvas(mask);
//        		canvas.drawBitmap(bottom, 0, 0, null);
//        		
//        		
//        		final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//        		Canvas canvas1 = new Canvas(bitmap3);
//				canvas1.drawBitmap(top, 0, 0, null);
//				Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//				paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//				float r = getRad(MyApplication.VIDEO_WIDTH * 2, MyApplication.VIDEO_HEIGHT * 2);
//				float f =r- r / (ANIMATED_FRAME_CAL) * factor;
//				canvas1.drawCircle(MyApplication.VIDEO_WIDTH / 2f, MyApplication.VIDEO_HEIGHT / 2f, r - f, paint);
//				canvas.drawBitmap(bitmap3, 0, 0, null);
//				
//				
//				
//				
//				
//        		return mask;
//			}
//
//			@Override
//			public void initBitmaps(Bitmap bottom, Bitmap top) {
//				
//			}
//			 
//		},
//		CROSS_IN("CROSS_IN") {
//			
//			
//			@Override
//			public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//				Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//        		Canvas canvas = new Canvas(mask);
//        		canvas.drawBitmap(bottom, 0, 0, null);
//        		
//        		
//        		final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//        		Canvas canvas1 = new Canvas(bitmap3);
//				canvas1.drawBitmap(top, 0, 0, null);
//				Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//				paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//				
//				
//				int w = MyApplication.VIDEO_WIDTH ;
//				int h = MyApplication.VIDEO_HEIGHT ;
//				
//				
//				float fx = w / (ANIMATED_FRAME_CAL * 2f) * factor;
//				float fy = h / (ANIMATED_FRAME_CAL * 2f) * factor;
//				Path path = new Path();
//				path.moveTo(0, 0);
//				path.lineTo(fx, 0);
//				path.lineTo(fx, fy);
//				path.lineTo(0, fy);
//				path.lineTo(0, 0);
//				path.close();
//				path.moveTo(w, 0);
//				path.lineTo(w - fx, 0);
//				path.lineTo(w - fx, fy);
//				path.lineTo(w, fy);
//				path.lineTo(w, 0);
//				path.close();
//				path.moveTo(w, h);
//				path.lineTo(w - fx, h);
//				path.lineTo(w - fx, h - fy);
//				path.lineTo(w, h - fy);
//				path.lineTo(w, h);
//				path.close();
//				path.moveTo(0, h);
//				path.lineTo(fx, h);
//				path.lineTo(fx, h - fy);
//				path.lineTo(0, h - fy);
//				path.lineTo(0, 0);
//				path.close();
//				
//				
//				
//				
//				canvas1.drawPath(path, paint); 
//				
//				
//				canvas.drawBitmap(bitmap3, 0, 0, null);
//				
//				
//				
//				
//				
//        		return mask;
//			}
//
//			@Override
//			public void initBitmaps(Bitmap bottom, Bitmap top) {
//				
//			}
//			
//			 
//		}
//		,
//		CROSS_OUT("CROSS_OUT") {
//			@Override
//			public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//				Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//        		Canvas canvas = new Canvas(mask);
//        		canvas.drawBitmap(bottom, 0, 0, null);
//        		
//        		
//        		final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//        		Canvas canvas1 = new Canvas(bitmap3);
//				canvas1.drawBitmap(top, 0, 0, null);
//				Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//				paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//				
//				
//				int w = MyApplication.VIDEO_WIDTH ;
//				int h = MyApplication.VIDEO_HEIGHT ;
//				
//				
//				float fx = w / (ANIMATED_FRAME_CAL * 2f) * factor;
//				float fy = h / (ANIMATED_FRAME_CAL * 2f) * factor;
//				Path path = new Path();
//				path.moveTo(w / 2f + fx, 0);
//				path.lineTo(w / 2f + fx, h / 2f - fy);
//				path.lineTo(w, h / 2f - fy);
//				path.lineTo(w, h / 2f + fy);
//				path.lineTo(w / 2f + fx, h / 2f + fy);
//				path.lineTo(w / 2f + fx, h);
//				path.lineTo(w / 2f - fx, h);
//				path.lineTo(w / 2f - fx, h / 2f - fy);
//				path.lineTo(0, h / 2f - fy);
//				path.lineTo(0, h / 2f + fy);
//				path.lineTo(w / 2f - fx, h / 2f + fy);
//				path.lineTo(w / 2f - fx, 0);
//				path.close();
//				
//				
//				
//				
//				canvas1.drawPath(path, paint); 
//				canvas.drawBitmap(bitmap3, 0, 0, null);
//        		return mask;
//			}
//
//			@Override
//			public void initBitmaps(Bitmap bottom, Bitmap top) {
//				
//			}
//		}
//		,
//		DIAMOND_IN("DIAMOND_IN") {
//			
//			@Override
//			public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//				Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//        		Canvas canvas = new Canvas(mask);
//        		canvas.drawBitmap(bottom, 0, 0, null);
//        		
//        		
//        		final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//        		Canvas canvas1 = new Canvas(bitmap3);
//				canvas1.drawBitmap(top, 0, 0, null);
//				Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//				paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//				
//				
//				int w = MyApplication.VIDEO_WIDTH ;
//				int h = MyApplication.VIDEO_HEIGHT ;
//				
//				
//				 
//				float fx = (float) w / (float) (ANIMATED_FRAME_CAL) * factor;
//				float fy = (float) h / (float) (ANIMATED_FRAME_CAL) * factor;
//				Path path = new Path();
//				path.moveTo(w / 2f, h / 2f - fy);
//				path.lineTo(w / 2f + fx, h / 2f);
//				path.lineTo(w / 2f, h / 2f + fy);
//				path.lineTo(w / 2f - fx, h / 2f);
//				path.lineTo(w / 2f, h / 2f - fy);
//				path.close();
//				
//				
//				
//				canvas1.drawPath(path, paint); 
//				
//				
//				canvas.drawBitmap(bitmap3, 0, 0, null);
//				
//				
//				
//				
//				
//        		return mask;
//			}
//
//			@Override
//			public void initBitmaps(Bitmap bottom, Bitmap top) {
//				
//			}
//		 
//
//		},
//		DIAMOND_OUT("DIAMOND_OUT") {
//
//			@Override
//			public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//				Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//        		Canvas canvas = new Canvas(mask);
//        		canvas.drawBitmap(top, 0, 0, null);
//        		
//        		
//        		final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//        		Canvas canvas1 = new Canvas(bitmap3);
//				canvas1.drawBitmap(bottom, 0, 0, null);
//				Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//				paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//				
//				
//				int w = MyApplication.VIDEO_WIDTH ;
//				int h = MyApplication.VIDEO_HEIGHT ;
//				
//				
//				 
//				Path path = new Path();
//				float fx = w - w / ((float) ANIMATED_FRAME_CAL)
//						* factor;
//				float fy = h - h / ((float) ANIMATED_FRAME_CAL)
//						* factor;
//				path.moveTo(w / 2f, h / 2f - fy);
//				path.lineTo(w / 2f + fx, h / 2f);
//				path.lineTo(w / 2f, h / 2f + fy);
//				path.lineTo(w / 2f - fx, h / 2f);
//				path.lineTo(w / 2f, h / 2f - fy);
//				path.close();
//				
//				canvas1.drawPath(path, paint); 
//				canvas.drawBitmap(bitmap3, 0, 0, null);
//        		return mask;
//			}
//
//			@Override
//			public void initBitmaps(Bitmap bottom, Bitmap top) {
//				
//			}
//
//		}
//		,
//
//		ECLIPSE_IN("ECLIPSE_IN") {
//			
//			
//			
//			@Override
//			public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//				Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//				final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//        		Canvas canvas = new Canvas(mask);
//        		Canvas canvas1 = new Canvas(bitmap3);
//
//        		canvas.drawBitmap(bottom, 0, 0, null);
//				canvas1.drawBitmap(top, 0, 0, null);
//				
//				Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//				paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//				
//				
//				int w = MyApplication.VIDEO_WIDTH ;
//				int h = MyApplication.VIDEO_HEIGHT ;
//				
//				
//				
//				float hf = h / (ANIMATED_FRAME_CAL * 2f) * factor;
//				float wf = w / (ANIMATED_FRAME_CAL * 2f) * factor;
//				RectF rectFL = new RectF(-wf, 0, wf, h);
//				RectF rectFT = new RectF(0, -hf, w, hf);
//				RectF rectFR = new RectF(w - wf, 0, w + wf, h);
//				RectF rectFB = new RectF(0, h - hf, w, h + hf);
//				canvas1.drawOval(rectFL, paint);
//				canvas1.drawOval(rectFT, paint);
//				canvas1.drawOval(rectFR, paint);
//				canvas1.drawOval(rectFB, paint);
//				
//				
//				 
//				 
//				
//				  
//				canvas.drawBitmap(bitmap3, 0, 0, null);
//        		return mask;
//			}
//
//			@Override
//			public void initBitmaps(Bitmap bottom, Bitmap top) {
//				
//			}
//			
//		 
//		}, 
//		
//		
//		 
//		FOUR_TRIANGLE("FOUR_TRIANGLE"){
//
//			
//			@Override
//			public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//				Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//				final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//        		Canvas canvas = new Canvas(mask);
//        		Canvas canvas1 = new Canvas(bitmap3);
//
//        		canvas.drawBitmap(bottom, 0, 0, null);
//				canvas1.drawBitmap(top, 0, 0, null);
//				
//				Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//				paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//				
//				
//				int w = MyApplication.VIDEO_WIDTH ;
//				int h = MyApplication.VIDEO_HEIGHT ;
//				
//				
//				
//				float ratioX = w / (ANIMATED_FRAME_CAL * 2f) * factor;
//				float ratioY = h / (ANIMATED_FRAME_CAL * 2f) * factor;
//				Path path = new Path();
//				path.moveTo(0, ratioY);
//				path.lineTo(0, 0);
//				path.lineTo(ratioX, 0);
//				path.lineTo(w, h - ratioY);
//				path.lineTo(w, h);
//				path.lineTo(w - ratioX, h);
//				path.lineTo(0, ratioY);
//				path.close();
//				path.moveTo(w - ratioX, 0);
//				path.lineTo(w, 0);
//				path.lineTo(w, ratioY);
//				path.lineTo(ratioX, h);
//				path.lineTo(0, h);
//				path.lineTo(0, h - ratioY);
//				path.lineTo(w - ratioX, 0);
//				path.close();
//				canvas1.drawPath(path, paint);
//				
//				
//				 
//				 
//				
//				  
//				canvas.drawBitmap(bitmap3, 0, 0, null);
//        		return mask;
//			}
//
//			@Override
//			public void initBitmaps(Bitmap bottom, Bitmap top) {
//				
//			}
//			
//		}
//		
//		,
//
//		HORIZONTAL_RECT("HORIZONTAL_RECT") {
//
//			
//		
//			@Override
//			public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//				Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//				final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//        		Canvas canvas = new Canvas(mask);
//        		Canvas canvas1 = new Canvas(bitmap3);
//
//        		canvas.drawBitmap(bottom, 0, 0, null);
//				canvas1.drawBitmap(top, 0, 0, null);
//				
//				Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//				paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//				
//				
//				int w = MyApplication.VIDEO_WIDTH ;
//				int h = MyApplication.VIDEO_HEIGHT ;
//				
//				
//				
//				float wf = w / 10f;
//				// float rectW = (factor==0?0: (wf*factor)/9f);
//
//				float rectW = wf / ANIMATED_FRAME_CAL * factor;
//				for (int i = 0; i < 10; i++) {
//					Rect rect2 = new Rect((int) (wf * i), 0, (int) (rectW + wf
//							* i), h);
//					canvas1.drawRect(rect2, paint);
//				}
//				
//				
//				 
//				
//				
//				 
//				 
//				
//				  
//				canvas.drawBitmap(bitmap3, 0, 0, null);
//        		return mask;
//			}
//
//			@Override
//			public void initBitmaps(Bitmap bottom, Bitmap top) {
//				
//			}
//			
//		 
//
//		},
//		HORIZONTAL_COLUMN_DOWNMASK("HORIZONTAL_COLUMN_DOWNMASK") {
//
//			@Override
//			public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//				Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//				final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//        		Canvas canvas = new Canvas(mask);
//        		Canvas canvas1 = new Canvas(bitmap3);
//
//        		canvas.drawBitmap(bottom, 0, 0, null);
//				canvas1.drawBitmap(top, 0, 0, null);
//				
//				Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//				paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//				
//				
//				int w = MyApplication.VIDEO_WIDTH ;
//				int h = MyApplication.VIDEO_HEIGHT ;
//				
//				
//				
//				float factorX = ANIMATED_FRAME_CAL / 2f;
//				RectF rectF;
//				rectF = new RectF(0, 0, w / (ANIMATED_FRAME_CAL / 2f) * factor,
//						h / 2f);
//				canvas1.drawRoundRect(rectF, 0, 0, paint);
//				if (factor >= factorX + 0.5f) {
//					factor -= factorX;
//					rectF = new RectF(w - w / ((ANIMATED_FRAME_CAL - 1) / 2f)
//							* factor, h / 2f, w, h);
//					canvas1.drawRoundRect(rectF, 0, 0, paint);
//				}
//				
//				
//				 
//				
//				
//				 
//				 
//				
//				  
//				canvas.drawBitmap(bitmap3, 0, 0, null);
//        		return mask;
//			}
//
//			@Override
//			public void initBitmaps(Bitmap bottom, Bitmap top) {
//				
//			}
//			 
//
//		},
//		
//		PIN_WHEEL("PIN_WHEEL"){
//			
//			
//			
//			
//			@Override
//			public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//				Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//				final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//        		Canvas canvas = new Canvas(mask);
//        		Canvas canvas1 = new Canvas(bitmap3);
//
//        		canvas.drawBitmap(bottom, 0, 0, null);
//				canvas1.drawBitmap(top, 0, 0, null);
//				
//				Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//				paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//				
//				
//				int w = MyApplication.VIDEO_WIDTH ;
//				int h = MyApplication.VIDEO_HEIGHT ;
//				
//				
//				float rationX = (float)w / (float)ANIMATED_FRAME_CAL * factor;
//				float rationY = (float)h / (float)ANIMATED_FRAME_CAL * factor;
//
//				Path path = new Path();
//
//				path.moveTo(w / 2f, h / 2f);
//				path.lineTo(0, h);
//				path.lineTo(rationX, h);
//				path.close();
//
//				path.moveTo(w / 2f, h / 2f);
//				path.lineTo(w, h);
//				path.lineTo(w, h - rationY);
//				path.close();
//
//				path.moveTo(w / 2f, h / 2f);
//				path.lineTo(w, 0);
//				path.lineTo(w - rationX, 0);
//				path.close();
//
//				path.moveTo(w / 2f, h / 2f);
//				path.lineTo(0, 0);
//				path.lineTo(0, rationY);
//				path.close();
//
//				canvas1.drawPath(path, paint);
//				canvas.drawBitmap(bitmap3, 0, 0, null);
//        		return mask;
//			}
//
//			@Override
//			public void initBitmaps(Bitmap bottom, Bitmap top) {
//				
//			}
//			
//			
//			
//			
//
//					
//				},
//				 
//				RECT_RANDOM("RECT_RANDOM") {
//
//					
//					@Override
//					public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//						Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//						final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//		        		Canvas canvas = new Canvas(mask);
//		        		Canvas canvas1 = new Canvas(bitmap3);
//
//		        		canvas.drawBitmap(bottom, 0, 0, null);
//						canvas1.drawBitmap(top, 0, 0, null);
//						
//						Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//						paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//						
//						
//						int w = MyApplication.VIDEO_WIDTH ;
//						int h = MyApplication.VIDEO_HEIGHT ;
//						
//						
//						float wf = w / (ANIMATED_FRAME_CAL);
//						float hf = h / (ANIMATED_FRAME_CAL);
//						for (int i = 0; i < randRect.length-1; i++) {
//							int rand = random.nextInt(randRect[i].length);
//							while (randRect[i][rand] == 1) {
//								rand = random.nextInt(randRect[i].length);
//							}
//							randRect[i][rand] = 1;
//							for (int j = 0; j < randRect[i].length; j++) {
//								if (randRect[i][j] == 1) {
//									RectF rect = new RectF(wf * i, hf
//											* j, wf * (i + 1f), hf * (j + 1f));
//									canvas1.drawRoundRect(rect, 0, 0, paint);
//								}
//							}
//						}
//
//						 
//						canvas.drawBitmap(bitmap3, 0, 0, null);
//		        		return mask;
//					}
//
//					@Override
//					public void initBitmaps(Bitmap bottom, Bitmap top) {
//						
//						FinalMaskBitmap3D.reintRect();
//					}
//				},
//				
//				SKEW_LEFT_MEARGE("SKEW_LEFT_MEARGE"){
//
//					@Override
//					public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//						Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//						final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//		        		Canvas canvas = new Canvas(mask);
//		        		Canvas canvas1 = new Canvas(bitmap3);
//
//		        		canvas.drawBitmap(bottom, 0, 0, null);
//						canvas1.drawBitmap(top, 0, 0, null);
//						
//						Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//						paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//						
//						
//						int w = MyApplication.VIDEO_WIDTH ;
//						int h = MyApplication.VIDEO_HEIGHT ;
//						
//						
//						float ratioX = (float)w /(float) ANIMATED_FRAME_CAL * factor;
//						float ratioY = (float)h /(float) ANIMATED_FRAME_CAL * factor;
//						Path path = new Path();
//
//						path.moveTo(0, ratioY);
//						path.lineTo(ratioX, 0);
//						path.lineTo(0, 0);
//						path.close();
//						path.moveTo(w - ratioX, h);
//						path.lineTo(w, h - ratioY);
//						path.lineTo(w, h);
//						path.close();
//
//						canvas1.drawPath(path, paint);
//						canvas.drawBitmap(bitmap3, 0, 0, null);
//		        		return mask;
//					}
//
//					@Override
//					public void initBitmaps(Bitmap bottom, Bitmap top) {
//						
//					}
//					
//				},
//				 
//				SKEW_LEFT_SPLIT("SKEW_LEFT_SPLIT"){
//					@Override
//					public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//						Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//						final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//		        		Canvas canvas = new Canvas(mask);
//		        		Canvas canvas1 = new Canvas(bitmap3);
//
//		        		canvas.drawBitmap(bottom, 0, 0, null);
//						canvas1.drawBitmap(top, 0, 0, null);
//						
//						Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//						paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//						
//						
//						int w = MyApplication.VIDEO_WIDTH ;
//						int h = MyApplication.VIDEO_HEIGHT ;
//						
//						float ratioX = w / (float)ANIMATED_FRAME_CAL * factor;
//						float ratioY = h / (float)ANIMATED_FRAME_CAL * factor;
//						Path path = new Path();
//
//						path.moveTo(0, ratioY);
//						path.lineTo(0, 0);
//						path.lineTo(ratioX, 0);
//						path.lineTo(w, h - ratioY);
//						path.lineTo(w, h);
//						path.lineTo(w - ratioX, h);
//						path.lineTo(0, ratioY);
//
//						path.close();
//
//						canvas1.drawPath(path, paint);
//						canvas.drawBitmap(bitmap3, 0, 0, null);
//		        		return mask;
//					}
//
//					@Override
//					public void initBitmaps(Bitmap bottom, Bitmap top) {
//						
//					}
//					
//				},
//				SKEW_RIGHT_SPLIT("SKEW_RIGHT_SPLIT"){
//					
//					@Override
//					public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//						Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//						final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//		        		Canvas canvas = new Canvas(mask);
//		        		Canvas canvas1 = new Canvas(bitmap3);
//
//		        		canvas.drawBitmap(bottom, 0, 0, null);
//						canvas1.drawBitmap(top, 0, 0, null);
//						
//						Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//						paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//						
//						
//						int w = MyApplication.VIDEO_WIDTH ;
//						int h = MyApplication.VIDEO_HEIGHT ;
//						
//						float ratioX = (float)w / (float)ANIMATED_FRAME_CAL * factor;
//						float ratioY = (float)h / (float)ANIMATED_FRAME_CAL * factor;
//						Path path = new Path();
//
//						path.moveTo(w - ratioX, 0);
//						path.lineTo(w, 0);
//						path.lineTo(w, ratioY);
//						path.lineTo(ratioX, h);
//						path.lineTo(0, h);
//						path.lineTo(0, h - ratioY);
//						path.lineTo(w - ratioX, 0);
//						path.close();
//
//						canvas1.drawPath(path, paint);
//						canvas.drawBitmap(bitmap3, 0, 0, null);
//		        		return mask;
//					}
//
//					@Override
//					public void initBitmaps(Bitmap bottom, Bitmap top) {
//						
//					}
//					
//				},
//				SKEW_RIGHT_MEARGE("SKEW_RIGHT_MEARGE"){
//
//					@Override
//					public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//						Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//						final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//		        		Canvas canvas = new Canvas(mask);
//		        		Canvas canvas1 = new Canvas(bitmap3);
//
//		        		canvas.drawBitmap(bottom, 0, 0, null);
//						canvas1.drawBitmap(top, 0, 0, null);
//						
//						Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//						paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//						
//						
//						int w = MyApplication.VIDEO_WIDTH ;
//						int h = MyApplication.VIDEO_HEIGHT ;
//						float ratioX =(float) w /(float) ANIMATED_FRAME_CAL * factor;
//						float ratioY = (float)h /(float) ANIMATED_FRAME_CAL * factor;
//						Path path = new Path();
//
//						path.moveTo(0, h - ratioY);
//						path.lineTo(ratioX, h);
//						path.lineTo(0, h);
//						path.close();
//						path.moveTo(w - ratioX, 0);
//						path.lineTo(w, ratioY);
//						path.lineTo(w, 0);
//						path.close();
//
//						canvas1.drawPath(path, paint);
//						canvas.drawBitmap(bitmap3, 0, 0, null);
//		        		return mask;
//					}
//
//					@Override
//					public void initBitmaps(Bitmap bottom, Bitmap top) {
//						
//					}
//					
//				},
//				SQUARE_IN("SQUARE_IN"){
//
//					@Override
//					public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//						Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//						final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//		        		Canvas canvas = new Canvas(mask);
//		        		Canvas canvas1 = new Canvas(bitmap3);
//
//		        		canvas.drawBitmap(bottom, 0, 0, null);
//						canvas1.drawBitmap(top, 0, 0, null);
//						
//						Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//						paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//						
//						
//						int w = MyApplication.VIDEO_WIDTH ;
//						int h = MyApplication.VIDEO_HEIGHT ;
//						
//						float ratioX = w / (ANIMATED_FRAME_CAL * 2f) * factor;
//						float ratioY = h /(ANIMATED_FRAME_CAL * 2f)  * factor;
//
//						Path path = new Path();
//
//						path.moveTo(0, 0);
//						path.lineTo(0, h);
//						path.lineTo(ratioX, h);
//						path.lineTo(ratioX, 0);
//
//						path.moveTo(w, h);
//						path.lineTo(w, 0);
//						path.lineTo(w - ratioX, 0);
//						path.lineTo(w - ratioX, h);
//
//						path.moveTo(ratioX, ratioY);
//						path.lineTo(ratioX, 0);
//						path.lineTo(w - ratioX, 0);
//						path.lineTo(w - ratioX, ratioY);
//
//						path.moveTo(ratioX, h - ratioY);
//						path.lineTo(ratioX, h);
//						path.lineTo(w - ratioX, h);
//						path.lineTo(w - ratioX, h - ratioY);
//
//						path.close();
//
//						canvas1.drawPath(path, paint);
//						canvas.drawBitmap(bitmap3, 0, 0, null);
//		        		return mask;
//					}
//
//					@Override
//					public void initBitmaps(Bitmap bottom, Bitmap top) {
//						
//					}
//					
//				},
//				SQUARE_OUT("SQUARE_OUT"){
//
//					@Override
//					public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//						Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//						final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//		        		Canvas canvas = new Canvas(mask);
//		        		Canvas canvas1 = new Canvas(bitmap3);
//
//		        		canvas.drawBitmap(bottom, 0, 0, null);
//						canvas1.drawBitmap(top, 0, 0, null);
//						
//						Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//						paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//						
//						
//						int w = MyApplication.VIDEO_WIDTH ;
//						int h = MyApplication.VIDEO_HEIGHT ;
//						
//						float ratioX = w / (ANIMATED_FRAME_CAL * 2f) * factor;
//						float ratioY = h / (ANIMATED_FRAME_CAL * 2f) * factor;
//						
//						RectF rectF=new RectF(w/2-ratioX,h/2-ratioY,w/2+ratioX,h/2+ratioY);
//						canvas1.drawRect(rectF, paint);
//
//						canvas.drawBitmap(bitmap3, 0, 0, null);
//		        		return mask;
//					}
//
//					@Override
//					public void initBitmaps(Bitmap bottom, Bitmap top) {
//						
//					}
//					
//				},
//				VERTICAL_RECT("VERTICAL_RECT") {
//
//					@Override
//					public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//						Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//						final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//		        		Canvas canvas = new Canvas(mask);
//		        		Canvas canvas1 = new Canvas(bitmap3);
//
//		        		canvas.drawBitmap(bottom, 0, 0, null);
//						canvas1.drawBitmap(top, 0, 0, null);
//						
//						Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//						paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//						
//						
//						int w = MyApplication.VIDEO_WIDTH ;
//						int h = MyApplication.VIDEO_HEIGHT ;
//						
//						float hf = h / 10f;
//						float rectH = (hf * factor) / ANIMATED_FRAME_CAL;
//						for (int i = 0; i < 10; i++) {
//							Rect rect2 = new Rect(0, (int) (hf * i), w,
//									(int) (rectH + hf * i));
//							canvas1.drawRect(rect2, paint);
//						}
//
//						canvas.drawBitmap(bitmap3, 0, 0, null);
//		        		return mask;
//					}
//
//					@Override
//					public void initBitmaps(Bitmap bottom, Bitmap top) {
//						
//					}
//
//				},
//
//				WIND_MILL("WIND_MILL") {
//
//					@Override
//					public Bitmap getMask(Bitmap top, Bitmap bottom, int factor) {
//						Bitmap mask = Bitmap.createBitmap(MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Bitmap.Config.ARGB_8888);
//						final Bitmap bitmap3 = Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
//		        		Canvas canvas = new Canvas(mask);
//		        		Canvas canvas1 = new Canvas(bitmap3);
//
//		        		canvas.drawBitmap(bottom, 0, 0, null);
//						canvas1.drawBitmap(top, 0, 0, null);
//						
//						Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//						paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT ));
//						
//						
//						int w = MyApplication.VIDEO_WIDTH ;
//						int h = MyApplication.VIDEO_HEIGHT ;
//						float r = getRad(w, h);
//						final RectF oval = new RectF();
//						oval.set((w / 2f - r), (h / 2f - r), w / 2f + r, h / 2f + r);
//						float angle = 90f / ANIMATED_FRAME_CAL * factor;
//						canvas1.drawArc(oval, 90, angle, true, paint);
//						canvas1.drawArc(oval, 180, angle, true, paint);
//						canvas1.drawArc(oval, 270, angle, true, paint);
//						canvas1.drawArc(oval, 360, angle, true, paint);
//
//						canvas.drawBitmap(bitmap3, 0, 0, null);
//		        		return mask;
//					}
//
//					@Override
//					public void initBitmaps(Bitmap bottom, Bitmap top) {
//						
//					}
//
//				}
//		
		
		
		
		
        
        
          ;
		String name = "";

		private EFFECT(String name) {
			this.name = name;
		}

		public void drawText(Canvas canvas) {
			Paint paint = new Paint();
			paint.setTextSize(50);
			paint.setColor(Color.RED);
//			canvas.drawText(name, 50, 100, paint);
		};

		public abstract Bitmap getMask(Bitmap bottom, Bitmap top, int factor);
		public abstract void initBitmaps(Bitmap bottom,Bitmap top );
		
		
	}

	static Bitmap getHorizontalColumnDownMask(int w, int h, int factor) {
		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		Bitmap mask = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(mask);
		float factorX = ANIMATED_FRAME_CAL / 2f;
		RectF rectF;
		rectF = new RectF(0, 0, w / (ANIMATED_FRAME_CAL / 2f) * factor, h / 2f);
		canvas.drawRoundRect(rectF, 0, 0, paint);
		if (factor >= factorX + 0.5f) {
			factor -= factorX;
			rectF = new RectF(w - w / ((ANIMATED_FRAME_CAL - 1) / 2f) * factor,
					h / 2f, w, h);
			canvas.drawRoundRect(rectF, 0, 0, paint);
		}
		return mask;
	}

	static Bitmap getRandomRectHoriMask(int w, int h, int factor) {
		Bitmap mask = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(mask);
		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		float wf = w / 10f;
		float rectW = (factor == 0 ? 0 : (wf * factor) / 9f);
		for (int i = 0; i < 10; i++) {
			Rect rect2 = new Rect((int) (wf * i), 0, (int) (rectW + wf * i), h);
			canvas.drawRect(rect2, paint);
		}
		return mask;
	}

	static Bitmap getNewMask(int w, int h, int factor) {

		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL_AND_STROKE);

		Bitmap mask = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(mask);
		 
		float ratioX = w / 18f * factor;
		float ratioY = h / 18f * factor;

		Path path = new Path();
		path.moveTo(w / 2, h / 2);
		path.lineTo(w / 2 + ratioX, h / 2);
		path.lineTo(w, 0);
		path.lineTo(w / 2, h / 2 - ratioY);
		path.lineTo(w / 2, h / 2);

		path.moveTo(w / 2, h / 2);
		path.lineTo(w / 2 - ratioX, h / 2);
		path.lineTo(0, 0);
		path.lineTo(w / 2, h / 2 - ratioY);
		path.lineTo(w / 2, h / 2);

		path.moveTo(w / 2, h / 2);
		path.lineTo(w / 2 - ratioX, h / 2);
		path.lineTo(0, h);
		path.lineTo(w / 2, h / 2 + ratioY);
		path.lineTo(w / 2, h / 2);

		path.moveTo(w / 2, h / 2);
		path.lineTo(w / 2 + ratioX, h / 2);
		path.lineTo(w, h);
		path.lineTo(w / 2, h / 2 + ratioY);
		path.lineTo(w / 2, h / 2);

		path.close();
		canvas.drawCircle(w / 2f, h / 2f, w / 18f * factor, paint);
		canvas.drawPath(path, paint);
		return mask;
	}

	public static Bitmap getRadialBitmap(int w, int h, int factor) {
		Bitmap mask = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		if (factor == 0) {
			return mask;
		}
		Canvas canvas = new Canvas(mask);
		if (factor == 9) {
			canvas.drawColor(Color.BLACK);
			return mask;
		}
		// drawStar(0, 0, w/9*factor, canvas);
		paint.setStyle(Style.STROKE);
		float radius = getRad(w, h);
		paint.setStrokeWidth(radius / 80f * factor);
		for (int i = 0; i < 11; i++) {
			canvas.drawCircle(w / 2f, h / 2f, radius / 10 * i, paint);
		}
		// drawStar(w, h, canvas);
		return mask;
	}
	
	
	
	
	
	
	   public static void setRotateDegree(int  factor) {
		   if (rollMode == EFFECT.RollInTurn_BT||rollMode == EFFECT.RollInTurn_LR
				   ||rollMode == EFFECT.RollInTurn_RL||rollMode == EFFECT.RollInTurn_TB) {
			   rotateDegree =(90f + (partNumber - 1) * 30f) *factor/(float)ANIMATED_FRAME_CAL;
	        } else if (rollMode == EFFECT.Jalousie_BT||rollMode == EFFECT.Jalousie_LR 
	        		  ) {
	        	rotateDegree =180f *factor/(float)ANIMATED_FRAME_CAL;
	        } else {
	        	rotateDegree =90f *factor/(float)ANIMATED_FRAME_CAL;
	        }
	        if (direction == 1) {
	            axisY = rotateDegree / (float) ((rollMode == EFFECT.Jalousie_BT||rollMode == EFFECT.Jalousie_LR 
		        		  ) ? 180 : 90) * MyApplication.VIDEO_HEIGHT;
	        } else {
	            axisX =  rotateDegree / (float) ((rollMode == EFFECT.Jalousie_BT||rollMode == EFFECT.Jalousie_LR 
		        		  ) ? 180 : 90) * MyApplication.VIDEO_WIDTH;
	        }
	    }
	
	  public static void initBitmaps(Bitmap bottom, Bitmap top,EFFECT effect) {
		    rollMode=effect;
	        if ( MyApplication.VIDEO_HEIGHT <= 0 &&  MyApplication.VIDEO_WIDTH <= 0)
	            return;
	        bitmaps = new Bitmap[2][partNumber];
	          averageWidth = (int) ( MyApplication.VIDEO_WIDTH / partNumber);
	          averageHeight = (int) ( MyApplication.VIDEO_HEIGHT / partNumber);
	        Bitmap partBitmap;
	        for (int i = 0; i < 2; i++) {
	            for (int j = 0; j < partNumber; j++) {
	                Rect rect;
	                if(rollMode == EFFECT.Jalousie_BT||rollMode == EFFECT.Jalousie_LR ){
	                    if (direction == 1) {
	                        rect = new Rect(0, j * averageHeight,  MyApplication.VIDEO_WIDTH, (j + 1) * averageHeight);
	                        partBitmap = getPartBitmap( i==0?bottom:top, 0, j * averageHeight, rect);
	                    } else {
	                        rect = new Rect(j * averageWidth, 0, (j + 1) * averageWidth,  MyApplication.VIDEO_HEIGHT);
	                        partBitmap = getPartBitmap( i==0?bottom:top, j * averageWidth, 0, rect);
	                    }
	                }else{
	                    if (direction == 1) {//纵�?�分�?�
	                        rect = new Rect(j * averageWidth, 0, (j + 1) * averageWidth,  MyApplication.VIDEO_HEIGHT);
	                        partBitmap = getPartBitmap(  i==0?bottom:top , j * averageWidth, 0, rect);
	                    } else {//横�?�分�?�
	                        rect = new Rect(0, j * averageHeight,  MyApplication.VIDEO_WIDTH, (j + 1) * averageHeight);
	                        partBitmap = getPartBitmap( i==0?bottom:top, 0, j * averageHeight, rect);
	                    }
	                }

	                bitmaps[i][j] = partBitmap;
	            }
	        }
	    }
	
	 
	    private static Bitmap getPartBitmap(Bitmap bitmap, int x, int y, Rect rect) {
	        Bitmap bitmap1 = Bitmap.createBitmap(bitmap, x, y, rect.width(), rect.height());
	        return bitmap1;
	    }
	
	    
	    /**
	     * 整体翻滚
	     * degree 0->90 往下翻滚或者往�?�翻滚
	     *
	     * @param canvas
	     * @param draw2D  是�?�画2D效果：true  画2D效果； false  画3D效果
	     */
	    private static void drawRollWhole3D(Bitmap bottom ,Bitmap top,Canvas canvas, boolean draw2D) {

	        Bitmap currWholeBitmap =bottom;
	        Bitmap nextWholeBitmap = top;
	        canvas.save();

	        if (direction == 1) {
	            camera.save();
	            if (draw2D)
	                camera.rotateX(0);
	            else
	                camera.rotateX(-rotateDegree);
	            camera.getMatrix(matrix);
	            camera.restore();

	            matrix.preTranslate(- MyApplication.VIDEO_WIDTH / 2, 0);
	            matrix.postTranslate( MyApplication.VIDEO_WIDTH / 2, axisY);
	            canvas.drawBitmap(currWholeBitmap, matrix, paint);

	            camera.save();
	            if (draw2D)
	                camera.rotateX(0);
	            else
	                camera.rotateX((90 - rotateDegree));
	            camera.getMatrix(matrix);
	            camera.restore();

	            matrix.preTranslate(- MyApplication.VIDEO_WIDTH / 2, - MyApplication.VIDEO_HEIGHT);
	            matrix.postTranslate( MyApplication.VIDEO_WIDTH / 2, axisY);
	            canvas.drawBitmap(nextWholeBitmap, matrix, paint);

	        } else {
	            camera.save();
	            if (draw2D)
	                camera.rotateY(0);
	            else
	                camera.rotateY(rotateDegree);
	            camera.getMatrix(matrix);
	            camera.restore();

	            matrix.preTranslate(0, - MyApplication.VIDEO_HEIGHT / 2);
	            matrix.postTranslate(axisX,  MyApplication.VIDEO_HEIGHT / 2);

	            canvas.drawBitmap(currWholeBitmap, matrix, paint);

	            camera.save();
	            if (draw2D)
	                camera.rotateY(0);
	            else
	                camera.rotateY(rotateDegree - 90);
	            camera.getMatrix(matrix);
	            camera.restore();

	            matrix.preTranslate(- MyApplication.VIDEO_WIDTH, - MyApplication.VIDEO_HEIGHT / 2);
	            matrix.postTranslate(axisX,  MyApplication.VIDEO_HEIGHT / 2);
	            canvas.drawBitmap(nextWholeBitmap, matrix, paint);
	        }
	        canvas.restore();
	    }


	    /**
	     * 纵�?�  头部接�?�  尾部分离效果
	     * degree 0->90 往下翻滚 或者 往�?�翻滚   90->0往上翻滚 或者往翻滚
	     *
	     * @param canvas
	     */
	    private static void drawSepartConbine(Canvas canvas) {
	        for (int i = 0; i < partNumber; i++) {
	            Bitmap currBitmap = bitmaps[0][i];
	            Bitmap nextBitmap = bitmaps[1][i];

	            canvas.save();
	            if (direction == 1) {

	                camera.save();
	                camera.rotateX(-rotateDegree);
	                camera.getMatrix(matrix);
	                camera.restore();

	                matrix.preTranslate(-currBitmap.getWidth() / 2, 0);
	                matrix.postTranslate(currBitmap.getWidth() / 2 + i * averageWidth, axisY);
	                canvas.drawBitmap(currBitmap, matrix, paint);

	                camera.save();
	                camera.rotateX((90 - rotateDegree));
	                camera.getMatrix(matrix);
	                camera.restore();

	                matrix.preTranslate(-nextBitmap.getWidth() / 2, -nextBitmap.getHeight());
	                matrix.postTranslate(nextBitmap.getWidth() / 2 + i * averageWidth, axisY);
	                canvas.drawBitmap(nextBitmap, matrix, paint);

	            } else {
	                camera.save();
	                camera.rotateY(rotateDegree);
	                camera.getMatrix(matrix);
	                camera.restore();

	                matrix.preTranslate(0, -currBitmap.getHeight() / 2);
	                matrix.postTranslate(axisX, currBitmap.getHeight() / 2 + i * averageHeight);
	                canvas.drawBitmap(currBitmap, matrix, paint);

	                camera.save();
	                camera.rotateY(rotateDegree - 90);
	                camera.getMatrix(matrix);
	                camera.restore();

	                matrix.preTranslate(-nextBitmap.getWidth(), -nextBitmap.getHeight() / 2);
	                matrix.postTranslate(axisX, nextBitmap.getHeight() / 2 + i * averageHeight);
	                canvas.drawBitmap(nextBitmap, matrix, paint);
	            }
	            canvas.restore();
	        }
	    }


	    /**
	     * �?次翻滚
	     *
	     * @param canvas
	     */
	    private static void drawRollInTurn(Canvas canvas) {
	        for (int i = 0; i < partNumber; i++) {
	            Bitmap currBitmap = bitmaps[0][i];
	            Bitmap nextBitmap = bitmaps[1][i];

	            float tDegree = rotateDegree - i * 30;
	            if (tDegree < 0)
	                tDegree = 0;
	            if (tDegree > 90)
	                tDegree = 90;


	            canvas.save();
	            if (direction == 1) {
	                float tAxisY = tDegree / 90f *  MyApplication.VIDEO_HEIGHT;
	                if (tAxisY >  MyApplication.VIDEO_HEIGHT)
	                    tAxisY =  MyApplication.VIDEO_HEIGHT;
	                if (tAxisY < 0)
	                    tAxisY = 0;

	                camera.save();
	                camera.rotateX(-tDegree);
	                camera.getMatrix(matrix);
	                camera.restore();

	                matrix.preTranslate(-currBitmap.getWidth(), 0);
	                matrix.postTranslate(currBitmap.getWidth() + i * averageWidth, tAxisY);
	                canvas.drawBitmap(currBitmap, matrix, paint);

	                camera.save();
	                camera.rotateX((90 - tDegree));
	                camera.getMatrix(matrix);
	                camera.restore();

	                matrix.preTranslate(-nextBitmap.getWidth(), -nextBitmap.getHeight());
	                matrix.postTranslate(nextBitmap.getWidth() + i * averageWidth, tAxisY);
	                canvas.drawBitmap(nextBitmap, matrix, paint);

	            } else {
	                float tAxisX = tDegree / 90f *  MyApplication.VIDEO_WIDTH;
	                if (tAxisX >  MyApplication.VIDEO_WIDTH)
	                    tAxisX =  MyApplication.VIDEO_WIDTH;
	                if (tAxisX < 0)
	                    tAxisX = 0;
	                camera.save();
	                camera.rotateY(tDegree);
	                camera.getMatrix(matrix);
	                camera.restore();

	                matrix.preTranslate(0, -currBitmap.getHeight() / 2);
	                matrix.postTranslate(tAxisX, currBitmap.getHeight() / 2 + i * averageHeight);
	                canvas.drawBitmap(currBitmap, matrix, paint);

	                //
	                camera.save();
	                camera.rotateY(tDegree - 90);
	                camera.getMatrix(matrix);
	                camera.restore();

	                matrix.preTranslate(-nextBitmap.getWidth(), -nextBitmap.getHeight() / 2);
	                matrix.postTranslate(tAxisX, nextBitmap.getHeight() / 2 + i * averageHeight);
	                canvas.drawBitmap(nextBitmap, matrix, paint);
	            }
	            canvas.restore();
	        }
	    }


	    /**
	     * 百�?�窗翻页
	     *
	     * @param canvas
	     */
	    private static void drawJalousie(Canvas canvas) {
	        for (int i = 0; i < partNumber; i++) {
	            Bitmap currBitmap = bitmaps[0][i];
	            Bitmap nextBitmap = bitmaps[1][i];

	            canvas.save();
	            //注�? 百�?�窗的翻转方�?�和其他模�?是相�??的  横�?�的时候纵翻  纵�?�的时候横翻
	            if (direction == 1) {

	                if (rotateDegree < 90) {
	                    camera.save();
	                    camera.rotateX(rotateDegree);
	                    camera.getMatrix(matrix);
	                    camera.restore();

	                    matrix.preTranslate(-currBitmap.getWidth() / 2, -currBitmap.getHeight() / 2);
	                    matrix.postTranslate(currBitmap.getWidth() / 2, currBitmap.getHeight() / 2 + i * averageHeight);
	                    canvas.drawBitmap(currBitmap, matrix, paint);
	                } else {
	                    camera.save();
	                    camera.rotateX(180 - rotateDegree);
	                    camera.getMatrix(matrix);
	                    camera.restore();

	                    matrix.preTranslate(-nextBitmap.getWidth() / 2, -nextBitmap.getHeight() / 2);
	                    matrix.postTranslate(nextBitmap.getWidth() / 2, nextBitmap.getHeight() / 2 + i * averageHeight);
	                    canvas.drawBitmap(nextBitmap, matrix, paint);
	                }


	            } else {
	                if (rotateDegree < 90) {
	                    camera.save();
	                    camera.rotateY(rotateDegree);
	                    camera.getMatrix(matrix);
	                    camera.restore();

	                    matrix.preTranslate(-currBitmap.getWidth() / 2, -currBitmap.getHeight() / 2);
	                    matrix.postTranslate(currBitmap.getWidth() / 2 + i * averageWidth, currBitmap.getHeight() / 2);
	                    canvas.drawBitmap(currBitmap, matrix, paint);
	                } else {
	                    camera.save();
	                    camera.rotateY(180 - rotateDegree);
	                    camera.getMatrix(matrix);
	                    camera.restore();

	                    matrix.preTranslate(-nextBitmap.getWidth() / 2, -nextBitmap.getHeight() / 2);
	                    matrix.postTranslate(nextBitmap.getWidth() / 2 + i * averageWidth, nextBitmap.getHeight() / 2);
	                    canvas.drawBitmap(nextBitmap, matrix, paint);
	                }

	            }
	            canvas.restore();
	        }
	    }
	    
	    
	
}
