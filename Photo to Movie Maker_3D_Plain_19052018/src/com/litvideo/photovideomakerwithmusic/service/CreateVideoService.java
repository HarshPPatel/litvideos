package com.litvideo.photovideomakerwithmusic.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.provider.MediaStore.Video.VideoColumns;
import android.support.v7.app.NotificationCompat;
import android.support.v7.app.NotificationCompat.Builder;
import android.text.TextUtils;
import android.util.Log;

import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.activity.VideoPlayActivity;
import com.litvideo.photovideomakerwithmusic.util.ScalingUtilities;
import com.litvideo.photovideomakerwithmusic.MyApplication;
import com.litvideo.photovideomakerwithmusic.OnProgressReceiver;
import com.videolib.libffmpeg.FileUtils;
import com.videolib.libffmpeg.Util;

public class CreateVideoService extends IntentService {
	MyApplication application;
	private Builder mBuilder;
	private NotificationManager mNotifyManager;
	public static final int NOTIFICATION_ID = 1001;
	private float toatalSecond;
	File imageFile;
	
	public CreateVideoService() {
		this(CreateVideoService.class.getName());
	}

	public CreateVideoService(String name) {
		super(name);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		application = MyApplication.getInstance();
		mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mBuilder = new NotificationCompat.Builder(this);
		mBuilder.setContentTitle("Creating Video")
				.setContentText("Making in progress")
				.setSmallIcon(R.drawable.icon);
		
		createVideo();
		
//		Bitmap icon = BitmapFactory.decodeResource(getResources(),
//				R.drawable.icon);

//		createDirectoryAndSaveFile(icon,"icon.png");
		
		
	}

	private void createDirectoryAndSaveFile(Bitmap imageToSave, String fileName) {

		File direct = FileUtils.APP_DIRECTORY;

	    if (!direct.exists()) {
	        File wallpaperDirectory = FileUtils.APP_DIRECTORY;
	        wallpaperDirectory.mkdirs();
	    }

	    File file = new File(FileUtils.APP_DIRECTORY, fileName);
	    if (file.exists()) {
	        file.delete();
	    }
	    try {
	        FileOutputStream out = new FileOutputStream(file);
	        imageToSave.compress(Bitmap.CompressFormat.JPEG, 100, out);
	        out.flush();
	        out.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    createVideo();
	}
	
	private void createVideo() {
		
		
//		imageFile = new File(Environment.getExternalStorageDirectory() + "/Lit Videos/icon.png");
//		Uri ur = Uri.parse("https://drive.google.com/open?id=1tA0AQ0qecHQcjLI3j2qZVEJg7XQm6stN");
//		imageFile = new File(ur.toString());
		long startTime = System.currentTimeMillis();

		toatalSecond = ((application.getSecond()
				* application.getSelectedImages().size() - 1));
		// toatalSecond = (long) (((long)application.getSecond() *
		// (long)application.videoImages
		// .size()) / (30));
		joinAudio();

		while (!ImageCreatorService.isImageComplate) {
			continue;
		}

		Log.e("createVideo", "video create start");
		// while (!application.isCompate()) {
		// continue;
		// }

		File videoIp = new File(FileUtils.TEMP_DIRECTORY, "video.txt");
		// File audioIp = new File(FileUtils.TEMP_DIRECTORY, "audio.txt");
		videoIp.delete();
		// audioIp.delete();
		for (int i = 0; i < application.videoImages.size(); i++) {
			appendVideoLog(String.format("file '%s'",
					application.videoImages.get(i)));
		}

		final String videoPath = new File(FileUtils.APP_DIRECTORY,
				getVideoName()).getAbsolutePath();

		String[] inputCode;
		if (application.getMusicData() != null) {

			if (application.getFrame() != -1) {

				if (!FileUtils.frameFile.exists()) {
					try {
						Bitmap bm = BitmapFactory.decodeResource(
								getResources(), application.getFrame());
						if (bm.getWidth() != MyApplication.VIDEO_WIDTH
								|| bm.getHeight() != MyApplication.VIDEO_HEIGHT) {
							bm = ScalingUtilities.scaleCenterCrop(bm,
									MyApplication.VIDEO_WIDTH,
									MyApplication.VIDEO_HEIGHT);
						}
						FileOutputStream outStream = new FileOutputStream(
								FileUtils.frameFile);
						bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
						outStream.flush();
						outStream.close();
						bm.recycle();
						System.gc();
					} catch (Exception exception) {
					}
				}

				inputCode = new String[] { FileUtils.getFFmpeg(this),
						"-r",
						"" + ((15 * 2) / application.getSecond()),
						"-f",
						"concat",
						"-safe",
						"0",
						"-i",
						videoIp.getAbsolutePath(),
						"-i",
						FileUtils.frameFile.getAbsolutePath(),
//						"-i",
//						imageFile.getPath(),
						"-i",
						audioFile.getAbsolutePath(),
						"-filter_complex",
						// "[1:v]scale=720x480;overlay=(W-w)/2:(H-h)/2",

						// "[0:v] scale=720x480 [wm]; [wm][1:v]  overlay=0:0 [v]",
						// "-map", "[v]",

						"overlay= 0:0", "-strict",
						"experimental",
						"-r",
						"" + ((15 * 2) / application.getSecond())
						// "30"

						, "-t", "" + toatalSecond, "-c:v", "libx264",
						"-preset", "ultrafast", "-pix_fmt", "yuv420p", "-ac",
						"2", videoPath };

			} else {

				inputCode = new String[] { FileUtils.getFFmpeg(this), "-r",
						"" + ((15 * 2) / application.getSecond()), "-f",
						"concat", "-safe", "0", "-i",
						videoIp.getAbsolutePath(), 
						"-i",
						audioFile.getAbsolutePath(), "-strict", "experimental",
						"-r", "30", "-t", "" + toatalSecond, "-c:v", "libx264",
						"-preset", "ultrafast",

						"-pix_fmt", "yuv420p", "-ac", "2", videoPath };
			}

		} else {
			inputCode = new String[] { FileUtils.getFFmpeg(this), "-r",
					"" + ((15 * 2) / application.getSecond()), "-f", "concat",
					"-i", videoIp.getAbsolutePath(), "-r", "30", "-c:v",
					"libx264", "-preset", "ultrafast", "-pix_fmt", "yuv420p",
					videoPath };
		}

		System.gc();
		Process process = null;
		try {
			process = Runtime.getRuntime().exec(inputCode);
			while (true) {
				if (Util.isProcessCompleted(process)) {
					break;
				}
				String line;
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(process.getErrorStream()));
				while ((line = reader.readLine()) != null) {

					Log.e("process", line);
					final int incr = durationToprogtess(line);// (i*100)/application.videoImages.size();

					int p = 25 + (int) (75f * incr / 100f);
					mBuilder.setProgress(100, p, false);
					mNotifyManager.notify(NOTIFICATION_ID, mBuilder.build());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			Util.destroyProcess(process);
		}
		mBuilder.setContentText(
				"Video created :"
						+ FileUtils.getDuration(System.currentTimeMillis()
								- startTime)

		).setProgress(0, 0, false);
		mNotifyManager.notify(NOTIFICATION_ID, mBuilder.build());

		try {
			File videoFile = new File(videoPath);

			long fileSize = videoFile.length();
			String mimeType = "video/mp4";

			String artist = "" + getResources().getText(R.string.artist_name);

			ContentValues values = new ContentValues();
			values.put(MediaStore.MediaColumns.DATA, videoPath);
			values.put(MediaStore.MediaColumns.SIZE, fileSize);
			values.put(MediaStore.MediaColumns.MIME_TYPE, mimeType);

			values.put(VideoColumns.ARTIST, artist);
			values.put(VideoColumns.DURATION, toatalSecond * 1000l);

			// Insert it into the database
			Uri uri = MediaStore.Audio.Media.getContentUriForPath(videoPath);
			getContentResolver().insert(uri, values);

		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
					Uri.fromFile(new File(videoPath))));
		} catch (Exception e) {
			e.printStackTrace();
		}

		application.clearAllSelection();
		// Intent intent = new Intent(this, VideoPlayActivity.class);
		// intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		// intent.putExtra(Intent.EXTRA_TEXT, videoPath);
		// startActivity(intent);

		buildNotification(videoPath);

		Handler handler = new Handler(Looper.getMainLooper());
		handler.post(new Runnable() {
			@Override
			public void run() {
				OnProgressReceiver receiver = application
						.getOnProgressReceiver();
				if (receiver != null) {
					receiver.onVideoProgressFrameUpdate(100f);
					receiver.onProgressFinish(videoPath);
				}
			}
		});
		FileUtils.deleteTempDir();

		application.setFrame(-1);
		stopSelf();

	}

	String timeRe = "\\btime=\\b\\d\\d:\\d\\d:\\d\\d.\\d\\d";
	private File audioIp;
	private File audioFile;

	private void buildNotification(String videoPath) {

		Intent notificationIntent = new Intent(this, VideoPlayActivity.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		notificationIntent.putExtra(Intent.EXTRA_TEXT, videoPath);

		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

		Resources res = getResources();
		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				this);
		builder.setContentIntent(contentIntent)
				.setSmallIcon(R.drawable.icon)
				.setLargeIcon(
						BitmapFactory.decodeResource(res, R.drawable.icon))
				.setWhen(System.currentTimeMillis()).setAutoCancel(true)
				.setContentTitle(getResources().getString(R.string.app_name))
				.setContentText("Video Created");
		Notification n = builder.build();

		n.defaults |= Notification.DEFAULT_ALL;
		mNotifyManager.notify(NOTIFICATION_ID, n);

		// TODO Auto-generated method stub

	}

	int last = 0;

	private int durationToprogtess(String input) {
		int progress = 0;
		Matcher matcher = Pattern.compile(timeRe).matcher(input);
		int SECOND = 1;
		int MINUTE = SECOND * 60;
		int HOUR = MINUTE * 60;

		if (TextUtils.isEmpty(input) || !input.contains("time=")) {
			Log.e("time", "not contain time " + input);
			return last;
		}
		while (matcher.find()) {
			String time = matcher.group();
			time = time.substring(time.lastIndexOf('=') + 1);
			String[] splitTime = time.split(":");
			float hour = (Float.valueOf(splitTime[0])) * HOUR// 12h
					+ Float.valueOf(splitTime[1]) * MINUTE// 59m
					+ Float.valueOf(splitTime[2]);// 10.96s
			Log.e("time", "totalSecond:" + hour);
			progress = (int) ((hour * 100) / toatalSecond);
			updateInMili(hour);
		}
		last = progress;
		return progress;
	}

	private void updateInMili(float time) {
		final double progress = ((time * 100d) / toatalSecond);
		Handler handler = new Handler(Looper.getMainLooper());
		handler.post(new Runnable() {
			@Override
			public void run() {
				OnProgressReceiver receiver = application
						.getOnProgressReceiver();
				if (receiver != null) {
					Log.e("timeToatal", "progress __" + progress);
					receiver.onVideoProgressFrameUpdate((float) progress);
				}
			}
		});
	}

	private void joinAudio() {
		audioIp = new File(FileUtils.TEMP_DIRECTORY, "audio.txt");
		audioFile = new File(FileUtils.APP_DIRECTORY, "audio.mp3");
		audioFile.delete();
		audioIp.delete();
		// appendLog("===============================================");
		for (int d = 0;/* d<application.getSelectedImages().size() */; d++) {
			appendAudioLog(String.format("file '%s'",
					application.getMusicData().track_data));

			Log.e("audio", d + " is D  " + (toatalSecond * 1000) + "___"
					+ (application.getMusicData().track_duration * d));

			// appendLog((toatalSecond *
			// 1000)+"___"+(application.getMusicData().track_duration
			// * d));
			if (toatalSecond * 1000 <= application.getMusicData().track_duration
					* d) {
				break;
			}
		}

		// appendLog("Joid Audio");
		// appendLog("===============================================");
		String[] inputCode = new String[] { FileUtils.getFFmpeg(this), "-f",
				"concat", "-safe", "0", "-i", audioIp.getAbsolutePath(),
				// "-t",""+toatalSecond,
				"-c", "copy", "-preset", "ultrafast", "-ac", "2",
				audioFile.getAbsolutePath() };

		Process process = null;
		try {

			process = Runtime.getRuntime().exec(inputCode);
			while (true) {
				if (Util.isProcessCompleted(process)) {
					break;
				}
				String line;
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(process.getErrorStream()));
				while ((line = reader.readLine()) != null) {
					Log.i("audio", line);
					// appendLog(line);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			Log.e("audio", "io", e);
		} finally {
			Util.destroyProcess(process);
		}
		// appendLog("===============================================");
	}

	private String getVideoName() {
		SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy_MMM_dd_HH_mm_ss",
				Locale.ENGLISH);
		return "video_" + fmtOut.format(new Date()) + ".mp4";

	}

	public static void appendVideoLog(String text) {
		if (!FileUtils.TEMP_DIRECTORY.exists()) {
			FileUtils.TEMP_DIRECTORY.mkdirs();
		}
		File logFile = new File(FileUtils.TEMP_DIRECTORY, "video.txt");
		Log.d("FFMPEG", "File append " + text);
		if (!logFile.exists()) {
			try {
				logFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			BufferedWriter buf = new BufferedWriter(new FileWriter(logFile,
					true));
			buf.append(text);
			buf.newLine();
			buf.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void appendAudioLog(String text) {
		if (!FileUtils.TEMP_DIRECTORY.exists()) {
			FileUtils.TEMP_DIRECTORY.mkdirs();
		}
		File logFile = new File(FileUtils.TEMP_DIRECTORY, "audio.txt");
		// Log.d("FFMPEG", "File append " + text);
		if (!logFile.exists()) {
			try {
				logFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			BufferedWriter buf = new BufferedWriter(new FileWriter(logFile,
					true));
			buf.append(text);
			buf.newLine();
			buf.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
