package com.litvideo.photovideomakerwithmusic.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v7.app.NotificationCompat;
import android.support.v7.app.NotificationCompat.Builder;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.data.ImageData;
import com.litvideo.photovideomakerwithmusic.mask.FinalMaskBitmap3D;
import com.litvideo.photovideomakerwithmusic.mask.FinalMaskBitmap3D.EFFECT;
import com.litvideo.photovideomakerwithmusic.util.ScalingUtilities;
import com.litvideo.photovideomakerwithmusic.MyApplication;
import com.litvideo.photovideomakerwithmusic.OnProgressReceiver;
import com.videolib.libffmpeg.FileUtils;

public class ImageCreatorService extends IntentService {

	public static boolean isImageComplate =false;
	
	public static final Object mLock = new Object();
	ArrayList<ImageData> arrayList;
	MyApplication application;
	int totalImages;
	
	public static final String ACTION_CREATE_NEW_THEME_IMAGES="ACTION_CREATE_NEW_THEME_IMAGES";
	public static final String ACTION_UPDATE_THEME_IMAGES="ACTION_UPDATE_THEME_IMAGES";
	public static final String EXTRA_SELECTED_THEME="selected_theme";
	private String selectedTheme;

	private NotificationManager mNotifyManager;

	private Builder mBuilder;
	
	
	public ImageCreatorService() {
		this(ImageCreatorService.class.getName());
	}
	public ImageCreatorService(String name) {
		super(name);
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		application = MyApplication.getInstance();
	}
	
	@Override
	@Deprecated
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
	}
	
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	@Override
	protected void onHandleIntent(Intent intent) {
		mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mBuilder = new NotificationCompat.Builder(this);
		mBuilder.setContentTitle("Preparing Video")
				.setContentText("Making in progress")
				.setSmallIcon(R.drawable.icon);
		 
		selectedTheme = intent.getStringExtra(EXTRA_SELECTED_THEME);
//		Log.i("ImageCreatorService", "selected Thme"+selectedTheme);
		arrayList = application.getSelectedImages();
		application.initArray();
		isImageComplate =false;
		createImages();
		  
	}
	
	
	
@SuppressWarnings("deprecation")
private void createImages() {
//		FileUtils.deleteFile(FileUtils.TEMP_DIRECTORY);
		Bitmap newFirstBmp;
		Bitmap newSecondBmp2=null;
		totalImages =arrayList.size();
		for(int i=0 ;i<arrayList.size()-1;i++){
			if(!isSameTheme() || MyApplication.isBreak){
				Log.e("ImageCreatorService",selectedTheme+ " break" );
				break;
			}
//			Log.e("ImageCreatorService",selectedTheme+ "images:"+i);
			
		File  imgDir = FileUtils.getImageDirectory(application.selectedTheme.toString(),i);
		if(i==0){
			Bitmap firstBitmap = ScalingUtilities.checkBitmap(arrayList.get(i ).imagePath);
			Bitmap temp = ScalingUtilities.scaleCenterCrop(firstBitmap, MyApplication.VIDEO_WIDTH,MyApplication.VIDEO_HEIGHT);
			newFirstBmp = ScalingUtilities.ConvetrSameSizeNew(firstBitmap, temp,  MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT );
			temp.recycle();
			temp=null;
			firstBitmap .recycle();
			firstBitmap =null;
			System.gc();
		}else{
			if(newSecondBmp2==null || newSecondBmp2.isRecycled()){
				Bitmap firstBitmap = ScalingUtilities.checkBitmap(arrayList.get(i ).imagePath);
				Bitmap temp = ScalingUtilities.scaleCenterCrop(firstBitmap, MyApplication.VIDEO_WIDTH,MyApplication.VIDEO_HEIGHT);
				newSecondBmp2 = ScalingUtilities.ConvetrSameSizeNew(firstBitmap, temp,  MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT );
				temp.recycle();
				temp=null;
				firstBitmap .recycle();
				firstBitmap =null;
			}
			newFirstBmp = newSecondBmp2;
			
			
			
		}
			Bitmap secondBitmap = ScalingUtilities.checkBitmap(arrayList.get(i+1).imagePath);
			Bitmap temp2 = ScalingUtilities.scaleCenterCrop(secondBitmap,  MyApplication.VIDEO_WIDTH,MyApplication.VIDEO_HEIGHT);
//			newSecondBmp2 = ScalingUtilities.ConvetrSameSize(secondBitmap, temp2,  MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, 1f, 0);
			newSecondBmp2 = ScalingUtilities.ConvetrSameSizeNew(secondBitmap, temp2,  MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT );
			temp2.recycle();
			temp2 =null;
			secondBitmap.recycle();
			secondBitmap=null;
			System.gc();
			
//			FinalMaskBitmap3D.reintRect();
			EFFECT effect = application.selectedTheme.getTheme().get(i%application.selectedTheme.getTheme().size());//    EFFECT.values()[i%EFFECT.values().length];
			effect.initBitmaps(newFirstBmp, newSecondBmp2);
			
			
			
			for(int j = 0; j <FinalMaskBitmap3D.ANIMATED_FRAME; j++){
				
			 
				{
					if(!isSameTheme() || MyApplication.isBreak){
						Log.e("ImageCreatorService",selectedTheme+ " break" );
						break;
					}
						final Bitmap bitmap3 =effect.getMask(newFirstBmp, newSecondBmp2, j);
						if(!isSameTheme()){
							break;
						}
						File file = new File(imgDir,String.format("img%02d.jpg", j));
						FileOutputStream var5;
						try {
							if(file.exists()){
								file.delete();
							}
							var5 = new FileOutputStream(file);
							bitmap3.compress(CompressFormat.JPEG, 100, var5);
							var5.close();
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
						 
						boolean isBreak=false;
						while (application.isEditModeEnable) {
							
							Log.e("ImageCreatorService", "application.isEditModeEnable :" );
							if(application.min_pos!=Integer.MAX_VALUE){
								i=application.min_pos;
								isBreak=true;
							}
							if(MyApplication.isBreak){
								Log.e("ImageCreatorService", "application.isEditModeEnable Break:" );
								isImageComplate =true;
								stopSelf();
								return;
							}
						}
						if(isBreak){
							isBreak=false;
							ArrayList<String> str = new ArrayList<>();
							str.addAll(application.videoImages);
							application.videoImages.clear();
							int size = Math.min(str.size(),(Math.max(0, i-i)  *30));
							for(int p=0;p<size;p++){
								application.videoImages.add(str.get(p));
							}
							application.min_pos=Integer.MAX_VALUE;
						}
						
						if(!isSameTheme() || MyApplication.isBreak){
							Log.i("ImageCreatorService",selectedTheme+ " :" );
							break;
						}else{
							application.videoImages.add(file.getAbsolutePath());
							updateImageProgress();
						}
						if(j==(FinalMaskBitmap3D.ANIMATED_FRAME-1)){
						for(int m=0;m<8;m++){
							 
							if(!isSameTheme() || MyApplication.isBreak){
								break;
							}else{
								application.videoImages.add(file.getAbsolutePath());
							}
							updateImageProgress();
						}
					}
				}
			}
		}
//		
		Glide.get(this).clearDiskCache();
		isImageComplate =true;
 
		stopSelf();
		 if(isSameTheme()){
		 }
		 
	}

	private boolean isSameTheme() {
		return selectedTheme.equals(application.getCurrentTheme());
	}
	private void updateImageProgress() {
		int totalImageSize = (totalImages-1)*30;
		final float progress =   (100f*application.videoImages.size()/(float)totalImageSize);
		Handler handler = new Handler(Looper.getMainLooper());
		handler.post(new Runnable() {
			@Override
			public void run() {
				OnProgressReceiver receiver = application
						.getOnProgressReceiver();
				if (receiver != null) {
					receiver.onImageProgressFrameUpdate(progress);
				}
			}
		});
	}
	
	
	private void updateNotification(int progress) {
		int p = (int) (25f*progress/100f);
		mBuilder.setProgress(100, p, false);
		mNotifyManager.notify(CreateVideoService.NOTIFICATION_ID, mBuilder.build());
	}
}
