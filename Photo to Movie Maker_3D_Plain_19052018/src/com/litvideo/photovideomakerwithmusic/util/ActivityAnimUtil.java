package com.litvideo.photovideomakerwithmusic.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.litvideo.photovideomakerwithmusic.R;

public class ActivityAnimUtil {

	private static Method sClipRevealMethod = null;
	static {
		Class<?> activityOptionsClass = ActivityOptions.class;
		try {
			sClipRevealMethod = activityOptionsClass.getDeclaredMethod(
					"makeClipRevealAnimation", View.class, int.class,
					int.class, int.class, int.class);
		} catch (Exception e) {
		}
	}

	// @Thunk
	public static boolean startActivitySafely(View v, Intent intent) {

		boolean success = false;
		try {
			success = startActivity(v, intent);

		} catch (ActivityNotFoundException e) {
			Toast.makeText(v.getContext(), R.string.applicatoin_not_found,
					Toast.LENGTH_SHORT).show();
		}

		return success;
	}

	@TargetApi(23)
	private static boolean startActivity(View v, Intent intent) {
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		Context context = v.getContext();
		try {
			boolean useLaunchAnimation = (v != null);
			Bundle optsBundle = null;
			if (useLaunchAnimation) {
				ActivityOptions opts = null;
				if (sClipRevealMethod != null) {
					// APIs
					int left = 0, top = 0;
					int width = v.getMeasuredWidth(), height = v
							.getMeasuredHeight();
					try {
						opts = (ActivityOptions) sClipRevealMethod.invoke(null,
								v, left, top, width, height);
					} catch (IllegalAccessException e) {
						sClipRevealMethod = null;
					} catch (InvocationTargetException e) {
						sClipRevealMethod = null;
					}
				}
				if (opts == null && !Utils.isLmpOrAbove()) {
					opts = ActivityOptions.makeScaleUpAnimation(v, 0, 0,
							v.getMeasuredWidth(), v.getMeasuredHeight());
				} else if (opts == null && Utils.isLmpMR1()) {
					opts = ActivityOptions.makeCustomAnimation(context,
							R.anim.task_open_enter, R.anim.no_anim);
				}
				optsBundle = opts != null ? opts.toBundle() : null;
			}
			context.startActivity(intent, optsBundle);
			return true;
		} catch (SecurityException e) {
		}
		return false;
	}
}
