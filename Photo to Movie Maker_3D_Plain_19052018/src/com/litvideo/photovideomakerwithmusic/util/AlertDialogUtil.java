package com.litvideo.photovideomakerwithmusic.util;

import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.widget.Button;

import com.litvideo.photovideomakerwithmusic.MyApplication;

public class AlertDialogUtil {

	public static void setDailogTyepFace(AlertDialog alertDialog) {

		Typeface tf = MyApplication.getInstance()
				.getApplicationTypeFace();

		((Button) alertDialog.findViewById(android.R.id.button1))
				.setTypeface(tf);
		((Button) alertDialog.findViewById(android.R.id.button2))
				.setTypeface(tf);
		((Button) alertDialog.findViewById(android.R.id.button3))
				.setTypeface(tf);

	}

}
