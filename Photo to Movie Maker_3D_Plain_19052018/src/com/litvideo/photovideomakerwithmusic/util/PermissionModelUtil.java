package com.litvideo.photovideomakerwithmusic.util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;

import com.litvideo.photovideomakerwithmusic.R;

@SuppressLint("NewApi")
public class PermissionModelUtil {
	public static final String[] NECESSARY_PERMISSIONS = new String[] {
			Manifest.permission.READ_EXTERNAL_STORAGE,
			Manifest.permission.WRITE_EXTERNAL_STORAGE,
	};
	private static final String PERMISSION_CHECK_PREF = "marshmallow_permission_check";
	private Context context;
	private SharedPreferences sharedPrefs;
	private PermissionModelUtil() {
		 
	}
	public PermissionModelUtil(Context context) {
		this.context = context;
		this.sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context);
	}
	public boolean needPermissionCheck() {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
			return false;
		} else {
			return context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED;
//			return sharedPrefs.getBoolean(PERMISSION_CHECK_PREF, true);
		}
	}
	public void showPermissionExplanationThenAuthorization() {
		new AlertDialog.Builder(context, R.style.Theme_MovieMaker_AlertDialog)
				.setTitle(R.string.permission_check_title)
				.setMessage(R.string.permission_check_message)
				.setPositiveButton(R.string.ok,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								requestPermissions();
								sharedPrefs
										.edit()
										.putBoolean(PERMISSION_CHECK_PREF,
												false).commit();
							}
						}).setCancelable(false).create().show();
	}
	private void requestPermissions() {
		((Activity) context).requestPermissions(NECESSARY_PERMISSIONS, 1);
	}
}
