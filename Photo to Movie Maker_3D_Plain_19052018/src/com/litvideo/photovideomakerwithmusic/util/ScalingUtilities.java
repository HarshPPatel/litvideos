package com.litvideo.photovideomakerwithmusic.util;

import java.io.IOException;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.BlurMaskFilter;
import android.graphics.BlurMaskFilter.Blur;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Matrix.ScaleToFit;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Log;

/**
 * Class containing static utility methods for bitmap decoding and scaling
 * 
 * @author Andreas Agvard (andreas.agvard@sonyericsson.com)
 */
public class ScalingUtilities {

	/**
	 * Utility function for decoding an image resource. The decoded bitmap will
	 * be optimized for further scaling to the requested destination dimensions
	 * and scaling logic.
	 * 
	 * @param res
	 *            The resources object containing the image data
	 * @param resId
	 *            The resource id of the image data
	 * @param dstWidth
	 *            Width of destination area
	 * @param dstHeight
	 *            Height of destination area
	 * @param scalingLogic
	 *            Logic to use to avoid image stretching
	 * @return Decoded bitmap
	 */
	public static Bitmap decodeResource(Resources res, int resId, int dstWidth,
			int dstHeight, ScalingLogic scalingLogic) {
		Options options = new Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);
		options.inJustDecodeBounds = false;
		options.inSampleSize = calculateSampleSize(options.outWidth,
				options.outHeight, dstWidth, dstHeight, scalingLogic);
		Bitmap unscaledBitmap = BitmapFactory.decodeResource(res, resId,
				options);

		return unscaledBitmap;
	}

	/**
	 * Utility function for creating a scaled version of an existing bitmap
	 * 
	 * @param unscaledBitmap
	 *            Bitmap to scale
	 * @param dstWidth
	 *            Wanted width of destination bitmap
	 * @param dstHeight
	 *            Wanted height of destination bitmap
	 * @param scalingLogic
	 *            Logic to use to avoid image stretching
	 * @return New scaled bitmap object
	 */
	public static Bitmap createScaledBitmap(Bitmap unscaledBitmap,
			int dstWidth, int dstHeight, ScalingLogic scalingLogic) {
		Rect srcRect = calculateSrcRect(unscaledBitmap.getWidth(),
				unscaledBitmap.getHeight(), dstWidth, dstHeight, scalingLogic);
		Rect dstRect = calculateDstRect(unscaledBitmap.getWidth(),
				unscaledBitmap.getHeight(), dstWidth, dstHeight, scalingLogic);
		Bitmap scaledBitmap = Bitmap.createBitmap(dstRect.width(),
				dstRect.height(), Config.ARGB_8888);
		Canvas canvas = new Canvas(scaledBitmap);
		canvas.drawBitmap(unscaledBitmap, srcRect, dstRect, new Paint(
				Paint.FILTER_BITMAP_FLAG));

		return scaledBitmap;
	}

	/**
	 * ScalingLogic defines how scaling should be carried out if source and
	 * destination image has different aspect ratio.
	 * 
	 * CROP: Scales the image the minimum amount while making sure that at least
	 * one of the two dimensions fit inside the requested destination area.
	 * Parts of the source image will be cropped to realize this.
	 * 
	 * FIT: Scales the image the minimum amount while making sure both
	 * dimensions fit inside the requested destination area. The resulting
	 * destination dimensions might be adjusted to a smaller size than
	 * requested.
	 */
	public static enum ScalingLogic {
		CROP, FIT
	}

	/**
	 * Calculate optimal down-sampling factor given the dimensions of a source
	 * image, the dimensions of a destination area and a scaling logic.
	 * 
	 * @param srcWidth
	 *            Width of source image
	 * @param srcHeight
	 *            Height of source image
	 * @param dstWidth
	 *            Width of destination area
	 * @param dstHeight
	 *            Height of destination area
	 * @param scalingLogic
	 *            Logic to use to avoid image stretching
	 * @return Optimal down scaling sample size for decoding
	 */
	public static int calculateSampleSize(int srcWidth, int srcHeight,
			int dstWidth, int dstHeight, ScalingLogic scalingLogic) {
		if (scalingLogic == ScalingLogic.FIT) {
			final float srcAspect = (float) srcWidth / (float) srcHeight;
			final float dstAspect = (float) dstWidth / (float) dstHeight;

			if (srcAspect > dstAspect) {
				return srcWidth / dstWidth;
			} else {
				return srcHeight / dstHeight;
			}
		} else {
			final float srcAspect = (float) srcWidth / (float) srcHeight;
			final float dstAspect = (float) dstWidth / (float) dstHeight;

			if (srcAspect > dstAspect) {
				return srcHeight / dstHeight;
			} else {
				return srcWidth / dstWidth;
			}
		}
	}

	/**
	 * Calculates source rectangle for scaling bitmap
	 * 
	 * @param srcWidth
	 *            Width of source image
	 * @param srcHeight
	 *            Height of source image
	 * @param dstWidth
	 *            Width of destination area
	 * @param dstHeight
	 *            Height of destination area
	 * @param scalingLogic
	 *            Logic to use to avoid image stretching
	 * @return Optimal source rectangle
	 */
	public static Rect calculateSrcRect(int srcWidth, int srcHeight,
			int dstWidth, int dstHeight, ScalingLogic scalingLogic) {
		if (scalingLogic == ScalingLogic.CROP) {
			final float srcAspect = (float) srcWidth / (float) srcHeight;
			final float dstAspect = (float) dstWidth / (float) dstHeight;

			if (srcAspect > dstAspect) {
				final int srcRectWidth = (int) (srcHeight * dstAspect);
				final int srcRectLeft = (srcWidth - srcRectWidth) / 2;
				return new Rect(srcRectLeft, 0, srcRectLeft + srcRectWidth,
						srcHeight);
			} else {
				final int srcRectHeight = (int) (srcWidth / dstAspect);
				final int scrRectTop = (srcHeight - srcRectHeight) / 2;
				return new Rect(0, scrRectTop, srcWidth, scrRectTop
						+ srcRectHeight);
			}
		} else {
			return new Rect(0, 0, srcWidth, srcHeight);
		}
	}

	/**
	 * Calculates destination rectangle for scaling bitmap
	 * 
	 * @param srcWidth
	 *            Width of source image
	 * @param srcHeight
	 *            Height of source image
	 * @param dstWidth
	 *            Width of destination area
	 * @param dstHeight
	 *            Height of destination area
	 * @param scalingLogic
	 *            Logic to use to avoid image stretching
	 * @return Optimal destination rectangle
	 */
	public static Rect calculateDstRect(int srcWidth, int srcHeight,
			int dstWidth, int dstHeight, ScalingLogic scalingLogic) {
		if (scalingLogic == ScalingLogic.FIT) {
			final float srcAspect = (float) srcWidth / (float) srcHeight;
			final float dstAspect = (float) dstWidth / (float) dstHeight;

			if (srcAspect > dstAspect) {
				return new Rect(0, 0, dstWidth, (int) (dstWidth / srcAspect));
			} else {
				return new Rect(0, 0, (int) (dstHeight * srcAspect), dstHeight);
			}
		} else {
			return new Rect(0, 0, dstWidth, dstHeight);
		}
	}

	public static Bitmap ConvetrSameSize(Bitmap originalImage,
			int mDisplayWidth, int mDisplayHeight) {
		Bitmap cs = null, bitmap;
		bitmap = originalImage;
		cs = Bitmap.createBitmap(mDisplayWidth, mDisplayHeight,
				Bitmap.Config.ARGB_8888);
		Canvas comboImage = new Canvas(cs);

		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, mDisplayWidth, mDisplayHeight);
		final int color = 0xff424242;
		paint.setAntiAlias(true);
		comboImage.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		comboImage.drawRect(rect, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		bitmap = newscaleBitmap(bitmap, mDisplayWidth, mDisplayHeight);
		comboImage.drawBitmap(bitmap, 0f, 0f, paint);
		return cs;
	}

	public static Bitmap ConvetrSameSizeTransBg(Bitmap originalImage,
			int mDisplayWidth, int mDisplayHeight) {
		Bitmap cs = null, bitmap;
		bitmap = originalImage;
		cs = Bitmap.createBitmap(mDisplayWidth, mDisplayHeight,
				Bitmap.Config.ARGB_8888);
		Canvas comboImage = new Canvas(cs);

		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, mDisplayWidth, mDisplayHeight);
		final int color = 0xff424242;
		// paint.setAntiAlias(true);
		// comboImage.drawARGB(0, 0, 0, 0);
		// paint.setColor(color);
		// comboImage.drawRect(rect, paint);
		// paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		bitmap = newscaleBitmap(bitmap, mDisplayWidth, mDisplayHeight);
		comboImage.drawBitmap(bitmap, 0f, 0f, paint);
		return cs;
	}

	public static Bitmap ConvetrSameSize(Bitmap originalImage, Bitmap bgBitmap,
			int mDisplayWidth, int mDisplayHeight) {
		Bitmap cs = null, bitmap;
		bitmap = originalImage;
		// cs = Bitmap.createBitmap(mDisplayWidth, mDisplayHeight,
		// Bitmap.Config.ARGB_8888);
		cs = bgBitmap;
		Canvas comboImage = new Canvas(cs);

		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, mDisplayWidth, mDisplayHeight);
		final int color = 0xff424242;
		// paint.setAntiAlias(true);
		// comboImage.drawARGB(0, 0, 0, 0);
		// paint.setColor(color);
		// comboImage.drawRect(rect, paint);
		// paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		bitmap = newscaleBitmap(bitmap, mDisplayWidth, mDisplayHeight);
		comboImage.drawBitmap(bitmap, 0f, 0f, paint);
		return cs;
	}

	public static Bitmap ConvetrSameSize(Bitmap originalImage, Bitmap bgBitmap,
			int mDisplayWidth, int mDisplayHeight, float x, float y) {
		Bitmap cs = null, bitmap;
		bitmap = originalImage;
		cs = bgBitmap.copy(bgBitmap.getConfig(), true);
		Canvas comboImage = new Canvas(FastBlur.doBlur(cs, 25, true));
		final Paint paint = new Paint();
		bitmap = newscaleBitmap(bitmap, mDisplayWidth, mDisplayHeight, x, y);
		comboImage.drawBitmap(bitmap, 0f, 0f, paint);
		return cs;
	}

	public static Bitmap ConvetrSameSizeNew(Bitmap originalImage,
			Bitmap bgBitmap, int mDisplayWidth, int mDisplayHeight) {
		Bitmap cs = null;
		// cs = bgBitmap.copy(Config.ARGB_8888, true);

		cs = FastBlur.doBlur(bgBitmap, 25, true);
		Canvas comboImage = new Canvas(cs);
		final Paint paint = new Paint();
		float originalWidth = originalImage.getWidth(), originalHeight = originalImage
				.getHeight();
		float scale = mDisplayWidth / originalWidth;
		float scaleY = mDisplayHeight / originalHeight;
		float xTranslation = 0.0f, yTranslation = (mDisplayHeight - originalHeight
				* scale) / 2.0f;
		if (yTranslation < 0) {
			yTranslation = 0;
			scale = mDisplayHeight / originalHeight;
			xTranslation = (mDisplayWidth - originalWidth * scaleY) / 2.0f;
		}
		Matrix transformation = new Matrix();
		transformation.postTranslate(xTranslation, yTranslation);
		transformation.preScale(scale, scale);
		comboImage.drawBitmap(originalImage, transformation, paint);

		return cs;
	}

	public static Bitmap scaleCenterCrop(Bitmap source, int newWidth,
			int newHeight) {
		int sourceWidth = source.getWidth();
		int sourceHeight = source.getHeight();
		if (sourceWidth == newWidth && sourceHeight == newHeight) {
			return source;
		}
		float xScale = (float) newWidth / sourceWidth;
		float yScale = (float) newHeight / sourceHeight;
		float scale = Math.max(xScale, yScale);
		float scaledWidth = scale * sourceWidth;
		float scaledHeight = scale * sourceHeight;
		float left = (newWidth - scaledWidth) / 2;
		float top = (newHeight - scaledHeight) / 2;
		RectF targetRect = new RectF(left, top, left + scaledWidth, top
				+ scaledHeight);
		Bitmap dest = Bitmap.createBitmap(newWidth, newHeight,
				source.getConfig());
		Canvas canvas = new Canvas(dest);

		canvas.drawBitmap(source, null, targetRect, null);
		return dest;
	}

	private static Bitmap newscaleBitmap(Bitmap originalImage, int width,
			int height, float x, float y) {
		Bitmap background = Bitmap
				.createBitmap(width, height, Config.ARGB_8888);
		float originalWidth = originalImage.getWidth(), originalHeight = originalImage
				.getHeight();
		Canvas canvas = new Canvas(background);
		float scale = width / originalWidth;
		float scaleY = height / originalHeight;
		float xTranslation = 0.0f, yTranslation = (height - originalHeight
				* scale) / 2.0f;
		if (yTranslation < 0) {
			yTranslation = 0;
			scale = height / originalHeight;
			xTranslation = (width - originalWidth * scaleY) / 2.0f;
		}
		Matrix transformation = new Matrix();
		transformation.postTranslate(xTranslation * x, yTranslation + y);
		Log.d("translation", "xTranslation :" + xTranslation
				+ " yTranslation :" + yTranslation);
		// transformation.postTranslate(1f, 1f);
		transformation.preScale(scale, scale);
		Paint paint = new Paint();
		// paint.setFilterBitmap(true);
		canvas.drawBitmap(originalImage, transformation, paint);
		return background;
	}

	public static Bitmap addShadow(final Bitmap bm, int color, int size,
			float dx, float dy) {
		final Bitmap mask = Bitmap.createBitmap(bm.getWidth() + size * 2,
				bm.getHeight(), Config.ALPHA_8);

		final Matrix scaleToFit = new Matrix();
		final RectF src = new RectF(0, 0, bm.getWidth(), bm.getHeight());
		final RectF dst = new RectF(0, 0, bm.getWidth() - dx - size,
				bm.getHeight() - dy);
		scaleToFit.setRectToRect(src, dst, ScaleToFit.CENTER);

		final Matrix dropShadow = new Matrix(scaleToFit);
		dropShadow.postTranslate(dx, dy);

		final Canvas maskCanvas = new Canvas(mask);
		final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		maskCanvas.drawBitmap(bm, scaleToFit, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_OUT));
		maskCanvas.drawBitmap(bm, dropShadow, paint);

		final BlurMaskFilter filter = new BlurMaskFilter(size, Blur.NORMAL);
		paint.reset();
		paint.setAntiAlias(true);
		paint.setColor(color);
		paint.setMaskFilter(filter);
		paint.setFilterBitmap(true);

		final Bitmap ret = Bitmap.createBitmap(bm.getWidth(), bm.getHeight(),
				Config.ARGB_8888);
		final Canvas retCanvas = new Canvas(ret);
		retCanvas.drawBitmap(mask, 0, 0, paint);
		retCanvas.drawBitmap(bm, scaleToFit, null);
		mask.recycle();
		return ret;
	}

	@SuppressLint("NewApi")
	public static Bitmap blurBitmap(Bitmap bitmap, Context context) {

		// Let's create an empty bitmap with the same size of the bitmap we want
		// to blur
		Bitmap outBitmap = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), Config.ARGB_8888);
		RenderScript rs = RenderScript.create(context);
		ScriptIntrinsicBlur blurScript = ScriptIntrinsicBlur.create(rs,
				Element.U8_4(rs));
		Allocation allIn = Allocation.createFromBitmap(rs, bitmap);
		Allocation allOut = Allocation.createFromBitmap(rs, outBitmap);
		blurScript.setRadius(25.f);
		blurScript.setInput(allIn);
		blurScript.forEach(allOut);
		allOut.copyTo(outBitmap);
		bitmap.recycle();
		rs.destroy();
		return outBitmap;
	}

	public static Bitmap overlay(Bitmap bitmap1, Bitmap bitmapOverlay,
			int opacity) {
		Bitmap resultBitmap = Bitmap.createBitmap(bitmap1.getWidth(),
				bitmap1.getHeight(), Bitmap.Config.ARGB_8888);

		Canvas c = new Canvas(resultBitmap);
		c.drawBitmap(bitmap1, 0, 0, null);
		Paint p = new Paint();
		p.setAlpha(opacity);
		c.drawBitmap(bitmapOverlay, 0, 0, p);
		return resultBitmap;
	}

	private static Bitmap newscaleBitmap(Bitmap originalImage, int width,
			int height) {
		Bitmap background = Bitmap
				.createBitmap(width, height, Config.ARGB_8888);
		float originalWidth = originalImage.getWidth(), originalHeight = originalImage
				.getHeight();
		Canvas canvas = new Canvas(background);
		float scale = width / originalWidth;
		float xTranslation = 0.0f, yTranslation = (height - originalHeight
				* scale) / 2.0f;

		if (yTranslation < 0) {
			yTranslation = 0;
			scale = height / originalHeight;
			xTranslation = (width - originalWidth * scale) / 2.0f;
		}

		Matrix transformation = new Matrix();
		transformation.postTranslate(xTranslation, yTranslation);
		Log.d("translation", "xTranslation :" + xTranslation
				+ " yTranslation :" + yTranslation);
		// transformation.postTranslate(1f, 1f);
		transformation.preScale(scale, scale);
		Paint paint = new Paint();
		paint.setFilterBitmap(true);
		canvas.drawBitmap(originalImage, transformation, paint);
		return background;
	}

	public static Bitmap checkBitmap(String path) {
		BitmapFactory.Options bounds = new BitmapFactory.Options();
		bounds.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, bounds);

		BitmapFactory.Options opts = new BitmapFactory.Options();
		Bitmap bm = BitmapFactory.decodeFile(path, opts);
		ExifInterface exif;
		try {
			exif = new ExifInterface(path);
			String orientString = exif
					.getAttribute(ExifInterface.TAG_ORIENTATION);
			int orientation = orientString != null ? Integer
					.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;
			int rotationAngle = 0;
			if (orientation == ExifInterface.ORIENTATION_ROTATE_90)
				rotationAngle = 90;
			if (orientation == ExifInterface.ORIENTATION_ROTATE_180)
				rotationAngle = 180;
			if (orientation == ExifInterface.ORIENTATION_ROTATE_270)
				rotationAngle = 270;
			Matrix matrix = new Matrix();
			matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2,
					(float) bm.getHeight() / 2);
			Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0,
					bounds.outWidth, bounds.outHeight, matrix, true);
			return rotatedBitmap;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
