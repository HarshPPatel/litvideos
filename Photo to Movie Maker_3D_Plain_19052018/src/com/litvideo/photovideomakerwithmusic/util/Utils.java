package com.litvideo.photovideomakerwithmusic.util;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Path;
import android.os.Build;
import android.util.TypedValue;

public class Utils {
	public static final String DEV_ACC_NAME = "Creative 2017 Apps";
	public static final String Privacy_policy = "https://docs.google.com/document/d/1rrrt2deOndGTkCzjhomgfzQupt08_4hzmchRBXMxx_k/pub";
	
	public final static int DEFAULT_TINT = Color.BLACK;
	public final static int DEFAULT_ALPHA = 50;
	public final static String TAG_IMAGE = "FrissonView";
	public final static int TIDE_COUNT = 3;
	public final static int TIDE_HEIGHT_DP = 30;
	public final static int DEFAULT_ANGLE = 90;
	public final static int START_COLOR = Color.BLACK;
	public final static int END_COLOR = Color.WHITE;

	/**
	 * Indicates if the device is running LMP or higher.
	 */
	public static boolean isLmpOrAbove() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
	}

	public static boolean isLmpMR1OrAbove() {
		// TODO(adamcohen): update to Build.VERSION_CODES.LOLLIPOP_MR1 once
		// building against 22;
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1;
	}

	public static boolean isLmpMR1() {
		// TODO(adamcohen): update to Build.VERSION_CODES.LOLLIPOP_MR1 once
		// building against 22;
		return Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP_MR1;
	}

	public static int getPixelForDp(Context context, int displayPixels) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				displayPixels, context.getResources().getDisplayMetrics());
	}

	public static Path getWavePath(float width, float height, float amplitude,
			float shift, float divide) {
		Path path = new Path();
		float quadrant = height - amplitude;
		float x, y;
		path.moveTo(0, 0);
		path.lineTo(0, quadrant);
		for (int i = 0; i < width + 10; i = i + 10) {
			x = (float) i;
			y = quadrant
					+ amplitude
					* (float) Math.sin(((i + 10) * Math.PI / 180) / divide
							+ shift);
			path.lineTo(x, y);
		}
		path.lineTo(width, 0);
		path.close();
		return path;
	}
}
