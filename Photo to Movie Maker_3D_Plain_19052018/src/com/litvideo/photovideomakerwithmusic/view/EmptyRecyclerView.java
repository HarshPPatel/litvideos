package com.litvideo.photovideomakerwithmusic.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

public class EmptyRecyclerView extends RecyclerView {
	private View emptyView;
	private AdapterDataObserver dataObserver = new AdapterDataObserver() {

		@Override
		public void onChanged() {
			Adapter<?> adapter = getAdapter();
			if (adapter != null && emptyView != null) {
				if (adapter.getItemCount() == 0) {
					emptyView.setVisibility(View.VISIBLE);
					EmptyRecyclerView.this.setVisibility(View.GONE);
				} else {
					emptyView.setVisibility(View.GONE);
					EmptyRecyclerView.this.setVisibility(View.VISIBLE);
				}
			}
		}
	};

	public EmptyRecyclerView(Context context) {
		this(context, null);

	}

	public EmptyRecyclerView(Context context, AttributeSet attr) {
		this(context, attr, 0);

	}

	public EmptyRecyclerView(Context context, AttributeSet attr, int defStyle) {
		super(context, attr, defStyle);

	}

	@Override
	public void setAdapter(@SuppressWarnings("rawtypes") Adapter adapter) {
		// TODO Auto-generated method stub
		super.setAdapter(adapter);
		if (adapter != null) {
			adapter.registerAdapterDataObserver(dataObserver);
		}
		dataObserver.onChanged();
	}

	public void setEmptyView(View emptyView) {
		this.emptyView = emptyView;
	}
}
