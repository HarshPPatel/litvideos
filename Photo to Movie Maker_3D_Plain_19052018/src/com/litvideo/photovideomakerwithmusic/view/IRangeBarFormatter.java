package com.litvideo.photovideomakerwithmusic.view;

public interface IRangeBarFormatter {
	String format(String value);
}
