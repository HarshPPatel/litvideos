package com.litvideo.photovideomakerwithmusic.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

public class MyVideoView extends VideoView {

	public static interface PlayPauseListner {
		void onVideoPause();
		void onVideoPlay();
	}
	PlayPauseListner listner;
	public void setOnPlayPauseListner(PlayPauseListner listner) {
		this.listner = listner;
	}
	public MyVideoView(Context context) {
		super(context);
	}
	public MyVideoView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public MyVideoView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	@Override
	public void start() {
		super.start();
		if(listner!=null){
			listner.onVideoPlay();
		}
	}
	@Override
	public void pause() {
		super.pause();
		if(listner!=null){
			listner.onVideoPause();
		}
	}
	 
}
