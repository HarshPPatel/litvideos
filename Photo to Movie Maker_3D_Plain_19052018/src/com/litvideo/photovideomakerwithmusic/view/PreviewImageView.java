package com.litvideo.photovideomakerwithmusic.view;

import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.MyApplication;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

public class PreviewImageView extends ImageView {

	public static int mAspectRatioWidth = 640;
	public static int mAspectRatioHeight = 360;

	public PreviewImageView(Context context) {
		super(context);

	}

	public PreviewImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		Init(context, attrs);
	}

	public PreviewImageView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		Init(context, attrs);
	}

	@SuppressLint("NewApi")
	public PreviewImageView(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		Init(context, attrs);
	}

	private void Init(Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.PreviewImageView);

		mAspectRatioWidth = a.getInt(
				R.styleable.PreviewImageView_aspectRatioWidth, MyApplication.VIDEO_WIDTH
				);
		mAspectRatioHeight = a.getInt(
				R.styleable.PreviewImageView_aspectRatioHeight, MyApplication.VIDEO_HEIGHT);

		Log.e("mAspectRatioWidth", "mAspectRatioWidth:"+mAspectRatioWidth+" mAspectRatioHeight:"+mAspectRatioHeight);
		a.recycle();
	}

	// **overrides**

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int originalWidth = MeasureSpec.getSize(widthMeasureSpec);

		int originalHeight = MeasureSpec.getSize(heightMeasureSpec);

		int calculatedHeight = (int) (originalWidth * mAspectRatioHeight
				/(float) mAspectRatioWidth);

		int finalWidth, finalHeight;

		if (calculatedHeight > originalHeight) {
			finalWidth = (int) (originalHeight * mAspectRatioWidth
					/ (float)mAspectRatioHeight);
			finalHeight = originalHeight;
		} else {
			finalWidth = originalWidth;
			finalHeight = calculatedHeight;
		}

		super.onMeasure(
				MeasureSpec.makeMeasureSpec(finalWidth, MeasureSpec.EXACTLY),
				MeasureSpec.makeMeasureSpec(finalHeight, MeasureSpec.EXACTLY));
	}
}
