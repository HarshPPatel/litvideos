package com.litvideo.photovideomakerwithmusic.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.MyApplication;

public class ProportionalRelativeLayout extends RelativeLayout {
	private float mProportion;

	@SuppressLint("Recycle")
	public ProportionalRelativeLayout(Context paramContext,
			AttributeSet paramAttributeSet) {
		super(paramContext, paramAttributeSet);
		this.mProportion = paramContext.obtainStyledAttributes(
				paramAttributeSet, R.styleable.ProportionalRelativeLayout)
				.getFloat(0,(float) MyApplication.VIDEO_HEIGHT/(float) MyApplication.VIDEO_WIDTH);
	}

	protected void onMeasure(int paramInt1, int paramInt2) {
		int j = View.MeasureSpec.getSize(paramInt1);
		int i = View.MeasureSpec.getSize(paramInt2);
		j = View.MeasureSpec.makeMeasureSpec(
				(int) Math.ceil(this.mProportion * j),
				View.MeasureSpec.getMode(paramInt2));
		if (i != 0) {
			i = View.MeasureSpec.makeMeasureSpec(
					(int) Math.ceil(i / this.mProportion),
					View.MeasureSpec.getMode(paramInt2));
			if (paramInt2 > j) {
				super.onMeasure(paramInt1, j);
				return;
			}
			super.onMeasure(i, paramInt2);
			return;
		}
		super.onMeasure(paramInt1, j);
	}
}