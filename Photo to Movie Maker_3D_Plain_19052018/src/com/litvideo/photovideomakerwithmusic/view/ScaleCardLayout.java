package com.litvideo.photovideomakerwithmusic.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;

import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.MyApplication;

public class ScaleCardLayout extends CardView {

	public   int mAspectRatioWidth = 640;
	public   int mAspectRatioHeight = 360;

	public ScaleCardLayout(Context context) {
		super(context);

	}

	public ScaleCardLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		Init(context, attrs);
	}

	public ScaleCardLayout(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		Init(context, attrs);
	}

 

	private void Init(Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.ScaleCardLayout);

		mAspectRatioWidth = a.getInt(
				R.styleable.ScaleCardLayout_aspectRatioWidth, MyApplication.VIDEO_WIDTH);
		mAspectRatioHeight = a.getInt(
				R.styleable.ScaleCardLayout_aspectRatioHeight , MyApplication.VIDEO_HEIGHT);
		a.recycle();
	}


	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		if (mAspectRatioHeight == mAspectRatioWidth) {
			super.onMeasure(widthMeasureSpec, widthMeasureSpec);
		}
		int originalWidth = MeasureSpec.getSize(widthMeasureSpec);
		int originalHeight = MeasureSpec.getSize(heightMeasureSpec);
		int calculatedHeight = (int) (originalWidth * mAspectRatioHeight / (float) mAspectRatioWidth);

		int finalWidth, finalHeight;

		if (calculatedHeight > originalHeight) {
			finalWidth = (int) (originalHeight * mAspectRatioWidth / (float) mAspectRatioHeight);
			finalHeight = originalHeight;
		} else {
			finalWidth = originalWidth;
			finalHeight = calculatedHeight;
		}

		super.onMeasure(
				MeasureSpec.makeMeasureSpec(finalWidth, MeasureSpec.EXACTLY),
				MeasureSpec.makeMeasureSpec(finalHeight, MeasureSpec.EXACTLY));
	}
}
