package com.litvideo.photovideomakerwithmusic.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.MyApplication;

public class ScaleRelativeLayout extends RelativeLayout {

	public   int mAspectRatioWidth = 640;
	public   int mAspectRatioHeight = 360;

	public ScaleRelativeLayout(Context context) {
		super(context);

	}

	public ScaleRelativeLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		Init(context, attrs);
	}

	public ScaleRelativeLayout(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		Init(context, attrs);
	}

	@SuppressLint("NewApi")
	public ScaleRelativeLayout(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		Init(context, attrs);
	}

	private void Init(Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.ScaleRelativeLayout);

		mAspectRatioWidth = a.getInt(
				R.styleable.ScaleRelativeLayout_aspectRatioWidth, MyApplication.VIDEO_WIDTH);
		mAspectRatioHeight = a.getInt(
				R.styleable.ScaleRelativeLayout_aspectRatioHeight , MyApplication.VIDEO_HEIGHT);
		a.recycle();
	}


	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		if (mAspectRatioHeight == mAspectRatioWidth) {
			super.onMeasure(widthMeasureSpec, widthMeasureSpec);
		}
		int originalWidth = MeasureSpec.getSize(widthMeasureSpec);
		int originalHeight = MeasureSpec.getSize(heightMeasureSpec);
		int calculatedHeight = (int) (originalWidth * mAspectRatioHeight / (float) mAspectRatioWidth);

		int finalWidth, finalHeight;

		if (calculatedHeight > originalHeight) {
			finalWidth = (int) (originalHeight * mAspectRatioWidth / (float) mAspectRatioHeight);
			finalHeight = originalHeight;
		} else {
			finalWidth = originalWidth;
			finalHeight = calculatedHeight;
		}

		super.onMeasure(
				MeasureSpec.makeMeasureSpec(finalWidth, MeasureSpec.EXACTLY),
				MeasureSpec.makeMeasureSpec(finalHeight, MeasureSpec.EXACTLY));
	}
}
