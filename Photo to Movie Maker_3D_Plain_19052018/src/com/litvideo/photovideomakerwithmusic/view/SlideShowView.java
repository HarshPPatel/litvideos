package com.litvideo.photovideomakerwithmusic.view;

import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;

import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.mask.FinalMaskBitmap3D;
import com.litvideo.photovideomakerwithmusic.mask.FinalMaskBitmap3D.EFFECT;
import com.litvideo.photovideomakerwithmusic.util.ScalingUtilities;
import com.litvideo.photovideomakerwithmusic.MyApplication;

public class SlideShowView extends View implements OnClickListener {
	EFFECT effect;
	Drawable topDrawable, bottomDrawable;
	private Paint paint;
	private Bitmap  tempBmp;
	private Canvas tempCanvas;
	private int frame=15,lastFrame=-1;
	private ValueAnimator animate;
	
	public SlideShowView(Context context) {
		super(context);
		init();
	}

	public SlideShowView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public SlideShowView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public SlideShowView(Context context, AttributeSet attrs, int defStyleAttr,
			int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		init();
		
	}
	Bitmap top,bottom; 

	private void init() {
		effect =EFFECT.Whole3D_BT ;//WOrk
		FinalMaskBitmap3D.direction =0;
		
		paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
		tempBmp=Bitmap.createBitmap( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, Config.ARGB_8888);
		tempCanvas = new  Canvas(tempBmp);
		Resources  resources =getResources();
		topDrawable = resources.getDrawable(R.drawable.ic_shine_theme);
		bottomDrawable = resources.getDrawable(R.drawable.ic_shine_theme);
		topDrawable.setBounds(0, 0, MyApplication.VIDEO_WIDTH,  MyApplication.VIDEO_HEIGHT);
		bottomDrawable.setBounds(0, 0, MyApplication.VIDEO_WIDTH,  MyApplication.VIDEO_HEIGHT);
		setOnClickListener(this);
		bottom =
				 
				
			 BitmapFactory.decodeResource( getResources(),
                        R.drawable.ic_shine_theme);
		bottom=ScalingUtilities.scaleCenterCrop(bottom, MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT);
		top=
				 BitmapFactory.decodeResource( getResources(),
	                        R.drawable.ic_shine_theme);
		top=ScalingUtilities.scaleCenterCrop(top, MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT);
		FinalMaskBitmap3D.initBitmaps(bottom
				 ,top,effect);
		FinalMaskBitmap3D.reintRect();
		
	}

	public void setEffect(EFFECT effect) {
		this.effect = effect;
	}

	public EFFECT getEffect() {
		return effect;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawColor(Color.BLACK);
		if (effect != null && topDrawable!=null && bottomDrawable!=null) {
			//bottomDrawable.draw(canvas);
//			topDrawable.draw(tempCanvas);
//			tempCanvas.drawBitmap(effect.getMask( MyApplication.VIDEO_WIDTH, MyApplication.VIDEO_HEIGHT, frame), 0, 0,paint);
			canvas.drawBitmap(
					effect.getMask(
							bottom,top,
					
					frame), 0, 0, null);
//			canvas.drawBitmap(tempBmp,0,0, null);
		}
	}
	
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		invalidate();
	}
	
	public  void startFrameAnimation() {
		if(animate!=null ){
			animate.cancel();
		}
		lastFrame=-1;
		frame=0;
		animate = ValueAnimator.ofInt(0,22);
		animate.setDuration(2000l);
//		animate.setRepeatCount(ValueAnimator.INFINITE);
		animate.setRepeatMode  (ValueAnimator.RESTART );
		animate.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				frame = (int) animation.getAnimatedValue() ;
				frame=frame%22;
				if(frame==0){
					FinalMaskBitmap3D.reintRect();
				}
				if(lastFrame!=frame){
					invalidate();
					lastFrame=frame;
				}else if(frame ==0){
					lastFrame=-1;
					invalidate();
				}
			}
		});
		animate.start();
	}

	@Override
	public void onClick(View arg0) {
		FinalMaskBitmap3D.reintRect();
		startFrameAnimation();
		
	}
}
