package com.litvideo.photovideomakerwithmusic.view;

import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.View;

public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
	  private int space;

	  public SpacesItemDecoration(int space) {
	    this.space = space;
	  }

	  @Override
	  public void getItemOffsets(Rect outRect, View view, 
	      RecyclerView parent, RecyclerView.State state) {
	    outRect.left = space;
	    outRect.right = space;
	    outRect.bottom = space;

	    LayoutManager manager = parent.getLayoutManager();
	    int span =1;
	    if(manager instanceof GridLayoutManager){
	    	GridLayoutManager grid= (GridLayoutManager) manager;
	    	span=grid.getSpanCount();
	    }
	    // Add top margin only for the first item to avoid double space between items
	    if (parent.getChildLayoutPosition(view) <(span)) {
	        outRect.top = space*2;
	    } else {
	        outRect.top = 0;
	    }
	  }
	}