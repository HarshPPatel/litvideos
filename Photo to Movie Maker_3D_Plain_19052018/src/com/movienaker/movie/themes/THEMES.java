package com.movienaker.movie.themes;

import java.util.ArrayList;

import com.litvideo.photovideomakerwithmusic.R;
import com.litvideo.photovideomakerwithmusic.mask.FinalMaskBitmap3D.EFFECT;

public enum THEMES {

	Shine("Shine") {
		@Override
		public ArrayList<EFFECT> getTheme() {
			ArrayList<EFFECT> mEffects = new ArrayList<>();
			// TODO

			mEffects.add(EFFECT.Whole3D_BT);
			mEffects.add(EFFECT.Whole3D_TB);
			mEffects.add(EFFECT.Whole3D_LR);
			mEffects.add(EFFECT.Whole3D_RL);

			mEffects.add(EFFECT.SepartConbine_BT);
			mEffects.add(EFFECT.SepartConbine_TB);
			mEffects.add(EFFECT.SepartConbine_LR);
			mEffects.add(EFFECT.SepartConbine_RL);

			mEffects.add(EFFECT.RollInTurn_BT);
			mEffects.add(EFFECT.RollInTurn_TB);
			mEffects.add(EFFECT.RollInTurn_LR);
			mEffects.add(EFFECT.RollInTurn_RL);

			mEffects.add(EFFECT.Jalousie_BT);
			mEffects.add(EFFECT.Jalousie_LR);

			mEffects.add(EFFECT.Roll2D_BT);
			mEffects.add(EFFECT.Roll2D_TB);
			mEffects.add(EFFECT.Roll2D_LR);
			mEffects.add(EFFECT.Roll2D_RL);

			return mEffects;
		}

		@Override
		public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
			return null;
		}

		@Override
		public int getThemeDrawable() {
			return R.drawable.ic_shine_theme;
		}

		@Override
		public int getThemeMusic() {
			// TODO Auto-generated method stub
			return R.raw._1;
		}

	},
	Jalousie_BT("Jalousie_BT") {

		@Override
		public ArrayList<EFFECT> getTheme() {
			ArrayList<EFFECT> mEffects = new ArrayList<>();
			mEffects.add(EFFECT.Jalousie_BT);
			return mEffects;
		}

		@Override
		public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
			return null;
		}

		@Override
		public int getThemeDrawable() {
			return R.drawable.ic_shine_theme;
		}

		@Override
		public int getThemeMusic() {
			return R.raw._2;
		}

	},
	Jalousie_LR("Jalousie_LR") {

		@Override
		public ArrayList<EFFECT> getTheme() {
			ArrayList<EFFECT> mEffects = new ArrayList<>();
			mEffects.add(EFFECT.Jalousie_LR);
			return mEffects;
		}

		@Override
		public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
			return null;
		}

		@Override
		public int getThemeDrawable() {
			return R.drawable.ic_shine_theme;
		}

		@Override
		public int getThemeMusic() {
			return R.raw._4;
		}

	},
	Whole3D_BT("Whole3D_BT") {

		@Override
		public ArrayList<EFFECT> getTheme() {
			ArrayList<EFFECT> mEffects = new ArrayList<>();
			mEffects.add(EFFECT.Whole3D_BT);
			return mEffects;
		}

		@Override
		public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
			return null;
		}

		@Override
		public int getThemeDrawable() {
			return R.drawable.ic_shine_theme;
		}

		@Override
		public int getThemeMusic() {
			return R.raw._5;
		}

	},
	Whole3D_TB("Whole3D_TB") {

		@Override
		public ArrayList<EFFECT> getTheme() {
			ArrayList<EFFECT> mEffects = new ArrayList<>();
			mEffects.add(EFFECT.Whole3D_TB);
			return mEffects;
		}

		@Override
		public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
			return null;
		}

		@Override
		public int getThemeDrawable() {
			return R.drawable.ic_shine_theme;
		}

		@Override
		public int getThemeMusic() {
			return R.raw._6;
		}

	},
	Whole3D_LR("Whole3D_LR") {

		@Override
		public ArrayList<EFFECT> getTheme() {
			ArrayList<EFFECT> mEffects = new ArrayList<>();
			mEffects.add(EFFECT.Whole3D_LR);
			return mEffects;
		}

		@Override
		public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
			return null;
		}

		@Override
		public int getThemeDrawable() {
			return R.drawable.ic_shine_theme;
		}

		@Override
		public int getThemeMusic() {
			return R.raw._1;
		}

	},
	Whole3D_RL("Whole3D_Rl") {

		@Override
		public ArrayList<EFFECT> getTheme() {
			ArrayList<EFFECT> mEffects = new ArrayList<>();
			mEffects.add(EFFECT.Whole3D_RL);
			return mEffects;
		}

		@Override
		public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
			return null;
		}

		@Override
		public int getThemeDrawable() {
			return R.drawable.ic_shine_theme;
		}

		@Override
		public int getThemeMusic() {
			return R.raw._2;
		}

	},
	SepartConbine_BT("SepartConbine_BT") {

		@Override
		public ArrayList<EFFECT> getTheme() {
			ArrayList<EFFECT> mEffects = new ArrayList<>();
			mEffects.add(EFFECT.SepartConbine_BT);
			return mEffects;
		}

		@Override
		public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
			return null;
		}

		@Override
		public int getThemeDrawable() {
			return R.drawable.ic_shine_theme;
		}

		@Override
		public int getThemeMusic() {
			return R.raw._3;
		}

	},
	SepartConbine_TB("SepartConbine_TB") {

		@Override
		public ArrayList<EFFECT> getTheme() {
			ArrayList<EFFECT> mEffects = new ArrayList<>();
			mEffects.add(EFFECT.SepartConbine_TB);
			return mEffects;
		}

		@Override
		public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
			return null;
		}

		@Override
		public int getThemeDrawable() {
			return R.drawable.ic_shine_theme;
		}

		@Override
		public int getThemeMusic() {
			return R.raw._4;
		}

	},
	SepartConbine_LR("SepartConbine_LR") {

		@Override
		public ArrayList<EFFECT> getTheme() {
			ArrayList<EFFECT> mEffects = new ArrayList<>();
			mEffects.add(EFFECT.SepartConbine_LR);
			return mEffects;
		}

		@Override
		public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
			return null;
		}

		@Override
		public int getThemeDrawable() {
			return R.drawable.ic_shine_theme;
		}

		@Override
		public int getThemeMusic() {
			return R.raw._5;
		}

	},
	SepartConbine_RL("SepartConbine_Rl") {

		@Override
		public ArrayList<EFFECT> getTheme() {
			ArrayList<EFFECT> mEffects = new ArrayList<>();
			mEffects.add(EFFECT.SepartConbine_RL);
			return mEffects;
		}

		@Override
		public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
			return null;
		}

		@Override
		public int getThemeDrawable() {
			return R.drawable.ic_shine_theme;
		}

		@Override
		public int getThemeMusic() {
			return R.raw._6;
		}

	},
	RollInTurn_BT("RollInTurn_BT") {

		@Override
		public ArrayList<EFFECT> getTheme() {
			ArrayList<EFFECT> mEffects = new ArrayList<>();
			mEffects.add(EFFECT.RollInTurn_BT);
			return mEffects;
		}

		@Override
		public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
			return null;
		}

		@Override
		public int getThemeDrawable() {
			return R.drawable.ic_shine_theme;
		}

		@Override
		public int getThemeMusic() {
			return R.raw._1;
		}

	},
	RollInTurn_TB("RollInTurn_TB") {

		@Override
		public ArrayList<EFFECT> getTheme() {
			ArrayList<EFFECT> mEffects = new ArrayList<>();
			mEffects.add(EFFECT.RollInTurn_TB);
			return mEffects;
		}

		@Override
		public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
			return null;
		}

		@Override
		public int getThemeDrawable() {
			return R.drawable.ic_shine_theme;
		}

		@Override
		public int getThemeMusic() {
			return R.raw._2;
		}

	},
	RollInTurn_LR("RollInTurn_LR") {

		@Override
		public ArrayList<EFFECT> getTheme() {
			ArrayList<EFFECT> mEffects = new ArrayList<>();
			mEffects.add(EFFECT.RollInTurn_LR);
			return mEffects;
		}

		@Override
		public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
			return null;
		}

		@Override
		public int getThemeDrawable() {
			return R.drawable.ic_shine_theme;
		}

		@Override
		public int getThemeMusic() {
			return R.raw._3;
		}

	},
	RollInTurn_Rl("RollInTurn_RL") {

		@Override
		public ArrayList<EFFECT> getTheme() {
			ArrayList<EFFECT> mEffects = new ArrayList<>();
			mEffects.add(EFFECT.RollInTurn_RL);
			return mEffects;
		}

		@Override
		public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
			return null;
		}

		@Override
		public int getThemeDrawable() {
			return R.drawable.ic_shine_theme;
		}

		@Override
		public int getThemeMusic() {
			return R.raw._4;
		}

	},
	Roll2D_BT("Roll2D_BT") {

		@Override
		public ArrayList<EFFECT> getTheme() {
			ArrayList<EFFECT> mEffects = new ArrayList<>();
			mEffects.add(EFFECT.Roll2D_BT);
			return mEffects;
		}

		@Override
		public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
			return null;
		}

		@Override
		public int getThemeDrawable() {
			return R.drawable.ic_shine_theme;
		}

		@Override
		public int getThemeMusic() {
			return R.raw._5;
		}

	},
	Roll2D_TB("Roll2D_TB") {

		@Override
		public ArrayList<EFFECT> getTheme() {
			ArrayList<EFFECT> mEffects = new ArrayList<>();
			mEffects.add(EFFECT.Roll2D_TB);
			return mEffects;
		}

		@Override
		public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
			return null;
		}

		@Override
		public int getThemeDrawable() {
			return R.drawable.ic_shine_theme;
		}

		@Override
		public int getThemeMusic() {
			return R.raw._6;
		}

	},
	Roll2D_LR("Roll2D_LR") {

		@Override
		public ArrayList<EFFECT> getTheme() {
			ArrayList<EFFECT> mEffects = new ArrayList<>();
			mEffects.add(EFFECT.Roll2D_LR);
			return mEffects;
		}

		@Override
		public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
			return null;
		}

		@Override
		public int getThemeDrawable() {
			return R.drawable.ic_shine_theme;
		}

		@Override
		public int getThemeMusic() {
			return R.raw._1;
		}

	},
	Roll2D_Rl("Roll2D_Rl") {

		@Override
		public ArrayList<EFFECT> getTheme() {
			ArrayList<EFFECT> mEffects = new ArrayList<>();
			mEffects.add(EFFECT.Roll2D_RL);
			return mEffects;
		}

		@Override
		public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
			return null;
		}

		@Override
		public int getThemeDrawable() {
			return R.drawable.ic_shine_theme;
		}

		@Override
		public int getThemeMusic() {
			return R.raw._2;
		}

	},

	// // TODO remove comment for 2D Effects & also from FinalMaskBitmap.class
	// file
	// CIRCLE_IN("Circle In"){
	//
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add(EFFECT.CIRCLE_IN);
	// return list;
	// }
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	// @Override
	// public int getThemeMusic() {
	//
	// return R.raw._5;
	// }
	//
	// }, CIRCLE_LEFT_BOTTOM("Circle Left Bottom"){
	//
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add(EFFECT.CIRCLE_LEFT_BOTTOM);
	// return list;
	// }
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	// @Override
	// public int getThemeMusic() {
	// return R.raw._1;
	// }
	//
	// }, CIRCLE_LEFT_TOP("Circle Left Top"){
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add(EFFECT.CIRCLE_LEFT_TOP );
	// return list;
	// }
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	// @Override
	// public int getThemeMusic() {
	// return R.raw._2;
	// }
	// }, CIRCLE_OUT("Circle Out"){
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add(EFFECT.CIRCLE_OUT );
	// return list;
	// }
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	// @Override
	// public int getThemeMusic() {
	// return R.raw._3;
	// }
	// }, CIRCLE_RIGHT_BOTTOM("Circle Right Bottom"){
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add(EFFECT.CIRCLE_RIGHT_BOTTOM );
	// return list;
	// }
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	// @Override
	// public int getThemeMusic() {
	// return R.raw._4;
	// }
	//
	// }, CIRCLE_RIGHT_TOP("Circle Right Top"){
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add(EFFECT.CIRCLE_RIGHT_TOP );
	// return list;
	// }
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	// @Override
	// public int getThemeMusic() {
	// return R.raw._5;
	// }
	// }, DIAMOND_IN("Diamond In"){
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add(EFFECT.DIAMOND_IN );
	// return list;
	// }
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	// @Override
	// public int getThemeMusic() {
	// return R.raw._1;
	// }
	// }, DIAMOND_OUT("Diamond out"){
	//
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add(EFFECT.DIAMOND_OUT );
	// return list;
	// }
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	// @Override
	// public int getThemeMusic() {
	// return R.raw._2;
	// }
	// }, ECLIPSE_IN("Eclipse In"){
	//
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add(EFFECT.ECLIPSE_IN );
	// return list;
	// }
	//
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	//
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	//
	// @Override
	// public int getThemeMusic() {
	// return R.raw._3;
	// }
	// }, FOUR_TRIANGLE("Four Triangle"){
	//
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add(EFFECT.FOUR_TRIANGLE );
	// return list;
	// }
	//
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	//
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	//
	// @Override
	// public int getThemeMusic() {
	// return R.raw._4;
	// }
	// }/*, LEAF("leaf"){
	//
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add(EFFECT.LEAF );
	// return list;
	// }
	//
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	//
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	//
	// @Override
	// public int getThemeMusic() {
	// return R.raw._5;
	// }
	// }*/
	//
	// /*, OPEN_DOOR("open_door"){
	//
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add(EFFECT.OPEN_DOOR );
	// return list;
	// }
	//
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	//
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	// @Override
	// public int getThemeMusic() {
	// return R.raw._1;
	// }
	//
	// }*/, PIN_WHEEL("Pin Wheel"){
	//
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add(EFFECT.PIN_WHEEL );
	// return list;
	// }
	//
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	//
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	// @Override
	// public int getThemeMusic() {
	// return R.raw._2;
	// }
	//
	// }, RECT_RANDOM("Rect Random"){
	//
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add(EFFECT.RECT_RANDOM );
	// return list;
	// }
	//
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	//
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	// @Override
	// public int getThemeMusic() {
	// return R.raw._3;
	// }
	//
	// }, SKEW_LEFT_MEARGE("Skew Left Mearge"){
	//
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add(EFFECT.SKEW_LEFT_MEARGE );
	// return list;
	// }
	//
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	//
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	// @Override
	// public int getThemeMusic() {
	// return R.raw._4;
	// }
	// }, SKEW_LEFT_SPLIT("Skew Left Split"){
	//
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add(EFFECT.SKEW_LEFT_SPLIT );
	// return list;
	// }
	//
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	//
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	//
	// @Override
	// public int getThemeMusic() {
	// return R.raw._5;
	// }
	// }, SKEW_RIGHT_MEARGE("Skew Right Mearge"){
	//
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add(EFFECT.SKEW_RIGHT_MEARGE );
	// return list;
	// }
	//
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	//
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	//
	// @Override
	// public int getThemeMusic() {
	// return R.raw._1;
	// }
	// },
	// SKEW_RIGHT_SPLIT("Skew Right Split"){
	//
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add( EFFECT.SKEW_RIGHT_SPLIT);
	// return list;
	// }
	//
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	// @Override
	// public int getThemeMusic() {
	// return R.raw._2;
	// }
	//
	// },
	// SQUARE_OUT("Square Out"){
	//
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add( EFFECT.SQUARE_OUT);
	// return list;
	// }
	//
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	//
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	//
	// @Override
	// public int getThemeMusic() {
	// return R.raw._3;
	// }
	// },
	// SQUARE_IN("Square In"){
	//
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add( EFFECT.SQUARE_IN);
	// return list;
	// }
	//
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	// @Override
	// public int getThemeMusic() {
	// return R.raw._4;
	// }
	//
	// },
	// VERTICAL_RECT("Vertical Rect"){
	//
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add(EFFECT.VERTICAL_RECT );
	// return list;
	// }
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	// @Override
	// public int getThemeMusic() {
	// return R.raw._5;
	// }
	// },
	// WIND_MILL("Wind Mill"){
	// @Override
	// public ArrayList<EFFECT> getTheme() {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// list.add(EFFECT.WIND_MILL );
	// return list;
	// }
	// @Override
	// public ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects) {
	// ArrayList<EFFECT> list =new ArrayList<EFFECT>();
	// return list;
	// }
	// @Override
	// public int getThemeDrawable() {
	// return R.drawable.ic_custom_theme;
	// }
	// @Override
	// public int getThemeMusic() {
	// return R.raw._1;
	// }
	// }
	// TODO remove comment for 2D Effects & also from FinalMaskBitmap.class file
	;
	String name = "";

	private THEMES(String string) {
		this.name = string;
	}

	public abstract ArrayList<EFFECT> getTheme();

	public abstract ArrayList<EFFECT> getTheme(ArrayList<EFFECT> effects);

	public abstract int getThemeDrawable();

	public abstract int getThemeMusic();
}
